#!/usr/bin/env python3

from sys import argv, exit
from ROOT import TTree,TFile

import math
import re

def main():
    if len(argv) != 3:
        print(f"Usage: {argv[0]} <input list of files> <output list of files with sum of weights> ")
        exit(1)

    if argv[1] == argv[2]:
        print("ERROR: Input list and output list cannot be the same!")
        exit(1)

    fin = open(argv[1], 'r')
    fout = open(argv[2], 'w')
    processFile(fin, fout)
    fout.close()
    fin.close()

    print("Done.")

def generator_weights(sum_weights_tree):
    sumw_vec = []
    weight_name = []
    for ievent in range(sum_weights_tree.GetEntries()):
        sum_weights_tree.GetEntry(ievent)
        if ievent == 0:
            for iweight in range(sum_weights_tree.totalEventsWeighted_mc_generator_weights.size()):
                sumw_vec.append(sum_weights_tree.totalEventsWeighted_mc_generator_weights.at(iweight))
                name = sum_weights_tree.names_mc_generator_weights.at(iweight)
                name = name.replace('=','')
                name = name.replace(':','')
                name = name.replace(',','')
                name = name.replace(' ','')
                name = name.replace('.','')
                weight_name.append(name)
        else:
            for iweight in range(len(sumw_vec)):
                sumw_vec[iweight] += sum_weights_tree.totalEventsWeighted_mc_generator_weights.at(iweight)

    return sumw_vec,weight_name

def processFile(fin, fout):
    files = fin.readlines() # NOTE, each line has a '\n' at the end
    # here we will store sumWeights for each DSID, for data/unknown DSID, default sumW = -1.
    sampleSumWeights = { }

    for f in [line.strip('\n') for line in files]:
        filename = f.split('/')[-1] # get the name of the folder
        parentdir = f.split('/')[-2] # get the name of the folder

        print(f"Processing: {parentdir}/{filename}")

        # Get DSID
        rootfile = TFile(f, 'read')
        sumWtree = rootfile.Get('sumWeights')
        DSID = -1
        af2 = 'FS'
        if re.search('_a[0-9][0-9][0-9]_', f):
            af2 = 'AFII'
        else:
            af2 = 'FS'
        campaign = 'mc21a'
        if sumWtree:
            sumWtree.GetEntry(0)
            DSID = sumWtree.dsid

            current_key = str(DSID) + af2 + campaign

            # if this is a MC file, collect it's sumWeights into our dictionary
            if DSID > 100000:
                sumWvec,names = generator_weights(sumWtree)

                if(current_key) in sampleSumWeights:
                    for iweight in range(len(sampleSumWeights[current_key][4])):
                        sampleSumWeights[current_key][4][iweight] += sumWvec[iweight]
                else:
                    sampleSumWeights[current_key] = [DSID, af2, campaign, names, sumWvec]


    # for each output dataset file, write full path and sum of weights
    # in the output file list
    for key,value in sampleSumWeights.items():
        DSID = value[0]
        af2 = value[1]
        campaign = value[2]
        names = value[3]

        if DSID > 100000:
            for iweight in range(len(value[4])):
                fout.write(str(DSID) + ' ' )
                fout.write(af2 + ' ' )
                fout.write(campaign + ' ' )
                fout.write(str(iweight) + ' ' )
                fout.write(names[iweight] + ' ' )
                if (math.isnan(value[4][iweight])):
                    fout.write('-1\n' ) 
                else:
                    fout.write(str(value[4][iweight]) + '\n' )

# run the script (if executed, and *not* imported as a module)
if __name__ == '__main__':
    main()
