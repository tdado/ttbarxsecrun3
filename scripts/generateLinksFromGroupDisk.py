#!/usr/bin/env python2

from sys import argv, exit
from ROOT import *
import subprocess
import re
import math
import sys
import os

RSENAME="INFN-ROMA2_LOCALGROUPDISK"

def getLocalGroupPath(rsename,ll):
  if "INFN-ROMA2_LOCALGROUPDISK" in RSENAME:
    return "/localgroup/"+ll.split("atlaslocalgroupdisk")[1].replace("|","").replace("'","").lstrip().rstrip()

def main():
  if len(argv) != 3:
    print("Usage: {} <name of dataset> <output dir>".format(argv[0]))
    exit(1)
  
  ds=argv[1]
  outdir=argv[2]
  MyOut = subprocess.Popen(['rucio', 'list-file-replicas', '--rse', RSENAME, ds],stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
  stdout,stderr = MyOut.communicate()
  
  ifile=0
  for ll in stdout.decode(sys.stdout.encoding).splitlines():
    if not "user" in ll:
      continue
    #print(ll)
    pathfile=getLocalGroupPath(RSENAME,ll)
    
    linkname=outdir+"/"+ds.replace("_output_root","")+"_"+str(ifile)+".root"
    print(f"created link for {pathfile} -> {linkname}")
    os.symlink(pathfile, linkname)
    ifile+=1

if __name__ == '__main__':
    main()
