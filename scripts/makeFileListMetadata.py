#!/usr/bin/env python3

from sys import argv, exit
from ROOT import TTree,TFile
import os.path

import re


# ======================= define DSIDs here: ============================
ttbar_singlelep_dsids = [601229]
ttbar_dilep_dsids = [601230]
ttbar_hdamp_dsids = [601398, 601399]
ttbar_PH7_dsids = [601414, 601415]
ttbar_sherpa_dsids = [700660, 700661, 700662]
single_top_s_dsids = [601348, 601349]
single_top_t_dsids = [601350, 601351]
single_top_tW_dsids = [601352, 601355]
single_top_tW_dilep_dsids = [601353, 601354]
single_top_tW_DS_dsids = [601455, 601457, 601459, 601461]
wjets_dsids = [i for i in range(700606, 700615)]
zjets_dsids = [i for i in range(700615, 700627)] + [i for i in range(700697, 700706)]
zjets_PP8_dsids = [601189, 601190, 601191]
zjets_aMCNLO_P8_dsids = [i for i in range(513105, 513114)]
diboson_dsids = [i for i in range(700566, 700575)] + [i for i in range(700600, 700606)]
ttV_dsids = [700757, 700758, 700759, 700706]
ttbar_mass_171p5_dsids = [601673]
ttbar_mass_173p5_dsids = [601674]
single_top_tW_mass_171p5_dsids = [601675, 601676]
single_top_tW_mass_173p5_dsids = [601677, 601678]

# =======================================================================

def main():
    if len(argv) != 3:
        print(f"Usage: {argv[0]} <input list of files> <output list of files with sum of weights>")
        exit(1)

    if argv[1] == argv[2]:
        print("ERROR: Input list and output list cannot be the same!")
        exit(1)


    fin = open(argv[1], 'r') #.txt_temp
    fout = open(argv[2], 'w') #.txt
    processFile(fin, fout)
    fout.close()
    fin.close()

    print("Done.")

def processFile(fin, fout):
    files = fin.readlines() # NOTE, each line has a '\n' at the end
    print("Writing file list with sumOfWeights values...")

    # for each output dataset file, write full path and sum of weights
    # in the output file list
    for f in [line.strip('\n') for line in files]:

        # Get DSID
        print(f)
        rootfile = TFile(f, 'read')
        tree = rootfile.Get('nominal')

        af2 = 'FS'
        if re.search('_a[0-9][0-9][0-9]_', f):
            af2 = 'AFII'
        else:
            af2 = 'FS'
        DSID = -1
        if tree:
            if tree.GetEntries() == 0:
                print(f"Skipping empty sample: {f}")
                continue
            tree.GetEntry(0)
            DSID = tree.mcChannelNumber
            campaign = 'mc21a'

            if DSID > 0:
                current_key = str(DSID) + af2 + campaign
            else:
                current_key = str(DSID)

        else:
            print(f"Skipping sample without nominal(_Loose) tree: {f}")
            continue

        fout.write(f + ' ')
        fout.write(getSampleType(DSID, af2) + ' ')
        fout.write(af2 + ' ' )
        fout.write(str(DSID) + ' ' )
        fout.write(campaign + '\n' )

def getSampleType(DSID, af2):
    if DSID == 0:
      return 'Data'
    elif DSID in ttbar_singlelep_dsids:
        return 'ttbar_PP8_singlelep_' + af2
    elif DSID in ttbar_dilep_dsids:
        return 'ttbar_PP8_dilep_' + af2
    elif DSID in ttbar_hdamp_dsids:
        return 'ttbar_PP8_hdamp_' + af2
    elif DSID in ttbar_PH7_dsids:
        return 'ttbar_PH7_' + af2
    elif DSID in ttbar_sherpa_dsids:
        return 'ttbar_Sherpa_' + af2
    elif DSID in single_top_s_dsids:
        return 'SingleTop_PP8_s_chan_' + af2
    elif DSID in single_top_t_dsids:
        return 'SingleTop_PP8_t_chan_' + af2
    elif DSID in single_top_tW_dsids:
        return 'SingleTop_PP8_tW_chan_' + af2
    elif DSID in single_top_tW_dilep_dsids:
        return 'SingleTop_PP8_tW_chan_dilep_' + af2
    elif DSID in single_top_tW_DS_dsids:
        return 'SingleTop_DS_PP8_tW_chan_' + af2
    elif DSID in wjets_dsids:
        return 'Wjets_Sherpa_' + af2
    elif DSID in zjets_dsids:
        return 'Zjets_Sherpa_' + af2
    elif DSID in zjets_PP8_dsids:
        return 'Zjets_PP8_' + af2
    elif DSID in zjets_aMCNLO_P8_dsids:
        return 'Zjets_aMCNLO_P8_' + af2
    elif DSID in diboson_dsids:
        return 'Diboson_Sherpa_' + af2
    elif DSID in ttV_dsids:
        return 'ttV_Sherpa_' + af2
    elif DSID in ttbar_mass_171p5_dsids:
        return 'ttbar_PP8_dilep_mass_171p5_' + af2
    elif DSID in ttbar_mass_173p5_dsids:
        return 'ttbar_PP8_dilep_mass_173p5_' + af2
    elif DSID in single_top_tW_mass_171p5_dsids:
        return 'singleTop_PP8_tW_chan_mass_171p5_' + af2
    elif DSID in single_top_tW_mass_173p5_dsids:
        return 'singleTop_PP8_tW_chan_mass_173p5_' + af2
    else:
        return 'unknown'

# run the script (if executed, and *not* imported as a module)
if __name__ == '__main__':
    main()
