#!/bin/bash

# THIS SCRIPT PRODUCES A LIST OF ROOT FILES WITH METADATA: SAMPLE TYPE, TOTAL SUM OF WEIGHTS FOR GIVEN SAMPLE

SAMPLEDIR='../test/input/dilep/'
OUTPUTFILELIST="file_list_dilep.txt"
OUTPUTSUMW="sumW_dilep.txt"

if [[ ! -f $(basename $0) ]]; then
    echo "Run this script from the dir where it lives" >&2
    exit 1
fi

WRKDIR=$PWD

echo "Going to create filelist out of samples in directory:"
echo "'${SAMPLEDIR}'"
echo "Filelist name: '${OUTPUTFILELIST}'"

FILELISTPATH="${WRKDIR}/"${OUTPUTFILELIST}
SUMWPATH="${WRKDIR}/"${OUTPUTSUMW}

if [ -e "$FILELISTPATH" ]; then
    echo "Removing existing file_list file."
    rm "$FILELISTPATH"
fi

if [ -d ${SAMPLEDIR} ]; then
    cd $SAMPLEDIR
    find -L `pwd` -type f -name "*.root*" >> $FILELISTPATH
else
    echo "ERROR: Directory '${SAMPLEDIR}' doesn't exist! Aborting."
    exit 1
fi

cd $WRKDIR

# pre-process the files in the filelist and create final filelist with sample types and sumOfWeights
#echo "GETTING SAMPLES' METADATA (SUM OF WEIGHTS & SAMPLE TYPE)"
mv "$FILELISTPATH" "${FILELISTPATH}_temp"
./makeFileListMetadata.py "${FILELISTPATH}_temp" "$FILELISTPATH"
./makeSumWeights.py "${FILELISTPATH}_temp" "$SUMWPATH"
rm "${FILELISTPATH}_temp"
