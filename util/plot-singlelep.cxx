#include "TTbarXsec/Plotter.h"
#include "TTbarXsec/SystHistoManager.h"

#include "TError.h"

#include <iostream>
#include <string>
#include <vector>

int main (int argc, const char *argv[]) {
    if (argc != 2 && argc != 3) {
        std::cerr << "Expecting one or two arguments but " << argc-1 << " arguments provided" << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::string folder = argv[1];
    bool use_syst(false);

    if (argc == 3) {
        const std::string syst_param = argv[2];
        if (syst_param == "syst") {
            use_syst = true;
        } else {
            std::cerr << "Third argument can only be 'syst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    gErrorIgnoreLevel = kError;

    Plotter plotter(folder, Common::CHANNEL::SINGLELEPTON);

    static const std::vector<std::string> ttbar_names = {"ttbar_PP8_FS"};
    static const std::vector<std::string> bkg_names = {"SingleTop_PP8_s_chan_FS",
                                                       "SingleTop_PP8_t_chan_FS",
                                                       "SingleTop_PP8_tW_chan_FS",
                                                       "Wjets_Sherpa_FS",
                                                       "Zjets_Sherpa_FS",
                                                       //"Diboson_Sherpa_FS",
                                                       //"ttV_aMCNLO_Pythia8_FS",
                                                       //"ttH_PP8_FS",
                                                       "Multijet"};

    static const std::vector<std::string> special_names = {"ttbar_PP8_hdamp_FS",
                                                           "ttbar_PH7_FS"
                                                           };

    plotter.SetTTbarNames(ttbar_names);
    plotter.SetBackgroundNames(bkg_names);
    plotter.SetSpecialNames(special_names);
    plotter.SetDataName("Data");
    plotter.SetAtlasLabel("Internal");
    plotter.SetLumiLabel("XXX");
    plotter.SetCollection("");
    plotter.SetNominalTTbarName("ttbar_PP8_FS");
    plotter.SetSystShapeOnly(false);
    plotter.SetRunSyst(use_syst);
    plotter.SetScale(1.0);
    plotter.SetRegionNames(Common::GetRegionString(Common::CHANNEL::SINGLELEPTON), {});
    plotter.OpenRootFiles();

    /// Set all systematics
    SystHistoManager syst_manager{};

    std::vector<std::string> all(bkg_names);
    all.emplace_back(plotter.GetNominalTTbarName());

    /// Set tree systematics
    for (const auto& ifile : all) {
        if (ifile == "Multijet") continue;
        for (int ib = 0; ib < 2; ++ib) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_B_"+std::to_string(ib)+"_up", "BTAG_B_"+std::to_string(ib)+"_down", "", "nominal", ifile, "", "");
        }
        for (int ic = 0; ic < 2; ++ic) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_C_"+std::to_string(ic)+"_up", "BTAG_C_"+std::to_string(ic)+"_down", "", "nominal", ifile, "", "");
        }
        for (int il = 0; il < 3; ++il) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Light_"+std::to_string(il)+"_up", "BTAG_Light_"+std::to_string(il)+"_down", "", "nominal", ifile, "", "");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Extrapolation_from_charm_up", "BTAG_Extrapolation_from_charm_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_trigger_up", "el_trigger_down", "", "nominal", ifile, "","");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_reco_up", "el_reco_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_id_up", "el_id_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_isol_up", "el_isol_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_stat_up", "mu_trigger_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_syst_up", "mu_trigger_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_up", "mu_id_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_up", "mu_id_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_lowpt_up", "mu_id_stat_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_lowpt_up", "mu_id_syst_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_stat_up", "mu_isol_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_syst_up", "mu_isol_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_stat_up", "mu_ttva_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_syst_up", "mu_ttva_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "jvt_up", "jvt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "pileup_up", "pileup_down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "JET_BJES_Response__1up", "JET_BJES_Response__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Detector1__1up", "JET_EffectiveNP_Detector1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Detector2__1up", "JET_EffectiveNP_Detector2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed1__1up", "JET_EffectiveNP_Mixed1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed2__1up", "JET_EffectiveNP_Mixed2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed3__1up", "JET_EffectiveNP_Mixed3__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling1__1up", "JET_EffectiveNP_Modelling1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling2__1up", "JET_EffectiveNP_Modelling2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling3__1up", "JET_EffectiveNP_Modelling3__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling4__1up", "JET_EffectiveNP_Modelling4__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical1__1up", "JET_EffectiveNP_Statistical1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical2__1up", "JET_EffectiveNP_Statistical2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical3__1up", "JET_EffectiveNP_Statistical3__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical4__1up", "JET_EffectiveNP_Statistical4__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical5__1up", "JET_EffectiveNP_Statistical5__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical6__1up", "JET_EffectiveNP_Statistical6__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_Modelling__1up", "JET_EtaIntercalibration_Modelling__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_NonClosure_highE__1up", "JET_EtaIntercalibration_NonClosure_highE__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_NonClosure_negEta__1up", "JET_EtaIntercalibration_NonClosure_negEta__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_NonClosure_posEta__1up", "JET_EtaIntercalibration_NonClosure_posEta__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_TotalStat__1up", "JET_EtaIntercalibration_TotalStat__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Flavor_Composition__1up", "JET_Flavor_Composition__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Flavor_Response__1up", "JET_Flavor_Response__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_DataVsMC_MC16__1up", "JET_JER_DataVsMC_MC16__1down", "", "nominal", ifile, "", "", "JET_JER_DataVsMC_MC16__1up_PseudoData", "JET_JER_DataVsMC_MC16__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_1__1up", "JET_JER_EffectiveNP_1__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_1__1up_PseudoData", "JET_JER_EffectiveNP_1__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_2__1up", "JET_JER_EffectiveNP_2__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_2__1up_PseudoData", "JET_JER_EffectiveNP_2__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_3__1up", "JET_JER_EffectiveNP_3__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_3__1up_PseudoData", "JET_JER_EffectiveNP_3__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_4__1up", "JET_JER_EffectiveNP_4__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_4__1up_PseudoData", "JET_JER_EffectiveNP_4__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_5__1up", "JET_JER_EffectiveNP_5__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_5__1up_PseudoData", "JET_JER_EffectiveNP_5__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_6__1up", "JET_JER_EffectiveNP_6__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_6__1up_PseudoData", "JET_JER_EffectiveNP_6__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_7__1up", "JET_JER_EffectiveNP_7__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_7__1up_PseudoData", "JET_JER_EffectiveNP_7__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_8__1up", "JET_JER_EffectiveNP_8__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_8__1up_PseudoData", "JET_JER_EffectiveNP_8__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_9__1up", "JET_JER_EffectiveNP_9__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_9__1up_PseudoData", "JET_JER_EffectiveNP_9__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_10__1up", "JET_JER_EffectiveNP_10__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_10__1up_PseudoData", "JET_JER_EffectiveNP_10__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_11__1up", "JET_JER_EffectiveNP_11__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_11__1up_PseudoData", "JET_JER_EffectiveNP_11__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_12restTerm__1up", "JET_JER_EffectiveNP_12restTerm__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_12restTerm__1up_PseudoData", "JET_JER_EffectiveNP_12restTerm__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_OffsetMu__1up", "JET_Pileup_OffsetMu__1up", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_OffsetNPV__1up", "JET_Pileup_OffsetNPV__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_PtTerm__1up", "JET_Pileup_PtTerm__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_RhoTopology__1up", "JET_Pileup_RhoTopology__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_PunchThrough_MC16__1up", "JET_PunchThrough_MC16__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_SingleParticle_HighPt__1up", "JET_SingleParticle_HighPt__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_ALL__1up", "EG_RESOLUTION_ALL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_ALL__1up", "EG_SCALE_ALL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MET_SoftTrk_ScaleUp", "MET_SoftTrk_ScaleDown", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_CB__1up", "MUON_CB__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_RESBIAS__1up", "MUON_SAGITTA_RESBIAS__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SCALE__1up", "MUON_SCALE__1down", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPara", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPerp", "", "nominal", ifile, "", "");

        /// Luminosity
        syst_manager.AddNormSyst(ifile, 0.017, 0.017, "");

        /// MC stat
        syst_manager.AddMCstatSyst(ifile);
    }

    /// Add individual systematics
    syst_manager.AddOneSidedSyst("ttbar_PH7_FS","nominal","ttbar_PP8_FS", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddOneSidedSyst("ttbar_PP8_hdamp_FS","nominal","ttbar_PP8_FS", "nominal", "ttbar_PP8_FS", "", "");
    
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "MUR1_MUF05_PDF260000", "MUR1_MUF2_PDF260000", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "MUR05_MUF1_PDF260000", "MUR2_MUF1_PDF260000", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_FS", "", "");

    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "MUR1_MUF05_PDF260000", "MUR1_MUF2_PDF260000", "", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "MUR05_MUF1_PDF260000", "MUR2_MUF1_PDF260000", "", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");

    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "MUR1_MUF05_PDF260000", "MUR1_MUF2_PDF260000", "", "nominal", "SingleTop_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "MUR05_MUF1_PDF260000", "MUR2_MUF1_PDF260000", "", "nominal", "SingleTop_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "SingleTop_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "SingleTop_PP8_t_chan_FS", "", "");

    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "MUR1_MUF05_PDF260000", "MUR1_MUF2_PDF260000", "", "nominal", "SingleTop_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "MUR05_MUF1_PDF260000", "MUR2_MUF1_PDF260000", "", "nominal", "SingleTop_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "SingleTop_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "SingleTop_PP8_s_chan_FS", "", "");

    //syst_manager.AddOneSidedSyst("Multijet","Alt","Multijet", "nominal", "Multijet", "", "");

    /// PDF
    for (int i = 93301; i <= 93342; ++i) {
        const std::string pdfname = "MUR1_MUF1_PDF"+std::to_string(i);
        syst_manager.AddOneSidedSyst("ttbar_PP8_FS", pdfname, "ttbar_PP8_FS", "MUR1_MUF1_PDF93300", "ttbar_PP8_FS", "", "");
    }

    //syst_manager.AddNormSyst("ttbar_PP8_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_s_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_t_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_tW_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("Wjets_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Zjets_Sherpa_FS", 0.5, 0.5, "");
    //syst_manager.AddNormSyst("Diboson_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Multijet", 0.5, 0.5, "");

    /// Pass all systematics
    plotter.SetAllSysts(syst_manager.GetAllSystematics());

    static const std::vector<Common::LEPTONTYPE> lepton_types = {Common::LEPTONTYPE::EL, Common::LEPTONTYPE::MU};
    /// 1D Data/MC plots
    for (const auto& ilep : lepton_types) {
        plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("jet1_eta","Leading jet #eta [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet1_phi","Leading jet #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet1_e","Leading jet energy [GeV]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet2_pt","Second jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("jet2_pt","Second jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("jet2_eta","Second jet #eta [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet2_phi","Second jet #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet2_e","Second jet energy [GeV]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet3_pt","Third jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("jet3_pt","Third jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("jet3_eta","Third jet #eta [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet3_phi","Third jet #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet3_e","Third jet energy [GeV]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet4_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("jet4_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("jet4_eta","Fourth jet #eta [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet4_phi","Fourth jet #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet4_e","Fourth jet energy [GeV]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet5_pt","Fifth jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("jet5_pt","Fifth jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("jet5_eta","Fifth jet #eta [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet5_phi","Fifth jet #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet5_e","Fifth jet energy [GeV]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet6_pt","Sixth jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("jet6_pt","Sixth jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("jet6_eta","Sixth jet #eta [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet6_phi","Sixth jet #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet6_e","Sixth jet energy [GeV]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("HT","H_{T}^{jets} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("HT","H_{T}^{jets} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("jet_n","Jet multiplicity [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("jet_n","Jet multiplicity [-]", "Events", "", true, ilep);
        plotter.PlotDataMCPlots("bjet_pt_85","b-jet p_{T} @85 WP [GeV]", "Jets", "GeV", false, ilep);
        plotter.PlotDataMCPlots("bjet_pt_85","b-jet p_{T} @85 WP [GeV]", "Jets", "GeV", true, ilep);
        plotter.PlotDataMCPlots("bjet_eta_85","b-jet #eta @85 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_phi_85","b-jet #phi @85 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_e_85","b-jet energy @85 WP [GeV]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_85","b-jet multiplicity @85 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_85","b-jet multiplicity @85 WP [-]", "Jets", "", true, ilep);
        plotter.PlotDataMCPlots("bjet_pt_77","b-jet p_{T} @77 WP [GeV]", "Jets", "GeV", false, ilep);
        plotter.PlotDataMCPlots("bjet_pt_77","b-jet p_{T} @77 WP [GeV]", "Jets", "GeV", true, ilep);
        plotter.PlotDataMCPlots("bjet_eta_77","b-jet #eta @77 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_phi_77","b-jet #phi @77 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_e_77","b-jet energy @77 WP [GeV]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_77","b-jet multiplicity @77 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_77","b-jet multiplicity @77 WP [-]", "Jets", "", true, ilep);
        plotter.PlotDataMCPlots("bjet_pt_70","b-jet p_{T} @70 WP [GeV]", "Jets", "GeV", false, ilep);
        plotter.PlotDataMCPlots("bjet_pt_70","b-jet p_{T} @70 WP [GeV]", "Jets", "GeV", true, ilep);
        plotter.PlotDataMCPlots("bjet_eta_70","b-jet #eta @70 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_phi_70","b-jet #phi @70 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_e_70","b-jet energy @70 WP [GeV]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_70","b-jet multiplicity @70 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_70","b-jet multiplicity @70 WP [-]", "Jets", "", true, ilep);
        plotter.PlotDataMCPlots("bjet_pt_60","b-jet p_{T} @60 WP [GeV]", "Jets", "GeV", false, ilep);
        plotter.PlotDataMCPlots("bjet_pt_60","b-jet p_{T} @60 WP [GeV]", "Jets", "GeV", true, ilep);
        plotter.PlotDataMCPlots("bjet_eta_60","b-jet #eta @60 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_phi_60","b-jet #phi @60 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_e_60","b-jet energy @60 WP [GeV]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_60","b-jet multiplicity @60 WP [-]", "Jets", "", false, ilep);
        plotter.PlotDataMCPlots("bjet_n_60","b-jet multiplicity @60 WP [-]", "Jets", "", true, ilep);
        plotter.PlotDataMCPlots("jet_tagbin"," Jet tag bin [-]", "Jets", "", true, ilep);

        plotter.PlotDataMCPlots("lepton_pt","Lepton p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("lepton_pt","Lepton p_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("lepton_eta","Lepton #eta [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("lepton_phi","Lepton #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("met_x","E_{T,x}^{miss} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("met_x","E_{T,x}^{miss} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("met_y","E_{T,y}^{miss} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("met_y","E_{T,y}^{miss} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("met_sumet","sum E_{T} [GeV]", "Events", "GeV", true, ilep);
        plotter.PlotDataMCPlots("met_sumet","sum E_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("mwt","M_{T}^{W} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("met_phi","E_{T}^{miss} #phi [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("mu","average #mu [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("mu_actual","actual #mu [-]", "Events", "", false, ilep);
        plotter.PlotDataMCPlots("reco_W_mass","hadronic W mass [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("reco_top_mass","hadronic top-quark mass [GeV]", "Events", "GeV", false, ilep);

        /// Make html table with yields
        plotter.CalculateYields(ilep);
    }

    plotter.CloseRootFiles();

    /// Successful run
    return 0;
}
