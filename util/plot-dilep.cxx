#include "TTbarXsec/Plotter.h"
#include "TTbarXsec/SystHistoManager.h"

#include "TError.h"

#include <iostream>
#include <string>
#include <vector>

int main (int argc, const char *argv[]) {
    if (argc != 2 && argc != 3) {
        std::cerr << "Expecting one or two arguments but " << argc-1 << " arguments provided" << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::string folder = argv[1];
    bool use_syst(false);
    if (argc == 3) {
        const std::string syst_param = argv[2];
        if (syst_param == "syst") {
            use_syst = true;
        } else {
            std::cerr << "Third argument can only be 'syst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    gErrorIgnoreLevel = kError;

    Plotter plotter(folder, Common::CHANNEL::DILEPTON);

    static const std::vector<std::string> ttbar_names = {"ttbar_PP8_dilep_FS"};
    //static const std::vector<std::string> ttbar_names = {"ttbar_Sherpa_FS"};

    static const std::vector<std::string> bkg_names = {"ttV_Sherpa_FS",
                                                       "SingleTop_PP8_tW_chan_dilep_FS",
                                                       //"Zjets_PP8_FS",
                                                       "Diboson_Sherpa_FS",
                                                       "Multijet"};

    static const std::vector<std::string> special_names = {"ttbar_PP8_hdamp_FS",
                                                           "ttbar_PH7_FS",
                                                           "SingleTop_DS_PP8_tW_chan_FS",
                                                           "Zjets_Sherpa_FS"
                                                           };
    plotter.SetTTbarNames(ttbar_names);
    plotter.SetZNames({"Zjets_Sherpa_FS"});
    plotter.SetBackgroundNames(bkg_names);
    plotter.SetSpecialNames(special_names);
    plotter.SetDataName("Data");
    plotter.SetAtlasLabel("Internal");
    plotter.SetLumiLabel("29 fb^{-1}");
    //plotter.SetLumiLabel("4.2 fb^{-1}");
    plotter.SetCollection("");
    plotter.SetNominalTTbarName("ttbar_PP8_dilep_FS");
    plotter.SetNominalZName("Zjets_Sherpa_FS");
    //plotter.SetNominalTTbarName("ttbar_Sherpa_FS");
    //plotter.SetSystShapeOnly(true);
    plotter.SetSystShapeOnly(false);
    plotter.SetRunSyst(use_syst);
    plotter.SetScale(29.0493);
    plotter.SetStoreDileptonPt(false);
    plotter.SetStoreLeadingLeptonPt(false);
    plotter.SetRegionNames(Common::GetRegionString(Common::CHANNEL::DILEPTON), Common::GetRegionString(Common::CHANNEL::DILEPTON, Common::LEPTONTYPE::EMU));
    //plotter.SetRegionNames({}, Common::GetRegionString(Common::CHANNEL::DILEPTON, Common::LEPTONTYPE::EMU));

    plotter.OpenRootFiles();

    /// Set all systematics
    SystHistoManager syst_manager{};

    std::vector<std::string> all(bkg_names);
    all.emplace_back("Zjets_Sherpa_FS");
    all.emplace_back(plotter.GetNominalTTbarName());

    /// Set tree systematics
    for (const auto& ifile : all) {
        if (ifile == "Multijet") continue;
        for (int ib = 0; ib <= 8; ++ib) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_B_"+std::to_string(ib)+"_up", "BTAG_B_"+std::to_string(ib)+"_down", "", "nominal", ifile, "", "");
        }
        for (int ic = 0; ic < 2; ++ic) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_C_"+std::to_string(ic)+"_up", "BTAG_C_"+std::to_string(ic)+"_down", "", "nominal", ifile, "", "");
        }
        for (int il = 0; il < 3; ++il) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Light_"+std::to_string(il)+"_up", "BTAG_Light_"+std::to_string(il)+"_down", "", "nominal", ifile, "", "");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Extrapolation_from_charm_up", "BTAG_Extrapolation_from_charm_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_trigger_up", "el_trigger_down", "", "nominal", ifile, "","");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_reco_up", "el_reco_down", "", "nominal", ifile, "", "");
        for (int i = 1; i <= 33; ++i ) {
          syst_manager.AddTwoSidedSyst(ifile, "", "el_simplified_id_"+std::to_string(i)+"_up", "el_simplified_id_"+std::to_string(i)+"_down", "", "nominal", ifile, "", "");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "el_isol_up", "el_isol_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_stat_up", "mu_trigger_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_syst_up", "mu_trigger_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_up", "mu_id_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_lowpt_up", "mu_id_stat_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_bkg_fraction_up", "mu_id_bkg_fraction_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_fit_model_lowpt_up", "mu_id_fit_model_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_lumi_uncert_up", "mu_id_lumi_uncert_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_matching_up", "mu_id_matching_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_matching_lowpt_up", "mu_id_matching_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_mc_xsec_up", "mu_id_mc_xsec_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_pt_dependency_up", "mu_id_pt_dependency_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_qcd_template_up", "mu_id_qcd_template_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_supression_scale_up", "mu_id_supression_scale_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_truth_up", "mu_id_truth_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_truth_lowpt_up", "mu_id_truth_lowpt_up", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_cr1_up", "mu_id_cr1_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_cr2_up", "mu_id_cr2_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_cr3_up", "mu_id_cr3_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_higheta_probeip_up", "mu_id_higheta_probeip_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_higheta_probeiso_up", "mu_id_higheta_probeiso_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_tagpt_up", "mu_id_tagpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_extrapolation_up", "mu_id_extrapolation_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_stat_up", "mu_isol_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_bkg_fraction_up", "mu_isol_bkg_fraction_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_drmuj_up", "mu_isol_drmuj_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_lumi_uncert_up", "mu_isol_lumi_uncert_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_xsec_up", "mu_isol_xsec_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_mllwindow_up", "mu_isol_mllwindow_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_qcd_template_up", "mu_isol_qcd_template_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_supression_scale_up", "mu_isol_supression_scale_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_extrapolation_up", "mu_isol_extrapolation_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_stat_up", "mu_ttva_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_bkg_fraction_up", "mu_ttva_bkg_fraction_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_lumi_uncert_up", "mu_ttva_lumi_uncert_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_mc_xsec_up", "mu_ttva_mc_xsec_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_qcd_template_up", "mu_ttva_qcd_template_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_supression_scale_up", "mu_ttva_supression_scale_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_extrapolation_up", "mu_ttva_extrapolation_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "jvt_custom_up", "jvt_custom_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "pileup_up", "pileup_down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "JET_BJES_Response__1up", "JET_BJES_Response__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Detector1__1up", "JET_EffectiveNP_Detector1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Detector2__1up", "JET_EffectiveNP_Detector2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed1__1up", "JET_EffectiveNP_Mixed1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed2__1up", "JET_EffectiveNP_Mixed2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed3__1up", "JET_EffectiveNP_Mixed3__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling1__1up", "JET_EffectiveNP_Modelling1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling2__1up", "JET_EffectiveNP_Modelling2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling3__1up", "JET_EffectiveNP_Modelling3__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling4__1up", "JET_EffectiveNP_Modelling4__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical1__1up", "JET_EffectiveNP_Statistical1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical2__1up", "JET_EffectiveNP_Statistical2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical3__1up", "JET_EffectiveNP_Statistical3__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical4__1up", "JET_EffectiveNP_Statistical4__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical5__1up", "JET_EffectiveNP_Statistical5__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical6__1up", "JET_EffectiveNP_Statistical6__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_Modelling__1up", "JET_EtaIntercalibration_Modelling__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_NonClosure_PreRec__1up", "JET_EtaIntercalibration_NonClosure_PreRec__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_TotalStat__1up", "JET_EtaIntercalibration_TotalStat__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Flavor_Composition__1up", "JET_Flavor_Composition__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Flavor_Response__1up", "JET_Flavor_Response__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_InSitu_NonClosure_PreRec__1up", "JET_InSitu_NonClosure_PreRec__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JESUnc_mc20vsmc21_MC21_PreRec__1up", "JET_JESUnc_mc20vsmc21_MC21_PreRec__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JESUnc_Noise_PreRec__1up", "JET_JESUnc_Noise_PreRec__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JESUnc_VertexingAlg_PreRec__1up", "JET_JESUnc_VertexingAlg_PreRec__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_OffsetMu__1up", "JET_Pileup_OffsetMu__1up", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_OffsetNPV__1up", "JET_Pileup_OffsetNPV__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_PtTerm__1up", "JET_Pileup_PtTerm__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_Pileup_RhoTopology__1up", "JET_Pileup_RhoTopology__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_PunchThrough_MC16__1up", "JET_PunchThrough_MC16__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_SingleParticle_HighPt__1up", "JET_SingleParticle_HighPt__1down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_DataVsMC_MC16__1up", "JET_JER_DataVsMC_MC16__1down", "", "nominal", ifile, "", "", "JET_JER_DataVsMC_MC16__1up_PseudoData", "JET_JER_DataVsMC_MC16__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_1__1up", "JET_JER_EffectiveNP_1__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_1__1up_PseudoData", "JET_JER_EffectiveNP_1__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_2__1up", "JET_JER_EffectiveNP_2__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_2__1up_PseudoData", "JET_JER_EffectiveNP_2__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_3__1up", "JET_JER_EffectiveNP_3__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_3__1up_PseudoData", "JET_JER_EffectiveNP_3__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_4__1up", "JET_JER_EffectiveNP_4__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_4__1up_PseudoData", "JET_JER_EffectiveNP_4__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_5__1up", "JET_JER_EffectiveNP_5__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_5__1up_PseudoData", "JET_JER_EffectiveNP_5__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_6__1up", "JET_JER_EffectiveNP_6__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_6__1up_PseudoData", "JET_JER_EffectiveNP_6__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_7__1up", "JET_JER_EffectiveNP_7__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_7__1up_PseudoData", "JET_JER_EffectiveNP_7__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_8__1up", "JET_JER_EffectiveNP_8__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_8__1up_PseudoData", "JET_JER_EffectiveNP_8__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_9__1up", "JET_JER_EffectiveNP_9__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_9__1up_PseudoData", "JET_JER_EffectiveNP_9__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_10__1up", "JET_JER_EffectiveNP_10__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_10__1up_PseudoData", "JET_JER_EffectiveNP_10__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_11__1up", "JET_JER_EffectiveNP_11__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_11__1up_PseudoData", "JET_JER_EffectiveNP_11__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_12restTerm__1up", "JET_JER_EffectiveNP_12restTerm__1down", "", "nominal", ifile, "", "", "JET_JER_EffectiveNP_12restTerm__1up_PseudoData", "JET_JER_EffectiveNP_12restTerm__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JERUnc_mc20vsmc21_MC21_PreRec__1up", "JET_JERUnc_mc20vsmc21_MC21_PreRec__1down", "", "nominal", ifile, "", "", "JET_JERUnc_mc20vsmc21_MC21_PreRec__1up_PseudoData", "JET_JERUnc_mc20vsmc21_MC21_PreRec__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JERUnc_Noise_PreRec__1up", "JET_JERUnc_Noise_PreRec__1down", "", "nominal", ifile, "", "", "JET_JERUnc_Noise_PreRec__1up_PseudoData", "JET_JERUnc_Noise_PreRec__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "JET_JERUnc_Noise_PreRec__1up", "JET_JERUnc_Noise_PreRec__1down", "", "nominal", ifile, "", "", "JET_JERUnc_Noise_PreRec__1up_PseudoData", "JET_JERUnc_Noise_PreRec__1down_PseudoData");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_MATERIALCALO__1up", "EG_RESOLUTION_MATERIALCALO__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_MATERIALCRYO__1up", "EG_RESOLUTION_MATERIALCRYO__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_MATERIALGAP__1up", "EG_RESOLUTION_MATERIALGAP__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_MATERIALIBL__1up", "EG_RESOLUTION_MATERIALIBL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_MATERIALID__1up", "EG_RESOLUTION_MATERIALID__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_MATERIALPP0__1up", "EG_RESOLUTION_MATERIALPP0__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_OFC__1up", "EG_RESOLUTION_OFC__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_PILEUP__1up", "EG_RESOLUTION_PILEUP__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_SAMPLINGTERM__1up", "EG_RESOLUTION_PILEUP__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_ZSMEARING__1up", "EG_RESOLUTION_ZSMEARING__1down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_E4SCINTILLATOR__ETABIN0__1up", "EG_SCALE_E4SCINTILLATOR__ETABIN0__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_E4SCINTILLATOR__ETABIN1__1up", "EG_SCALE_E4SCINTILLATOR__ETABIN1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_E4SCINTILLATOR__ETABIN2__1up", "EG_SCALE_E4SCINTILLATOR__ETABIN2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_G4__1up", "EG_SCALE_G4__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_L1GAIN__1up", "EG_SCALE_L1GAIN__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_L2GAIN__1up", "EG_SCALE_L2GAIN__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_LARCALIB__ETABIN0__1up", "EG_SCALE_LARCALIB__ETABIN0__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_LARCALIB__ETABIN1__1up", "EG_SCALE_LARCALIB__ETABIN1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_LARELECCALIB__1up", "EG_SCALE_LARELECCALIB__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_LARELECUNCONV__ETABIN0__1up", "EG_SCALE_LARELECUNCONV__ETABIN0__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_LARELECUNCONV__ETABIN1__1up", "EG_SCALE_LARELECUNCONV__ETABIN1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_LARUNCONVCALIB__ETABIN0__1up", "EG_SCALE_LARUNCONVCALIB__ETABIN0__1up", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_LARUNCONVCALIB__ETABIN1__1up", "EG_SCALE_LARUNCONVCALIB__ETABIN1__1up", "", "nominal", ifile, "", "");
        for (int i = 0; i <= 11; ++i) {
          syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATCALO__ETABIN"+std::to_string(i)+"__1up", "EG_SCALE_MATCALO__ETABIN"+std::to_string(i)+"__1down", "", "nominal", ifile, "", "");
          syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATCRYO__ETABIN"+std::to_string(i)+"__1up", "EG_SCALE_MATCRYO__ETABIN"+std::to_string(i)+"__1down", "", "nominal", ifile, "", "");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATID__ETABIN0__1up", "EG_SCALE_MATID__ETABIN0__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATID__ETABIN1__1up", "EG_SCALE_MATID__ETABIN1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATID__ETABIN2__1up", "EG_SCALE_MATID__ETABIN2__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATID__ETABIN3__1up", "EG_SCALE_MATID__ETABIN3__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATPP0__ETABIN0__1up", "EG_SCALE_MATPP0__ETABIN0__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_MATPP0__ETABIN1__1up", "EG_SCALE_MATPP0__ETABIN1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_OFC__1up", "EG_SCALE_OFC__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_PEDESTAL__1up", "EG_SCALE_PEDESTAL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_PS_BARREL_B12__1up", "EG_SCALE_PS_BARREL_B12__1down", "", "nominal", ifile, "", "");
        for (int i = 0; i <= 8; ++i) {
          syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_PS__ETABIN"+std::to_string(i)+"__1up", "EG_SCALE_PS__ETABIN"+std::to_string(i)+"__1down", "", "nominal", ifile, "", "");
        }
        for (int i = 0; i <= 4; ++i) {
          syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_S12__ETABIN"+std::to_string(i)+"__1up", "EG_SCALE_S12__ETABIN"+std::to_string(i)+"__1down", "", "nominal", ifile, "", "");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_TOPOCLUSTER_THRES__1up", "EG_SCALE_TOPOCLUSTER_THRES__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_WTOTS1__1up", "EG_SCALE_WTOTS1__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_ZEESTAT__1up", "EG_SCALE_ZEESTAT__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_ZEESYST__1up", "EG_SCALE_ZEESYST__1down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_CB__1up", "MUON_CB__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_RESBIAS__1up", "MUON_SAGITTA_RESBIAS__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_DATASTAT__1up", "MUON_SAGITTA_DATASTAT__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_GLOBAL__1up", "MUON_SAGITTA_GLOBAL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_PTEXTRA__1up", "MUON_SAGITTA_PTEXTRA__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SCALE_CB__1up", "MUON_SCALE_CB__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SCALE_CB_ELOSS__1up", "MUON_SCALE_CB_ELOSS__1down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "MET_SoftTrk_Scale__1up", "MET_SoftTrk_Scale__1down", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPara", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPerp", "", "nominal", ifile, "", "");

        // Luminosity
        syst_manager.AddNormSyst(ifile, 0.022, 0.022, "");

        // MC stat
        syst_manager.AddMCstatSyst(ifile);
    }

    /// Add individual systematics
    syst_manager.AddOneSidedSyst("ttbar_PH7_FS","nominal","ttbar_PP8_dilep_FS", "nominal", "ttbar_PP8_dilep_FS", "", "");
    syst_manager.AddOneSidedSyst("ttbar_PP8_hdamp_FS","nominal","ttbar_PP8_dilep_FS", "nominal", "ttbar_PP8_dilep_FS", "", "");
    syst_manager.AddOneSidedSyst("SingleTop_DS_PP8_tW_chan_FS","nominal","SingleTop_PP8_tW_chan_dilep_FS", "nominal", "SingleTop_PP8_tW_chan_dilep_FS", "", "");
    
    //syst_manager.AddOneSidedSyst("Zjets_Sherpa_FS","nominal","Zjets_PP8_FS", "nominal", "Zjets_PP8_FS", "", "");
    
    //syst_manager.AddTwoSidedSyst("Zjets_PP8_FS","","MUR05_MUF1_PDF10800", "MUR2_MUF1_PDF10800", "", "nominal", "Zjets_PP8_FS", "", "");
    //syst_manager.AddTwoSidedSyst("Zjets_PP8_FS","","MUR1_MUF05_PDF10800", "MUR1_MUF2_PDF10800", "", "nominal", "Zjets_PP8_FS", "", "");
    //syst_manager.AddTwoSidedSyst("Zjets_PP8_FS","","isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "Zjets_PP8_FS", "", "");
    //syst_manager.AddTwoSidedSyst("Zjets_PP8_FS","","isrmuRfac20_fsrmuRfac10", "isrmuRfac05_fsrmuRfac10", "", "nominal", "Zjets_PP8_FS", "", "");
    
    syst_manager.AddOneSidedSyst("Zjets_Sherpa_FS","MUR2_MUF2_PDF303200_PSMUR2_PSMUF2", "Zjets_Sherpa_FS", "nominal", "Zjets_Sherpa_FS", "", "");

    syst_manager.AddTwoSidedSyst("ttbar_PP8_dilep_FS", "", "MUR1_MUF05_PDF260000", "MUR1_MUF2_PDF260000", "", "nominal", "ttbar_PP8_dilep_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_dilep_FS", "", "MUR05_MUF1_PDF260000", "MUR2_MUF1_PDF260000", "", "nominal", "ttbar_PP8_dilep_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_dilep_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_dilep_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_dilep_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_dilep_FS", "", "");
    syst_manager.AddOneSidedSyst("ttbar_PP8_dilep_FS","NNLO_PT", "ttbar_PP8_dilep_FS", "nominal", "ttbar_PP8_dilep_FS", "", "");

    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_dilep_FS", "", "MUR1_MUF05_PDF260000", "MUR1_MUF2_PDF260000", "", "nominal", "SingleTop_PP8_tW_chan_dilep_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_dilep_FS", "", "MUR05_MUF1_PDF260000", "MUR2_MUF1_PDF260000", "", "nominal", "SingleTop_PP8_tW_chan_dilep_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_dilep_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "SingleTop_PP8_tW_chan_dilep_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_dilep_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "SingleTop_PP8_tW_chan_dilep_FS", "", "");

    /// PDF
    for (int i = 93301; i <= 93342; ++i) {
        const std::string pdfname = "MUR1_MUF1_PDF"+std::to_string(i);
        syst_manager.AddOneSidedSyst("ttbar_PP8_dilep_FS", pdfname, "ttbar_PP8_dilep_FS", "MUR1_MUF1_PDF93300", "ttbar_PP8_dilep_FS", "", "");
        //syst_manager.AddOneSidedSyst("Zjets_PP8_FS", pdfname, "ttbar_PP8_dilep_FS", "MUR1_MUF1_PDF93300", "ttbar_PP8_dilep_FS", "", "");
    }

    //syst_manager.AddNormSyst("ttbar_PP8_dilep_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_tW_chan_dilep_FS", 0.035, 0.035, "");
    //syst_manager.AddNormSyst("Zjets_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Diboson_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Multijet", 0.99, 0.99, "");

    /// Pass all systematics
    plotter.SetAllSysts(syst_manager.GetAllSystematics());

    //static const std::vector<Common::LEPTONTYPE> lepton_types = {Common::LEPTONTYPE::EE, Common::LEPTONTYPE::MUMU, Common::LEPTONTYPE::EMU};
    //static const std::vector<Common::LEPTONTYPE> lepton_types = {Common::LEPTONTYPE::EMU};
    static const std::vector<Common::LEPTONTYPE> lepton_types = {Common::LEPTONTYPE::MUMU};
    //static const std::vector<Common::LEPTONTYPE> lepton_types = {Common::LEPTONTYPE::EE};
    /// 1D Data/MC plots
    for (const auto& ilep : lepton_types) {
        //plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet1_eta","Leading jet #eta", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet1_phi","Leading jet #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet1_e","Leading jet energy [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet1_e","Leading jet energy [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet2_pt","Second jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet2_pt","Second jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet2_eta","Second jet #eta", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet2_phi","Second jet #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet2_e","Second jet energy [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet2_e","Second jet energy [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet3_pt","Third jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet3_pt","Third jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet3_eta","Third jet #eta", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet3_phi","Third jet #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet3_e","Third jet energy [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet3_e","Third jet energy [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet4_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet4_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet4_eta","Fourth jet #eta", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet4_phi","Fourth jet #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet4_e","Fourth jet energy [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("jet4_e","Fourth jet energy [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("HT","H_{T}^{jets} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("HT","H_{T}^{jets} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("jet_n","Jet multiplicity", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet_n","Jet multiplicity", "Events", "", true, ilep);
        //plotter.PlotDataMCPlots("bjet_n_77","b-tagged 77% WP jet multiplicity", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("bjet_n_77","b-tagged 77% WP jet multiplicity", "Events", "", true, ilep);
        //plotter.PlotDataMCPlots("jet_tagbin","DL1d b-tag bin", "Entries", "", false, ilep);
        //plotter.PlotDataMCPlots("jet_tagbin","DL1d b-tag bin", "Entries", "", true, ilep);
        //plotter.PlotDataMCPlots("jet1_tagbin","Leading jet DL1d b-tag bin", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet1_tagbin","Leading jet DL1d b-tag bin", "Events", "", true, ilep);
        //plotter.PlotDataMCPlots("jet2_tagbin","Second jet DL1d b-tag bin", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet2_tagbin","Second jet DL1d b-tag bin", "Events", "", true, ilep);
        //plotter.PlotDataMCPlots("jet3_tagbin","Third jet DL1d b-tag bin", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet3_tagbin","Third jet DL1d b-tag bin", "Events", "", true, ilep);
        //plotter.PlotDataMCPlots("jet4_tagbin","Fourth jet DL1d b-tag bin", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("jet4_tagbin","Fourth jet DL1d b-tag bin", "Events", "", true, ilep);
        //plotter.PlotDataMCPlots("bjet_pt_77","b-tagged 77 WP jet p_{T} [GeV]", "Jets", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("bjet_pt_77","b-tagged 77 WP jet p_{T} [GeV]", "Jets", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("bjet_eta_77","b-tagged 77 WP jet #eta", "Jets", "", false, ilep);
        //plotter.PlotDataMCPlots("bjet_phi_77","b-tagged 77 WP jet #phi", "Jets", "", false, ilep);
        //plotter.PlotDataMCPlots("bjet_e_77","b-tagged 77 WP jet energy [GeV]", "Jets", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("bjet_e_77","b-tagged 77 WP jet energy [GeV]", "Jets", "GeV", true, ilep);
        plotter.PlotDataMCPlots("lepton1_pt","Leading lepton p_{T} [GeV]", "Events", "GeV", false, ilep);
        plotter.PlotDataMCPlots("lepton1_pt","Leading lepton p_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("lepton1_eta","Leading lepton #eta", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("lepton1_phi","Leading lepton #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("lepton1_e","Leading lepton energy [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("lepton1_e","Leading lepton energy [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("lepton2_pt","Second lepton p_{T} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("lepton2_pt","Second lepton p_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("lepton2_eta","Second lepton #eta", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("lepton2_phi","Second lepton #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("lepton2_e","Second lepton energy [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("lepton2_e","Second lepton energy [GeV]", "Events", "GeV", true, ilep);
        //if (ilep == Common::LEPTONTYPE::EMU) {
        //    plotter.PlotDataMCPlots("electron_pt","Electron p_{T} [GeV]", "Events", "GeV", false, ilep);
        //    plotter.PlotDataMCPlots("electron_pt","Electron p_{T} [GeV]", "Events", "GeV", true, ilep);
        //    plotter.PlotDataMCPlots("electron_eta","Electron #eta", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("electron_phi","Electron #phi", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("electron_e","Electron energy [GeV]", "Events", "GeV", false, ilep);
        //    plotter.PlotDataMCPlots("electron_e","Electron energy [GeV]", "Events", "GeV", true, ilep);
        //    plotter.PlotDataMCPlots("muon_pt","Muon p_{T} [GeV]", "Events", "GeV", false, ilep);
        //    plotter.PlotDataMCPlots("muon_pt","Muon p_{T} [GeV]", "Events", "GeV", true, ilep);
        //    plotter.PlotDataMCPlots("muon_eta","Muon #eta", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("muon_phi","Muon #phi", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("muon_e","Muon energy [GeV]", "Events", "GeV", false, ilep);
        //    plotter.PlotDataMCPlots("muon_e","Muon energy [GeV]", "Events", "GeV", true, ilep);
        //} else {
        //    plotter.PlotDataMCPlots("jet_Z_balance_pt_17_30","jet p_{T} / Z p_{T}, 17 < Z p_{T} < 30 GeV", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("jet_Z_balance_pt_30_50","jet p_{T} / Z p_{T}, 30 < Z p_{T} < 50 GeV", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("jet_Z_balance_pt_50_100","jet p_{T} / Z p_{T}, 50 < Z p_{T} < 100 GeV", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("jet_Z_balance_pt_100_200","jet p_{T} / Z p_{T}, 100 < Z p_{T} < 200 GeV", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("jet_Z_balance_pt_200_inf","jet p_{T} / Z p_{T}, 200 < Z p_{T}", "Events", "", false, ilep);
        //    plotter.PlotDataMCPlots("jet_Z_balance_jet_pt","jet p_{T} [GeV]", "Events", "GeV", false, ilep);
        //    plotter.PlotDataMCPlots("jet_Z_balance_Z_pt","Z p_{T} [GeV]", "Events", "GeV", false, ilep);
        //}
        //plotter.PlotDataMCPlots("dilepton_m","Dilepton mass [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_m_peak","Dilepton mass [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_m_peak_5bins","Dilepton mass [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_m_peak_10bins","Dilepton mass [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_m_peak_15bins","Dilepton mass [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_m_peak_20bins","Dilepton mass [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_m_peak_25bins","Dilepton mass [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_deltaR","Dilepton #Delta R", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_deltaPhi","Dilepton #Delta #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_pt","Dilepton p_{T} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("dilepton_pt","Dilepton p_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("met_x","E_{T,x}^{miss} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("met_x","E_{T,x}^{miss} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("met_y","E_{T,y}^{miss} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("met_y","E_{T,y}^{miss} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("met_phi","E_{T}^{miss} #phi", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("met_sumet","sum E_{T} [GeV]", "Events", "GeV", false, ilep);
        //plotter.PlotDataMCPlots("met_sumet","sum E_{T} [GeV]", "Events", "GeV", true, ilep);
        //plotter.PlotDataMCPlots("mu","average #mu", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("mu_actual","actual #mu ", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("n_1bjet","1 b-tag events", "Events", "", false, ilep);
        //plotter.PlotDataMCPlots("n_2bjet","2 b-tag events", "Events", "", false, ilep);
        
        /// Make html table with yields
        plotter.CalculateYields(ilep);
    }

    plotter.CloseRootFiles();

    /// Successful run
    return 0;
}
