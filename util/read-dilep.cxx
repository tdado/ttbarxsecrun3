#include "TTbarXsec/FileProcessorDilep.h"

#include "TError.h"
#include "TH1.h"
#include "TROOT.h"

#include <iostream>

int main(int argc, const char *argv[]) {

    TH1::SetDefaultSumw2();
    TH1::AddDirectory(kFALSE);

    if (argc == 1 || argc > 5) {
        std::cerr << "1, 2, 3 or 4 parameters allowed, but " << argc-1 << " parameters provided \n";
        exit(EXIT_FAILURE);
    }

    gErrorIgnoreLevel = kError;

    std::string dataset_type("full");
    std::string systematics("nominal");
    std::string parallelSyst("");

    std::string name =  argv[1];
    if (argc > 2) dataset_type = argv[2];
    if (argc > 3) {
        systematics = argv[3];
        if (systematics != "nominal" && systematics != "syst" && systematics != "sfsyst") {
            std::cerr << "Last parameter has to be 'nominal', 'syst' or 'sfsyst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    if (argc > 4) {
        parallelSyst = argv[4];
    }

    ROOT::EnableThreadSafety();

    FileProcessorDilep processor(name, dataset_type, systematics, parallelSyst);

    processor.ReadConfig();
    
    processor.ProcessAllFiles(DileptonMatcher::MATCHINGOPTION::DELTAR);

    std::cout << "\nmain: Everything went fine\n";

    return 0;
}
