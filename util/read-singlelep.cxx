#include "TTbarXsec/FileProcessorSingleLep.h"

#include "TError.h"
#include "TH1.h"
#include "TROOT.h"

#include <iostream>

int main(int argc, const char *argv[]) {

    TH1::SetDefaultSumw2();
    TH1::AddDirectory(kFALSE);

    if (argc == 1 || argc > 4) {
        std::cerr << "1, 2 or 3 parameters allowed, but " << argc-1 << " parameters provided \n";
        exit(EXIT_FAILURE);
    }

    gErrorIgnoreLevel = kError;

    std::string dataset_type("full");
    std::string systematics("nominal");

    std::string name =  argv[1];
    if (argc > 2) dataset_type = argv[2];
    if (argc > 3) {
        systematics = argv[3];
        if (systematics != "nominal" && systematics != "syst" && systematics != "sfsyst") {
            std::cerr << "Last parameter has to be 'nominal', 'syst' or 'sfsyst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    
    ROOT::EnableThreadSafety();

    FileProcessorSingleLep processor(name, dataset_type, systematics);

    processor.ReadConfig();
    
    processor.ProcessAllFiles();

    std::cout << "\nmain: Everything went fine\n";

    return 0;
}
