#include "TTbarXsec/Comparator.h"

#include "TError.h"

#include <iostream>

void PrintUsage() {
    std::cout << "You need to pass 6 arguments in this order: \n";
    std::cout << "1. Folder name of the first histo\n";
    std::cout << "2. File name of the first histo\n";
    std::cout << "3. Histo name of the first histo\n";
    std::cout << "4. (OPTIONAL) Histo name of the first histo (can be used for up/down)\n";
    std::cout << "5. Folder name of the second histo\n";
    std::cout << "6. File name of the second histo\n";
    std::cout << "7. Histo name of the second histo\n";
    std::cout << "8. Lepton type. Can be 'ee', 'emu', 'mumu', 'el' or 'mu'\n";
}

int main (int argc, const char *argv[]) {
    if (argc != 8 && argc != 9) {
        PrintUsage();
        exit(EXIT_FAILURE);
    }

    std::string folder1;
    std::string file1;
    std::string name1;
    std::string name1_dn;
    std::string folder2;
    std::string file2;
    std::string name2;
    std::string channel;
    if (argc == 8) {
        folder1  = argv[1];
        file1    = argv[2];
        name1    = argv[3];
        folder2  = argv[4];
        file2    = argv[5];
        name2    = argv[6];
        channel  = argv[7];
        name1_dn = "";
    } else if (argc == 9) {
        folder1  = argv[1];
        file1    = argv[2];
        name1    = argv[3];
        name1_dn = argv[4];
        folder2  = argv[5];
        file2    = argv[6];
        name2    = argv[7];
        channel  = argv[8];
    }

    gErrorIgnoreLevel = kError;
    
    Comparator comparator{};

    if (channel == "ee") {
        comparator.SetLeptonType(Common::LEPTONTYPE::EE);
        comparator.SetRegionNames(Common::GetRegionString(Common::CHANNEL::DILEPTON));
    } else if (channel == "mumu") {
        comparator.SetLeptonType(Common::LEPTONTYPE::MUMU);
        comparator.SetRegionNames(Common::GetRegionString(Common::CHANNEL::DILEPTON));
    } else if (channel == "emu") {
        comparator.SetLeptonType(Common::LEPTONTYPE::EMU);
        comparator.SetRegionNames(Common::GetRegionString(Common::CHANNEL::DILEPTON, Common::LEPTONTYPE::EMU));
    } else if (channel == "el") {
        comparator.SetLeptonType(Common::LEPTONTYPE::EL);
        comparator.SetRegionNames(Common::GetRegionString(Common::CHANNEL::SINGLELEPTON));
    } else if (channel == "mu") {
        comparator.SetLeptonType(Common::LEPTONTYPE::MU);
        comparator.SetRegionNames(Common::GetRegionString(Common::CHANNEL::SINGLELEPTON));
    } else if (channel == "elmu") {
        comparator.SetLeptonType(Common::LEPTONTYPE::ELMU);
        comparator.SetRegionNames(Common::GetRegionString(Common::CHANNEL::SINGLELEPTON));
    } else {
        std::cerr << "The last argument has to be 'ee', 'emu', 'mumu', 'el', 'mu' or 'elmu' " << std::endl;
        exit(EXIT_FAILURE);
    }

    comparator.SetFirstHisto(folder1, file1, name1, name1_dn);
    comparator.SetSecondHisto(folder2, file2, name2);
    comparator.SetNormalise(false);

    comparator.OpenRootFiles();
    
    comparator.GetHistoList();
    
    comparator.PlotAllHistos();
    
    comparator.CloseRootFiles();
}
