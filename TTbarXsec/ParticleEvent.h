//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jan 30 15:12:15 2023 by ROOT version 6.27/01
// from TTree particleLevel/tree
// found on file: output.root
//////////////////////////////////////////////////////////

#ifndef PARTICLEEVENT_h
#define PARTICLEEVENT_h

#include <TChain.h>
#include <TFile.h>

#include <vector>

class ParticleEvent {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         weight_mc;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   Float_t         weight_pileup;
   Float_t         weight_beamspot;
   std::vector<float>   *el_pt;
   std::vector<float>   *el_eta;
   std::vector<float>   *el_phi;
   std::vector<float>   *el_e;
   std::vector<float>   *el_charge;
   std::vector<int>     *el_true_type;
   std::vector<int>     *el_true_origin;
   std::vector<float>   *el_pt_bare;
   std::vector<float>   *el_eta_bare;
   std::vector<float>   *el_phi_bare;
   std::vector<float>   *el_e_bare;
   std::vector<float>   *mu_pt;
   std::vector<float>   *mu_eta;
   std::vector<float>   *mu_phi;
   std::vector<float>   *mu_e;
   std::vector<float>   *mu_charge;
   std::vector<int>     *mu_true_type;
   std::vector<int>     *mu_true_origin;
   std::vector<float>   *mu_pt_bare;
   std::vector<float>   *mu_eta_bare;
   std::vector<float>   *mu_phi_bare;
   std::vector<float>   *mu_e_bare;
   std::vector<float>   *jet_pt;
   std::vector<float>   *jet_eta;
   std::vector<float>   *jet_phi;
   std::vector<float>   *jet_e;
   std::vector<int>     *jet_nGhosts_bHadron;
   std::vector<int>     *jet_nGhosts_cHadron;
   Float_t         met_met;
   Float_t         met_phi;
   std::vector<float>   *mc_generator_weights;
   Int_t           emu_2022;
   Int_t           ee_2022;
   Int_t           mumu_2022;
   std::vector<float>   *leptonBornPt;
   std::vector<float>   *leptonBornEta;
   std::vector<float>   *leptonBornPhi;
   std::vector<float>   *leptonBornE;
   std::vector<int>     *leptonBornPdgId;

   // List of branches
   TBranch        *b_weight_mc;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_beamspot;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_pt_bare;   //!
   TBranch        *b_el_eta_bare;   //!
   TBranch        *b_el_phi_bare;   //!
   TBranch        *b_el_e_bare;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_pt_bare;   //!
   TBranch        *b_mu_eta_bare;   //!
   TBranch        *b_mu_phi_bare;   //!
   TBranch        *b_mu_e_bare;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_nGhosts_bHadron;   //!
   TBranch        *b_jet_nGhosts_cHadron;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_emu_2022;   //!
   TBranch        *b_ee_2022;   //!
   TBranch        *b_mumu_2022;   //!
   TBranch        *b_leptonBornPt;   //!
   TBranch        *b_leptonBornEta;   //!
   TBranch        *b_leptonBornPhi;   //!
   TBranch        *b_leptonBornE;   //!
   TBranch        *b_leptonBornPdgId;   //!

   explicit ParticleEvent(TTree *tree=0);
   virtual ~ParticleEvent();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
};

#endif
