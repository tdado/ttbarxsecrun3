#ifndef DILEPTONMATCHER_H_
#define DILEPTONMATCHER_H_

#include "TTbarXsec/Common.h"
#include "TTbarXsec/Event.h"

class DileptonMatcher {

public:

    enum MATCHINGOPTION {
        DELTAR = 0,
        MLB = 1
    };

    explicit DileptonMatcher(const MATCHINGOPTION& opt);

    ~DileptonMatcher() = default;

    std::pair<std::size_t, std::size_t> FindMatching(const Event& event,
                                                     const Common::LEPTONTYPE& type) const;

private:

    MATCHINGOPTION m_option;

};

#endif
