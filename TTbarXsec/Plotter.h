#ifndef PLOTTER_H_
#define PLOTTER_H_

#include "TTbarXsec/Common.h"
#include "TTbarXsec/SystHistoManager.h"

#include "TFile.h"

#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

class TGraphAsymmErrors;
class TH1D;
class TPad;
class TArrow;

class Plotter {

public:
    explicit Plotter(const std::string& directory, const Common::CHANNEL chan);

    ~Plotter() = default;

    void SetTTbarNames(const std::vector<std::string>& names);
    
    void SetZNames(const std::vector<std::string>& names);
    
    void SetBackgroundNames(const std::vector<std::string>& names);
    
    void SetSpecialNames(const std::vector<std::string>& names);

    void SetDataName(const std::string& name);

    void SetAtlasLabel(const std::string& label);
    
    void SetLumiLabel(const std::string& label);
    
    void SetCollection(const std::string& collection);
    
    void SetNominalTTbarName(const std::string& name);
    
    void SetNominalZName(const std::string& name);

    inline const std::string& GetNominalTTbarName() const {return m_nominal_ttbar_name;}

    inline void SetSystShapeOnly(const bool& flag) {m_syst_shape_only = flag;}

    inline void SetAllSysts(const std::vector<Systematic>& systs) {m_systematics = systs;}

    void SetRegionNames(const std::vector<std::string>& regNames, const std::vector<std::string>& regNamesEMu);

    void SetRunSyst(const bool& flag) {m_run_syst = flag;}

    inline void SetScale(const float& scale) {m_scale = scale;}

    inline void SetStoreDileptonPt(const bool& flag) {m_storeDileptonPt = flag;}

    inline void SetStoreLeadingLeptonPt(const bool& flag) {m_storeLeadingLeptonPt = flag;}

    void PlotDataMCPlots(const std::string& name,
                         const std::string& axis,
                         const std::string& elements,
                         const std::string& units,
                         const bool& logY,
                         const Common::LEPTONTYPE& lepton_type) const;

    void CalculateYields(const Common::LEPTONTYPE& lepton_type) const;

    void OpenRootFiles();

    void CloseRootFiles();

private:
    std::string m_directory;

    std::vector<std::string> m_ttbar_names;
    std::vector<std::string> m_Z_names;
    std::vector<std::string> m_background_names;
    std::vector<std::string> m_special_names;
    std::vector<std::string> m_background_names_with_impossible;
    std::string m_data_name;

    std::vector<std::unique_ptr<TFile> > m_ttbar_files_el;
    std::vector<std::unique_ptr<TFile> > m_Z_files_el;
    std::vector<std::unique_ptr<TFile> > m_background_files_el;
    std::vector<std::unique_ptr<TFile> > m_special_files_el;
    std::unique_ptr<TFile> m_data_file_el;
    std::vector<std::unique_ptr<TFile> > m_ttbar_files_mu;
    std::vector<std::unique_ptr<TFile> > m_Z_files_mu;
    std::vector<std::unique_ptr<TFile> > m_background_files_mu;
    std::vector<std::unique_ptr<TFile> > m_special_files_mu;
    std::unique_ptr<TFile> m_data_file_mu;
    std::vector<std::unique_ptr<TFile> > m_ttbar_files_ee;
    std::vector<std::unique_ptr<TFile> > m_Z_files_ee;
    std::vector<std::unique_ptr<TFile> > m_background_files_ee;
    std::vector<std::unique_ptr<TFile> > m_special_files_ee;
    std::unique_ptr<TFile> m_data_file_ee;
    std::vector<std::unique_ptr<TFile> > m_ttbar_files_mumu;
    std::vector<std::unique_ptr<TFile> > m_Z_files_mumu;
    std::vector<std::unique_ptr<TFile> > m_background_files_mumu;
    std::vector<std::unique_ptr<TFile> > m_special_files_mumu;
    std::unique_ptr<TFile> m_data_file_mumu;
    std::vector<std::unique_ptr<TFile> > m_ttbar_files_emu;
    std::vector<std::unique_ptr<TFile> > m_Z_files_emu;
    std::vector<std::unique_ptr<TFile> > m_background_files_emu;
    std::vector<std::unique_ptr<TFile> > m_special_files_emu;
    std::unique_ptr<TFile> m_data_file_emu;

    Common::CHANNEL m_channel;

    std::vector<std::pair<int,int> > m_styles;

    std::string m_atlas_label;
    std::string m_lumi_label;
    std::string m_collection;
    std::string m_nominal_ttbar_name;
    std::size_t m_nominal_ttbar_index;
    std::string m_nominal_Z_name;
    std::size_t m_nominal_Z_index;
    bool m_syst_shape_only;
    bool m_run_syst;
    float m_scale;
    std::map<const std::string, int> m_colour_map;
    std::map<const std::string, std::string> m_label_map;
    std::vector<Systematic> m_systematics;
    std::vector<std::string> m_regNames;
    std::vector<std::string> m_regNamesEMu;
    bool m_storeDileptonPt;
    bool m_storeLeadingLeptonPt;

    void FillStyleMap();

    float MaxResize(const std::vector<std::unique_ptr<TH1D> >& histos, const bool& is_log) const;

    void DrawLabels(TPad *pad,
                    const float& x,
                    const float& y,
                    const bool& add_lumi,
                    const bool& is_simulation,
                    const Common::LEPTONTYPE& type,
                    const std::string& ireg) const;

    void DrawUpperDataMCPlot(TPad* pad,
                             TH1D* ttbar_histo,
                             const std::vector<std::unique_ptr<TH1D> >& background_histos,
                             TH1D* data_histo,
                             TGraphAsymmErrors* error,
                             const std::string& name) const;

    void DrawRatioDataMCPlot(TPad* pad,
                             TH1D* combined,
                             TH1D* data,
                             const std::string& axis,
                             TGraphAsymmErrors* total_down,
                             std::vector<std::unique_ptr<TArrow> >& arrows,
                             const std::string& name) const;

    void SetColourMap();
    
    void SetLabelMap();

    void GetTotalUpDownUncertainty(TH1D* total_up,
                                   TH1D* total_down,
                                   const std::string& name,
                                   const Common::LEPTONTYPE& lepton_type,
                                   const std::string& regName) const;

    void AddSingleSyst(TH1D* total_up,
                       TH1D* total_down,
                       const std::string& name,
                       const Systematic& systematic,
                       const Common::LEPTONTYPE& lepton_type,
                       const std::string& regName) const;

    std::unique_ptr<TH1D> GetHistoFromAll(const std::string& file_name,
                                          const std::string& histo_name,
                                          const Common::LEPTONTYPE& lepton_type,
                                          const bool scale) const;

    void AddHistosInSquares(TH1D* total_up,
                            TH1D* total_down,
                            TH1D* histo_up,
                            TH1D* histo_down,
                            TH1D* histo_nominal,
                            const double scale) const;

    void TransformErrorHistoToTGraph(TGraphAsymmErrors* error,
                                     TH1D* up,
                                     TH1D* down) const;
    
    void PlotDataMCPlots(const std::string& name,
                         const std::string& axis,
                         const std::string& elements,
                         const std::string& units,
                         const bool& logY,
                         const Common::LEPTONTYPE& lepton_type,
                         const std::string& regName) const;

    void CalculateYields(const Common::LEPTONTYPE& lepton_type, const std::string& ireg) const;

    void StoreActualMu(const TH1D* total, const TH1D* data) const;
    
    void StoreDileptonPt(const TH1D* total, const TH1D* data) const;
    
    void StoreLeadingLeptonPt(const TH1D* total, const TH1D* data) const;
};

#endif
