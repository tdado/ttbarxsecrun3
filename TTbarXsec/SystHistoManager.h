#ifndef SYSHISTOMANAGER_H_
#define SYSHISTOMANAGER_H_

#include <string>
#include <vector>

enum SYSTEMATICTYPE {
    ONESIDED,
    TWOSIDED,
    NORMALISATION,
    MCSTAT
};

struct Systematic {
    SYSTEMATICTYPE type;
    std::string up_histo;
    std::string down_histo;
    std::string up_file;
    std::string down_file;
    std::string reference_file;
    std::string reference_histo;
    std::string target;
    std::string name;
    std::string category;
    float norm_up;
    float norm_down;
    std::string subtractUpName;
    std::string subtractDownName;
    float scale = 1;
};

class SystHistoManager{
public:
    SystHistoManager();

    ~SystHistoManager() = default;

    void AddOneSidedSyst(const std::string& sys_file,
                         const std::string& sys,
                         const std::string& reference_file,
                         const std::string& reference_histo,
                         const std::string& target,
                         const std::string& name,
                         const std::string& category,
                         const float scale = 1.);
    
    void AddTwoSidedSyst(const std::string& sys_file_up,
                         const std::string& sys_file_down,
                         const std::string& up,
                         const std::string& down,
                         const std::string& reference_file,
                         const std::string& reference_histo,
                         const std::string& target,
                         const std::string& name,
                         const std::string& category,
                         const std::string& subUp = "",
                         const std::string& subDown = "",
                         const float scale = 1.);

    void AddNormSyst(const std::string& target,
                     const float& up,
                     const float& down,
                     const std::string& name);

    void AddMCstatSyst(const std::string& target);

    const std::vector<Systematic>& GetAllSystematics() const{return m_systematics;}

private:

    std::vector<Systematic> m_systematics;
};

#endif
