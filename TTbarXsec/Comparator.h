#ifndef COMPARATOR_H_
#define COMPARATOR_H_

#include "TTbarXsec/Common.h"

#include "TFile.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

struct Labels{
    std::string x_axis;
    std::string units;
};

class Comparator {

public:
    explicit Comparator();

    ~Comparator() = default;

    void SetLeptonType(const Common::LEPTONTYPE& type);

    void SetFirstHisto(const std::string& folder,
                       const std::string& file,
                       const std::string& name,
                       const std::string& name_dn);
    
    void SetSecondHisto(const std::string& folder,
                        const std::string& file,
                        const std::string& name);

    inline void SetNormalise(const bool& flag) {m_normalise = flag;}

    inline void SetRegionNames(const std::vector<std::string>& regNames) {m_regionNames = regNames;}

    Labels GetLabel(const std::string& name) const;

    void OpenRootFiles();

    void GetHistoList();

    void PlotAllHistos();

    void CloseRootFiles();

private:

    std::string m_folder1_name;
    std::string m_file1_name;
    std::string m_histo1_name;
    std::string m_histo1_dn_name;
    std::string m_folder2_name;
    std::string m_file2_name;
    std::string m_histo2_name;
    bool m_normalise;
    bool m_has_up_down;
    Common::LEPTONTYPE m_lepton_type;
    std::vector<std::string> m_regionNames;
    std::vector<std::string> m_histo_list;

    std::map<std::string, Labels> m_labels_map;

    std::unique_ptr<TFile> m_file1;
    std::unique_ptr<TFile> m_file2;

    void PlotSingleHisto(const std::string& name, const std::string& ireg) const;

    void AddSingleLabel(const std::string& name,
                        const std::string& x,
                        const std::string& units);

    void AddAllLabels();
};

#endif
