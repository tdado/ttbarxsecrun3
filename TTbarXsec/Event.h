//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Sep 19 12:11:07 2019 by ROOT version 6.10/08
// from TTree nominal/tree
// found on file: ../test/input_files/ttbar_pflow.root
//////////////////////////////////////////////////////////

#ifndef EVENT_H_
#define EVENT_H_

#include "TTbarXsec/Common.h"

#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

class Event {
public :

   Common::CHANNEL m_channel;

   bool m_isMC;
   bool m_isLoose;
   bool m_isSyst;

   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   std::vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_bTagSF_DL1dv01_77;
   Float_t         weight_jvt;
   Float_t         weight_trigger;
   Float_t         weight_beamspot;
   Float_t         weight_trigger_EL_SF_Trigger_UP;
   Float_t         weight_trigger_EL_SF_Trigger_DOWN;
   Float_t         weight_trigger_MU_SF_STAT_UP;
   Float_t         weight_trigger_MU_SF_STAT_DOWN;
   Float_t         weight_trigger_MU_SF_SYST_UP;
   Float_t         weight_trigger_MU_SF_SYST_DOWN;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   std::vector<float>         *weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP;
   std::vector<float>         *weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_BKG_FRACTION_UP;
   Float_t         weight_leptonSF_MU_SF_ID_BKG_FRACTION_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_LUMI_UNCERT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_LUMI_UNCERT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_MATCHING_UP;
   Float_t         weight_leptonSF_MU_SF_ID_MATCHING_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_MC_XSEC_UP;
   Float_t         weight_leptonSF_MU_SF_ID_MC_XSEC_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_UP;
   Float_t         weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_UP;
   Float_t         weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_TRUTH_UP;
   Float_t         weight_leptonSF_MU_SF_ID_TRUTH_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_CR1_UP;
   Float_t         weight_leptonSF_MU_SF_ID_CR1_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_CR2_UP;
   Float_t         weight_leptonSF_MU_SF_ID_CR2_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_CR3_UP;
   Float_t         weight_leptonSF_MU_SF_ID_CR3_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_UP;
   Float_t         weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_UP;
   Float_t         weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_TAGPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_TAGPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_EXTRAPOLATION_UP;
   Float_t         weight_leptonSF_MU_SF_ID_EXTRAPOLATION_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_BKG_FRACTION_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_BKG_FRACTION_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_DRMUJ_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_DRMUJ_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_MC_XSEC_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_MC_XSEC_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_MLLWINDOW_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_MLLWINDOW_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_MC_XSEC_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_MC_XSEC_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_B_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_C_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_Light_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_B_down;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_C_down;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   Float_t         mu_original_xAOD;
   Float_t         mu_actual_original_xAOD;
   //UInt_t          backgroundFlags;
   std::vector<float>   *el_pt;
   std::vector<float>   *el_eta;
   //std::vector<float>   *el_cl_eta;
   std::vector<float>   *el_phi;
   std::vector<float>   *el_e;
   std::vector<float>   *el_charge;
   //std::vector<float>   *el_topoetcone20;
   //std::vector<float>   *el_ptvarcone20;
   //std::vector<char>    *el_CF;
   //std::vector<char>    *el_d0sig;
   //std::vector<char>    *el_delta_z0_sintheta;
   std::vector<char>    *el_isTight;
   std::vector<int>     *el_true_type;
   std::vector<int>     *el_true_origin;
   std::vector<char>    *el_true_isPrompt;

   //std::vector<int>     *el_true_firstEgMotherTruthType;
   //std::vector<int>     *el_true_firstEgMotherTruthOrigin;
   //std::vector<int>     *el_true_firstEgMotherPdgId;
   //std::vector<char>    *el_true_isChargeFl;
   std::vector<char>    *mu_isTight;
   std::vector<float>   *mu_pt;
   std::vector<float>   *mu_eta;
   std::vector<float>   *mu_phi;
   std::vector<float>   *mu_e;
   std::vector<float>   *mu_charge;
   //std::vector<float>   *mu_topoetcone20;
   //std::vector<float>   *mu_ptvarcone20;
   //std::vector<char>    *mu_d0sig;
   //std::vector<char>    *mu_delta_z0_sintheta;
   std::vector<int>     *mu_true_type;
   std::vector<int>     *mu_true_origin;
   std::vector<char>    *mu_true_isPrompt;
   std::vector<float>   *jet_pt;
   std::vector<float>   *jet_eta;
   std::vector<float>   *jet_phi;
   std::vector<float>   *jet_e;
   std::vector<float>   *jet_jvt;
   //std::vector<int>     *jet_truthflav;
   //std::vector<int>     *jet_truthPartonLabel;
   //std::vector<char>    *jet_isTrueHS;
   //std::vector<int>     *jet_truthflavExtended;
   //std::vector<int>     *jet_truthJetFlavour;
   std::vector<float>   *jet_DL1dv01;
   std::vector<float>   *jet_DL1dv01_pb;
   std::vector<float>   *jet_DL1dv01_pc;
   std::vector<float>   *jet_DL1dv01_pu;
   std::vector<char>    *jet_isbtagged_DL1dv01_85;
   std::vector<char>    *jet_isbtagged_DL1dv01_77;
   std::vector<char>    *jet_isbtagged_DL1dv01_70;
   std::vector<char>    *jet_isbtagged_DL1dv01_60;
   std::vector<int>    *jet_tagWeightBin_DL1dv01_Continuous;
   Float_t               met_met;
   Float_t               met_phi;
   Float_t               met_sumet;
   Int_t                 ejets_2022 = 0;
   Int_t                 mujets_2022 = 0;
   Int_t                 emu_2022 = 0;
   Int_t                 ee_2022 = 0;
   Int_t                 mumu_2022 = 0;
   Float_t               dileptonMass = 0;
   Float_t               dileptonDeltaR = 0;
   Float_t               dileptonDeltaPhi = 0;
   Int_t                 hadBjetIndex = 0;
   Int_t                 lightJetIndex1 = 0;
   Int_t                 lightJetIndex2 = 0;
   Float_t               chi2 = 0;
   Bool_t                  HLT_mu24_ivarmedium = false;
   Bool_t                  HLT_mu50_L1MU14FCH = false;
   Bool_t                  HLT_e26_lhtight_ivarloose_L1EM22VHI = false;
   Bool_t                  HLT_e60_lhmedium_L1EM22VHI = false;
   Bool_t                  HLT_e140_lhloose_L1EM22VHI = false;


   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_trigger;   //!
   TBranch        *b_weight_beamspot;   //!
   TBranch        *b_weight_trigger_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_trigger_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_trigger_MU_SF_STAT_UP;   //!
   TBranch        *b_weight_trigger_MU_SF_STAT_DOWN;   //!
   TBranch        *b_weight_trigger_MU_SF_SYST_UP;   //!
   TBranch        *b_weight_trigger_MU_SF_SYST_DOWN;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_BKG_FRACTION_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_BKG_FRACTION_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_LUMI_UNCERT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_LUMI_UNCERT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_MATCHING_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_MATCHING_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_MC_XSEC_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_MC_XSEC_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_TRUTH_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_TRUTH_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_CR1_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_CR1_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_CR2_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_CR2_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_CR3_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_CR3_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_TAGPT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_TAGPT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_EXTRAPOLATION_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_EXTRAPOLATION_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_BKG_FRACTION_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_BKG_FRACTION_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_DRMUJ_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_DRMUJ_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_MC_XSEC_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_MC_XSEC_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_MLLWINDOW_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_MLLWINDOW_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_MC_XSEC_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_MC_XSEC_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_DOWN; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_UP; //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_DOWN; //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_mu_original_xAOD;   //!
   TBranch        *b_mu_actual_original_xAOD;   //!
   //TBranch        *b_backgroundFlags;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   //TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   //TBranch        *b_el_topoetcone20;   //!
   //TBranch        *b_el_ptvarcone20;   //!
   //TBranch        *b_el_CF;   //!
   //TBranch        *b_el_d0sig;   //!
   //TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_isPrompt;  //!
   //TBranch        *b_el_true_firstEgMotherTruthType;
   //TBranch        *b_el_true_firstEgMotherTruthOrigin;
   //TBranch        *b_el_true_firstEgMotherPdgId;
   //TBranch        *b_el_true_isChargeFl;
   TBranch        *b_el_isTight;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_true_isPrompt; //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   //TBranch        *b_mu_topoetcone20;   //!
   //TBranch        *b_mu_ptvarcone20;   //!
   //TBranch        *b_mu_d0sig;   //!
   //TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_isTight;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_jvt;   //!
   //TBranch        *b_jet_truthflav;   //!
   //TBranch        *b_jet_truthPartonLabel;   //!
   //TBranch        *b_jet_isTrueHS;   //!
   //TBranch        *b_jet_truthflavExtended;   //!
   //TBranch        *b_jet_truthJetFlavour;   //!
   TBranch        *b_jet_DL1dv01;   //!
   TBranch        *b_jet_DL1dv01_pb;   //!
   TBranch        *b_jet_DL1dv01_pc;   //!
   TBranch        *b_jet_DL1dv01_pu;   //!
   TBranch        *b_jet_isbtagged_DL1dv01_85;   //!
   TBranch        *b_jet_isbtagged_DL1dv01_77;   //!
   TBranch        *b_jet_isbtagged_DL1dv01_70;   //!
   TBranch        *b_jet_isbtagged_DL1dv01_60;   //!
   TBranch        *b_jet_tagWeightBin_DL1dv01_Continuous;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_met_sumet;   //!
   TBranch        *b_ejets_2022;   //!
   TBranch        *b_mujets_2022;   //!
   TBranch        *b_emu_2022;   //!
   TBranch        *b_ee_2022;   //!
   TBranch        *b_mumu_2022;   //!
   TBranch        *b_dileptonMass;   //!
   TBranch        *b_dileptonDeltaR;   //!
   TBranch        *b_dileptonDeltaPhi;   //!
   TBranch        *b_hadBjetIndex;   //!
   TBranch        *b_lightJetIndex1;   //!
   TBranch        *b_lightJetIndex2;   //!
   TBranch        *b_chi2;   //!
   TBranch        *b_HLT_mu24_ivarmedium;   //!
   TBranch        *b_HLT_mu50_L1MU14FCH;   //!
   TBranch        *b_HLT_e26_lhtight_ivarloose_L1EM22VHI;   //!
   TBranch        *b_HLT_e60_lhmedium_L1EM22VHI;   //!
   TBranch        *b_HLT_e140_lhloose_L1EM22VHI;   //!

   Event(TTree *tree, bool isMC, bool isLoose, bool isSyst, Common::CHANNEL ch);
   virtual ~Event();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
};

#endif
