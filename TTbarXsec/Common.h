#ifndef COMMON_H_
#define COMMON_H_

#include "TTbarXsec/ParticleEvent.h"
#include "TTbarXsec/TruthEvent.h"
#include "TTbarNNLOReweighter/TTbarNNLOReweighter/TTbarNNLORecursiveRew.h"

#include "TLorentzVector.h"

#include <memory>
#include <string>
#include <vector>

class Event;
class TH1F;
class TH1D;
class TH2D;
class TH2F;

namespace Common {

    enum LEPTONTYPE {
        EL = 0,
        MU = 1,
        ELMU = 2,
        EE = 3,
        EMU = 4,
        MUMU = 5
    };

    enum CHANNEL {
        SINGLELEPTON = 0,
        DILEPTON = 1
    };

    enum class REGION {
      INCLUSIVE = 0,
      SAMESIGNZMASS = 1,
      SAMESIGNZVETO = 2,
      OPPOSITESIGNZMASS = 3,
      OPPOSITESIGNZVETO = 4, 
      SAMESIGN = 5,
      OPPOSITESIGN = 6,
      OPPOSITESIGN1B = 7
    };
    
    void AddOverflow(TH1D* hist);
    
    void AddOverflow(TH2D* hist);

    std::string RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove);

    double GetMax(const TH1D* const h1, const TH1D* const h2);

    bool EventHasLeptonFakes(const Event& event);

    std::string TypeToString(const LEPTONTYPE& type);

    std::pair<TLorentzVector, TLorentzVector> GetLeptonsChargeOrdered(const Event& event,
                                                                      const LEPTONTYPE& type);

    std::pair<TLorentzVector, TLorentzVector> GetLeptonsPtOrdered(const Event& event,
                                                                  const LEPTONTYPE& type);

    bool JetsArePaired(const TLorentzVector& v1, const TLorentzVector& v2);

    LEPTONTYPE FindLeptonType(const Event& event);
    
    REGION FindRegion(const Event& event);

    std::string RegionToString(const REGION reg);
    
    std::vector<std::string> GetRegionString(const CHANNEL channel, const LEPTONTYPE type = LEPTONTYPE::EE);

    double MWT(const Event& event, const LEPTONTYPE type);

    std::unique_ptr<TH1D> GetHa(const TH1D* geq0);
    
    std::unique_ptr<TH1D> GetHb(const TH1D* geq0, const TH1D* bjet1, const TH1D* bjet2);
    
    std::unique_ptr<TH1D> GetHc(const TH1D* geq0, const TH1D* bjet1, const TH1D* bjet2);

    std::unique_ptr<TH1D> GetShiftedHisto(const TH1D* hist, bool isUp);

    std::size_t GetNbtags_77(const Event& event);

    std::string RegionToStringPlots(const std::string& reg);

    bool HasLeptonsBelowX(const Event& event, const double cut);

    double GetNNLOWeight(const TruthEvent* event, const TTbarNNLORecursiveRew* rew);

    bool IsFiducialZ(const ParticleEvent& particle);
    
    double IsoCorrection(const Event& event, const std::unique_ptr<TH1D>& el, const std::unique_ptr<TH1D>& mu);

    double JvtSyst(const Event& event, const bool isUp);

    void RemoveJetsBelowX(Event* event, const double pt);

    double GetDileptonPtWeight(const Event& event, const TH1D* hist);
}

#endif
