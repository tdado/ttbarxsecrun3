#ifndef CONTROLPLOTS_H_
#define CONTROLPLOTS_H_

#include "TTbarXsec/Common.h"

#include "TH1D.h"
#include "TH2D.h"

#include <string>
#include <vector>

class Event;
class MadgraphReweighter;
class TFile;

class ControlPlots {

public:

    /**
     * @brief Construct a new Control Plots object
     * 
     */
    explicit ControlPlots() noexcept;

    /**
     * @brief Destroy the Control Plots object
     * 
     */
    ~ControlPlots() = default;

    /**
     * @brief Initialise the histograms, set binning etc
     * 
     * @param sf_syst_names names of the SF systematics
     * @param isDilepton flag if dilepton or single lep
     */
    void Init(const std::vector<std::string>& sf_syst_names, const bool isDilepton);

    /**
     * @brief Tells the code which systematics it processes
     * 
     * @param syst 
     */
    void SetSyst(const std::string& syst);

    /**
     * @brief Fill al histos 
     * 
     * @param event Event class 
     * @param pairs Needed for dilepton reconstruction
     * @param isMC flag if the sample is MC or data
     * @param lepton_type type of the lepton
     * @param weights total even weights
     * @param isFIducial tells the code if the event is fiducial
     * @param reg region
     */
    void FillAllHistos(const Event& event,
                       const std::pair<std::size_t, std::size_t>& pairs,
                       const bool& isMC,
                       const Common::LEPTONTYPE& lepton_type,
                       const std::vector<double>& weights,
                       const bool isFiducial,
                       const Common::REGION reg = Common::REGION::SAMESIGNZMASS);

    /**
     * @brief Add under/overflow etc
     * 
     */
    void Finalise();

    /**
     * @brief Write to a ROOT file 
     * 
     * @param name name of the current systematics
     * @param file_ee ee file
     * @param file_mumu mumu file
     * @param file_emu emu file
     * @param file_el el file
     * @param file_mu mu file
     */
    void Write(const std::string& name, TFile* file_ee, TFile* file_mumu, TFile* file_emu, TFile* file_el, TFile* file_mu) const;

private:

    std::vector<std::string> m_sf_syst_names;
    std::vector<double> m_weights;
    std::string m_syst;
    bool m_isMC;
    bool m_isDilepton;
    Common::LEPTONTYPE m_lepton_type;

    /**
     * @brief Structure holding the different histogram types 
     * 
     */
    struct Histos {
        std::vector<TH1D> jet1_pt;
        std::vector<TH1D> jet1_eta;
        std::vector<TH1D> jet1_phi;
        std::vector<TH1D> jet1_e;
        std::vector<TH1D> jet2_pt;
        std::vector<TH1D> jet2_eta;
        std::vector<TH1D> jet2_phi;
        std::vector<TH1D> jet2_e;
        std::vector<TH1D> jet3_pt;
        std::vector<TH1D> jet3_eta;
        std::vector<TH1D> jet3_phi;
        std::vector<TH1D> jet3_e;
        std::vector<TH1D> jet4_pt;
        std::vector<TH1D> jet4_eta;
        std::vector<TH1D> jet4_phi;
        std::vector<TH1D> jet4_e;
        std::vector<TH1D> jet5_pt;
        std::vector<TH1D> jet5_eta;
        std::vector<TH1D> jet5_phi;
        std::vector<TH1D> jet5_e;
        std::vector<TH1D> jet6_pt;
        std::vector<TH1D> jet6_eta;
        std::vector<TH1D> jet6_phi;
        std::vector<TH1D> jet6_e;
        std::vector<TH1D> bjet_pt_77;
        std::vector<TH1D> bjet_eta_77;
        std::vector<TH1D> bjet_phi_77;
        std::vector<TH1D> bjet_e_77;
        std::vector<TH1D> HT;
        std::vector<TH1D> jet_n;
        std::vector<TH1D> bjet_n_77;
        std::vector<TH1D> jet_tagbin;
        std::vector<TH1D> jet1_tagbin;
        std::vector<TH1D> jet2_tagbin;
        std::vector<TH1D> jet3_tagbin;
        std::vector<TH1D> jet4_tagbin;

        std::vector<TH1D> lepton1_pt;
        std::vector<TH1D> lepton1_eta;
        std::vector<TH1D> lepton1_phi;
        std::vector<TH1D> lepton1_e;
        std::vector<TH1D> lepton2_pt;
        std::vector<TH1D> lepton2_eta;
        std::vector<TH1D> lepton2_phi;
        std::vector<TH1D> lepton2_e;
        std::vector<TH1D> electron_pt;
        std::vector<TH1D> electron_eta;
        std::vector<TH1D> electron_phi;
        std::vector<TH1D> electron_e;
        std::vector<TH1D> lepton_pt;
        std::vector<TH1D> lepton_eta;
        std::vector<TH1D> lepton_phi;
        std::vector<TH1D> lepton_e;
        std::vector<TH1D> muon_pt;
        std::vector<TH1D> muon_eta;
        std::vector<TH1D> muon_phi;
        std::vector<TH1D> muon_e;
        std::vector<TH1D> dilepton_m;
        std::vector<TH1D> dilepton_m_peak;
        std::vector<TH1D> dilepton_m_peak_fid;
        std::vector<TH1D> dilepton_m_peak_oof;
        std::vector<TH1D> dilepton_m_peak_5bins;
        std::vector<TH1D> dilepton_m_peak_5bins_fid;
        std::vector<TH1D> dilepton_m_peak_5bins_oof;
        std::vector<TH1D> dilepton_m_peak_10bins;
        std::vector<TH1D> dilepton_m_peak_10bins_fid;
        std::vector<TH1D> dilepton_m_peak_10bins_oof;
        std::vector<TH1D> dilepton_m_peak_15bins;
        std::vector<TH1D> dilepton_m_peak_15bins_fid;
        std::vector<TH1D> dilepton_m_peak_15bins_oof;
        std::vector<TH1D> dilepton_m_peak_20bins;
        std::vector<TH1D> dilepton_m_peak_20bins_fid;
        std::vector<TH1D> dilepton_m_peak_20bins_oof;
        std::vector<TH1D> dilepton_m_peak_25bins;
        std::vector<TH1D> dilepton_m_peak_25bins_fid;
        std::vector<TH1D> dilepton_m_peak_25bins_oof;
        std::vector<TH1D> dilepton_deltaR;
        std::vector<TH1D> dilepton_deltaPhi;
        std::vector<TH1D> dilepton_pt;
        std::vector<TH1D> met;
        std::vector<TH1D> met_phi;
        std::vector<TH1D> met_x;
        std::vector<TH1D> met_y;
        std::vector<TH1D> met_sumet;
        std::vector<TH1D> mwt;
        std::vector<TH1D> average_mu;
        std::vector<TH1D> actual_mu;
        std::vector<TH1D> reco_W_mass;
        std::vector<TH1D> reco_top_mass;
        std::vector<TH1D> chi2;
        std::vector<TH1D> n_geq0;
        std::vector<TH1D> n_geq0_fid;
        std::vector<TH1D> n_geq0_oof;
        std::vector<TH1D> n_1bjet;
        std::vector<TH1D> n_2bjet;
        std::vector<TH1D> n_geq0_30;
        std::vector<TH1D> n_1bjet_30;
        std::vector<TH1D> n_2bjet_30;
        std::vector<TH1D> n_geq0_35;
        std::vector<TH1D> n_1bjet_35;
        std::vector<TH1D> n_2bjet_35;
        std::vector<TH1D> n_geq0_leadingElectron;
        std::vector<TH1D> n_geq0_leadingMuon;
        std::vector<TH1D> n_1bjet_leadingElectron;
        std::vector<TH1D> n_1bjet_leadingMuon;
        std::vector<TH1D> n_2bjet_leadingElectron;
        std::vector<TH1D> n_2bjet_leadingMuon;
        std::vector<TH1D> jet_Z_balance_pt_17_30;
        std::vector<TH1D> jet_Z_balance_pt_30_50;
        std::vector<TH1D> jet_Z_balance_pt_50_100;
        std::vector<TH1D> jet_Z_balance_pt_100_200;
        std::vector<TH1D> jet_Z_balance_pt_200_inf;
        std::vector<TH1D> jet_Z_balance_jet_pt;
        std::vector<TH1D> jet_Z_balance_Z_pt;
    };

    std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > > m_histoVec;

    std::vector<std::string> m_regionNamesNotEMu;
    std::vector<std::string> m_regionNamesEMu;

    /**
     * @brief Fills jet related plots 
     * 
     * @param event Event class
     * @param itr iterator representing the given channel
     * @param index position in the vector of the region
     */
    void FillJetPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index);
    
    /**
     * @brief Fills b-tag related plots 
     * 
     * @param event Event class
     * @param itr iterator representing the given channel
     * @param index position in the vector of the region
     */
    void FillTagPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index);

    /**
     * @brief Fills otherplots like MET
     * 
     * @param event Event class
     * @param itr iterator representing the given channel
     * @param index position in the vector of the region
     */
    void FillOtherPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index);

    /**
     * @brief Fills special dilepton plots 
     * 
     * @param event Event class
     * @param itr iterator representing the given channel
     * @param index position in the vector of the region
     * @param isFiducial tells the code if the event is fiducial
     */
    void FillDileptonPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index, const bool isFiducial);
    
    void FillDileptonPlotsExtra(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index);

    /**
     * @brief Fills special singlelep plots 
     * 
     * @param event Event class
     * @param itr iterator representing the given channel
     * @param index position in the vector of the region
     */
    void FillSingleLepPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index);
};

#endif
