//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Oct 18 18:49:19 2022 by ROOT version 6.27/01
// from TTree truth/tree
// found on file: test/input/dilep/r22-dilepton-ttbar.root
//////////////////////////////////////////////////////////

#ifndef TRUTHEVENT_h
#define TRUTHEVENT_h

#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class TruthEvent {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         weight_mc;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   Float_t         mu;
   Float_t         mu_actual;
   Float_t         weight_pileup;
   Float_t         weight_beamspot;
   UInt_t          mcChannelNumber;
   Int_t           MC_Wdecay2_from_t_pdgId;
   Int_t           MC_Wdecay2_from_tbar_pdgId;
   Int_t           MC_Wdecay1_from_tbar_pdgId;
   Int_t           MC_Wdecay1_from_t_pdgId;
   Float_t         MC_Wdecay2_from_tbar_phi;
   Float_t         MC_Wdecay2_from_tbar_pt;
   Float_t         MC_Wdecay2_from_t_phi;
   Float_t         MC_Wdecay2_from_t_pt;
   Float_t         MC_Wdecay2_from_t_m;
   Float_t         MC_Wdecay1_from_tbar_phi;
   Float_t         MC_tbar_beforeFSR_pt;
   Float_t         MC_tbar_beforeFSR_m;
   Float_t         MC_tbar_beforeFSR_eta;
   Float_t         MC_W_from_tbar_eta;
   Float_t         MC_Wdecay1_from_t_eta;
   Float_t         MC_ttbar_beforeFSR_pt;
   Float_t         MC_t_afterFSR_SC_pt;
   Float_t         MC_ttbar_afterFSR_beforeDecay_m;
   Float_t         MC_b_from_tbar_m;
   Float_t         MC_t_beforeFSR_phi;
   Float_t         MC_ttbar_afterFSR_phi;
   Float_t         MC_t_beforeFSR_eta;
   Float_t         MC_b_from_tbar_pt;
   Float_t         MC_t_afterFSR_SC_phi;
   Float_t         MC_tbar_beforeFSR_phi;
   Float_t         MC_ttbar_afterFSR_eta;
   Float_t         MC_t_beforeFSR_pt;
   Float_t         MC_ttbar_beforeFSR_eta;
   Float_t         MC_Wdecay2_from_t_eta;
   Float_t         MC_t_afterFSR_SC_eta;
   Float_t         MC_b_from_t_m;
   Float_t         MC_ttbar_beforeFSR_m;
   Float_t         MC_tbar_afterFSR_SC_pt;
   Float_t         MC_b_from_t_eta;
   Float_t         MC_tbar_afterFSR_m;
   Float_t         MC_t_afterFSR_pt;
   Float_t         MC_ttbar_afterFSR_beforeDecay_eta;
   Float_t         MC_Wdecay2_from_tbar_m;
   Float_t         MC_ttbar_beforeFSR_phi;
   Float_t         MC_ttbar_afterFSR_beforeDecay_phi;
   Float_t         MC_t_afterFSR_m;
   Float_t         MC_t_afterFSR_phi;
   Float_t         MC_ttbar_afterFSR_beforeDecay_pt;
   Float_t         MC_ttbar_afterFSR_m;
   Float_t         MC_tbar_afterFSR_pt;
   Float_t         MC_W_from_t_eta;
   Float_t         MC_Wdecay1_from_tbar_m;
   Float_t         MC_Wdecay2_from_tbar_eta;
   Float_t         MC_t_afterFSR_eta;
   Float_t         MC_tbar_afterFSR_eta;
   Float_t         MC_tbar_afterFSR_phi;
   Float_t         MC_tbar_afterFSR_SC_eta;
   Float_t         MC_tbar_afterFSR_SC_m;
   Float_t         MC_tbar_afterFSR_SC_phi;
   Float_t         MC_W_from_t_m;
   Float_t         MC_W_from_t_pt;
   Float_t         MC_W_from_tbar_pt;
   Float_t         MC_W_from_t_phi;
   Float_t         MC_W_from_tbar_m;
   Float_t         MC_Wdecay1_from_t_phi;
   Float_t         MC_W_from_tbar_phi;
   Float_t         MC_b_from_tbar_eta;
   Float_t         MC_b_from_t_pt;
   Float_t         MC_b_from_t_phi;
   Float_t         MC_t_afterFSR_SC_m;
   Float_t         MC_b_from_tbar_phi;
   Float_t         MC_Wdecay1_from_t_m;
   Float_t         MC_t_beforeFSR_m;
   Float_t         MC_Wdecay1_from_t_pt;
   Float_t         MC_ttbar_afterFSR_pt;
   Float_t         MC_Wdecay1_from_tbar_pt;
   Float_t         MC_Wdecay1_from_tbar_eta;

   // List of branches
   TBranch        *b_weight_mc;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_beamspot;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_MC_Wdecay2_from_t_pdgId;   //!
   TBranch        *b_MC_Wdecay2_from_tbar_pdgId;   //!
   TBranch        *b_MC_Wdecay1_from_tbar_pdgId;   //!
   TBranch        *b_MC_Wdecay1_from_t_pdgId;   //!
   TBranch        *b_MC_Wdecay2_from_tbar_phi;   //!
   TBranch        *b_MC_Wdecay2_from_tbar_pt;   //!
   TBranch        *b_MC_Wdecay2_from_t_phi;   //!
   TBranch        *b_MC_Wdecay2_from_t_pt;   //!
   TBranch        *b_MC_Wdecay2_from_t_m;   //!
   TBranch        *b_MC_Wdecay1_from_tbar_phi;   //!
   TBranch        *b_MC_tbar_beforeFSR_pt;   //!
   TBranch        *b_MC_tbar_beforeFSR_m;   //!
   TBranch        *b_MC_tbar_beforeFSR_eta;   //!
   TBranch        *b_MC_W_from_tbar_eta;   //!
   TBranch        *b_MC_Wdecay1_from_t_eta;   //!
   TBranch        *b_MC_ttbar_beforeFSR_pt;   //!
   TBranch        *b_MC_t_afterFSR_SC_pt;   //!
   TBranch        *b_MC_ttbar_afterFSR_beforeDecay_m;   //!
   TBranch        *b_MC_b_from_tbar_m;   //!
   TBranch        *b_MC_t_beforeFSR_phi;   //!
   TBranch        *b_MC_ttbar_afterFSR_phi;   //!
   TBranch        *b_MC_t_beforeFSR_eta;   //!
   TBranch        *b_MC_b_from_tbar_pt;   //!
   TBranch        *b_MC_t_afterFSR_SC_phi;   //!
   TBranch        *b_MC_tbar_beforeFSR_phi;   //!
   TBranch        *b_MC_ttbar_afterFSR_eta;   //!
   TBranch        *b_MC_t_beforeFSR_pt;   //!
   TBranch        *b_MC_ttbar_beforeFSR_eta;   //!
   TBranch        *b_MC_Wdecay2_from_t_eta;   //!
   TBranch        *b_MC_t_afterFSR_SC_eta;   //!
   TBranch        *b_MC_b_from_t_m;   //!
   TBranch        *b_MC_ttbar_beforeFSR_m;   //!
   TBranch        *b_MC_tbar_afterFSR_SC_pt;   //!
   TBranch        *b_MC_b_from_t_eta;   //!
   TBranch        *b_MC_tbar_afterFSR_m;   //!
   TBranch        *b_MC_t_afterFSR_pt;   //!
   TBranch        *b_MC_ttbar_afterFSR_beforeDecay_eta;   //!
   TBranch        *b_MC_Wdecay2_from_tbar_m;   //!
   TBranch        *b_MC_ttbar_beforeFSR_phi;   //!
   TBranch        *b_MC_ttbar_afterFSR_beforeDecay_phi;   //!
   TBranch        *b_MC_t_afterFSR_m;   //!
   TBranch        *b_MC_t_afterFSR_phi;   //!
   TBranch        *b_MC_ttbar_afterFSR_beforeDecay_pt;   //!
   TBranch        *b_MC_ttbar_afterFSR_m;   //!
   TBranch        *b_MC_tbar_afterFSR_pt;   //!
   TBranch        *b_MC_W_from_t_eta;   //!
   TBranch        *b_MC_Wdecay1_from_tbar_m;   //!
   TBranch        *b_MC_Wdecay2_from_tbar_eta;   //!
   TBranch        *b_MC_t_afterFSR_eta;   //!
   TBranch        *b_MC_tbar_afterFSR_eta;   //!
   TBranch        *b_MC_tbar_afterFSR_phi;   //!
   TBranch        *b_MC_tbar_afterFSR_SC_eta;   //!
   TBranch        *b_MC_tbar_afterFSR_SC_m;   //!
   TBranch        *b_MC_tbar_afterFSR_SC_phi;   //!
   TBranch        *b_MC_W_from_t_m;   //!
   TBranch        *b_MC_W_from_t_pt;   //!
   TBranch        *b_MC_W_from_tbar_pt;   //!
   TBranch        *b_MC_W_from_t_phi;   //!
   TBranch        *b_MC_W_from_tbar_m;   //!
   TBranch        *b_MC_Wdecay1_from_t_phi;   //!
   TBranch        *b_MC_W_from_tbar_phi;   //!
   TBranch        *b_MC_b_from_tbar_eta;   //!
   TBranch        *b_MC_b_from_t_pt;   //!
   TBranch        *b_MC_b_from_t_phi;   //!
   TBranch        *b_MC_t_afterFSR_SC_m;   //!
   TBranch        *b_MC_b_from_tbar_phi;   //!
   TBranch        *b_MC_Wdecay1_from_t_m;   //!
   TBranch        *b_MC_t_beforeFSR_m;   //!
   TBranch        *b_MC_Wdecay1_from_t_pt;   //!
   TBranch        *b_MC_ttbar_afterFSR_pt;   //!
   TBranch        *b_MC_Wdecay1_from_tbar_pt;   //!
   TBranch        *b_MC_Wdecay1_from_tbar_eta;   //!

   explicit TruthEvent(TTree *tree=0);
   virtual ~TruthEvent();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
};
#endif

