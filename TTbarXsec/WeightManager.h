#ifndef WEIGHTMANAGER_H_
#define WEIGHTMANAGER_H_

#include "TTbarXsec/SfSyst.h"

#include "TH2F.h"

#include <map>
#include <string>
#include <vector>

class Event;
class TH1D;
class TruthEvent;
class TTbarNNLORecursiveRew;

class WeightManager {

public:
    struct Weight {
        std::vector<double> sumW;
        std::vector<std::string> names;
        std::vector<std::size_t> indices;
    };
    
    WeightManager();

    ~WeightManager() = default;

    bool ReadFile(const std::string& path);
    
    void SetDsidAf2CampaignType(const int& dsid,
                                const std::string& af2,
                                const std::string& campaign,
                                const std::string& type);

    void SetXsecLumi(const double xSec, const double lumi);

    double GetNominalWeight(const Event& event, const bool isSherpa)const;

    std::vector<double> GetSfSystWeights(const Event& event, const bool isSherpa, const TruthEvent* truth = nullptr, const TTbarNNLORecursiveRew* rew = nullptr) const;
    
    void ProcessNominalNormalisation();

    void ProcessModellingNormalisation();

    std::vector<std::string> GetSfSystList(const std::string& systematics,
                                           const std::string& type) const;

    void Clear();

private:

    int m_dsid;
    std::string m_af2;
    std::string m_campaign;
    double m_xSec;
    double m_lumi;
    double m_nominal_normalisation;
    std::string m_type;
    std::vector<double> m_model_normalisation;

    std::vector<SFSYSTENUM> m_sf_enum;

    std::map<const std::string, std::string> m_weight_buggy;

    std::map<const std::string, Weight> m_weight_map;
    
    std::map<const std::string, std::vector<std::string> > m_model_sf_names;

    std::unique_ptr<TH1D> m_elIsoHist;
    std::unique_ptr<TH1D> m_muIsoHist;
    std::unique_ptr<TH1D> m_dileptonPt;

    void InitialiseModelNames();
    
    Weight FindWeight() const;
    
    double GetNominalSumWeight() const;

    std::vector<std::size_t> GetModelIndices(const std::vector<std::string>& names) const;

    void ReadIsoHistos();

    void InitDileptonPtHists();
};

#endif
