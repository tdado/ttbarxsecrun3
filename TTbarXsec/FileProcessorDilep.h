#ifndef FILEPROCESSORDILEP_H_
#define FILEPROCESSORDILEP_H_

#include "TTbarXsec/DileptonMatcher.h"

#include <string>
#include <vector>

class FileProcessorDilep {

public:
    FileProcessorDilep(const std::string& name,
                       const std::string& dataset_type,
                       const std::string& systematics,
                       const std::string& parallelSyst);

    ~FileProcessorDilep() = default;

    void ReadConfig();

    void ProcessAllFiles(const DileptonMatcher::MATCHINGOPTION& opt);

private:
    std::string m_name;
    std::string m_dataset_type;
    std::string m_systematics;
    std::vector<std::string> m_process;
    std::vector<std::string> m_syst_list;
    std::vector<std::string> m_sfsyst_list;
    std::vector<std::string> m_file_path;
    std::vector<std::string> m_file_type;
    std::vector<std::string> m_af2;
    std::vector<std::string> m_campaign;
    std::vector<int> m_dsid;
    std::string m_parallelSyst;

    void ReadFileList(const std::string& path);

    std::vector<std::string> ReadSystList() const;

    void CreateOutputFiles(const std::string& path) const;

    void RenameOutputFiles(const std::string& path) const;
};

#endif
