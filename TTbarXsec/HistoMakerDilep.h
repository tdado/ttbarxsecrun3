#ifndef HISTOMAKERDILEP_H_
#define HISTOMAKERDILEP_H_

#include "TTbarXsec/ControlPlots.h"
#include "TTbarXsec/Common.h"
#include "TTbarXsec/DileptonMatcher.h"
#include "TTbarNNLOReweighter/TTbarNNLOReweighter/TTbarNNLORecursiveRew.h"

#include <memory>
#include <set>
#include <string>
#include <vector>

class WeightManager;
class TH1D;

class HistoMakerDilep {

public:
    explicit HistoMakerDilep(const std::string& type,
                             const std::string& systematics,
                             const std::vector<std::string>& sfsyst_list);

    void SetCurrentSyst(const std::string& syst){m_current_syst = syst;}
    void SetIsMC(const bool& isMC){m_isMC = isMC;}
    void SetDSID(const int& dsid){m_dsid = dsid;}

    void FillHistos(const WeightManager& wei_manager,
                    const std::string& tree_name,
                    const std::string& path,
                    const DileptonMatcher::MATCHINGOPTION& opt,
                    const bool findFakes);
    void Finalise();
    void WriteHistosToFile(const std::string& path, const std::string& name, const std::string& parallelSyst) const;

private:

    std::string m_type;
    std::string m_systematics;
    std::string m_current_syst;
    bool m_isMC;
    int m_dsid;
    Common::LEPTONTYPE m_lepton_type;
    std::string m_tree_name;
    std::vector<std::string> m_sfsyst_list;
    bool m_isSherpa;

    std::unique_ptr<ControlPlots> m_control_plots;
    std::vector<std::pair<Common::LEPTONTYPE,double> > m_eventYields;
    std::set<std::pair<UInt_t, ULong64_t> > m_duplicateEventCheck;
    std::unique_ptr<TTbarNNLORecursiveRew> m_reweighter;
};

#endif
