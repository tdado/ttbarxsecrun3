# TTbarXsec [![build status](https://gitlab.cern.ch/tdado/ttbarxsecrun3/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/tdado/ttbarxsecrun3/commits/master)


This code is used to process the ntuples used for inclusive cross-section lepton and dilepton channels.
The code produces histograms and various kinds of plots.


## Table of contents
1.  [Getting the code](#getting-the-code)
2.  [Compiling the code](#compiling-the-code)
3.  [Creating file list](#creating-file-list)
4.  [Producing histograms](#producing-histograms)
5.  [Producing plots](#producing-plots)
6.  [Comparing histograms](#comparing-histograms)
7.  [Contact](#contact)

## Getting the code

The suggested file structure is as follows:
```
mkdir TTbarXsec
cd TTbarXsec
```
And then get the code using the following command:
```
git clone ssh://git@gitlab.cern.ch:7999/tdado/ttbarxsecrun3.git
```

Now you can call a simple macro that will create the expected directory structure for the output
```
./makeDirectoryStructure.sh
```

## Compiling the code
To compile the code you only need `ROOT` and `cmake`.
You can set it up on lxplus via

```
lsetup cmake
lsetup "views LCG_102 x86_64-centos7-gcc11-opt"
```

Change directory to the main repository:

```
cd ttbarxsecrun3
```

Now compile the code:
```
mkdir build
cd build
cmake ../
make -j4
```

## Creating file list
Now you can produce file list (a text file with the paths to the input files) and also a sumWeights file (a text file with sumWeights, needed for normalisation).
You can do this by going to `scripts/`:
```
cd ../scripts/
```

Change the path to the folder in `makeFileListDilep.sh` and `makeFileListSingleLep.sh`.
As the names suggest, each script will create a file lists (and sum weight files) for dilepton analysis and single lepton analysis, respectively.

The folder can contain all files for signal, background and data with any kind of campaign and a mix of FullSim and FastSim samples.
The code fill search for ROOT files in the directory and read the metadata information

Produce the files for dilepton:
```
./makeFileListDilep.sh
```

and for single lepton
```
./makeFileListSingleLep.sh
```

This will create the two text files for each channel. You can inspect them.

In case you are working with datasets replicated on a local group disk, you can still use this worrkflow by exploting the `generateLinksFromGroupDisk.py` script that will populate a local folder with symbolic links to the files on the local group disk.
You'll need to adjust the script by changing the RSENAME to that of your local group disk, and work on the getLocalGroupPath method in the script to implement the appropriate path translation for your use case, which depends on the specific RSE architecture and local mount point. After fixing that, you'll just need to call `python generateLinksFromGroupDisk.py NAMEOFDATASET OUTPUTFOLDER` to store symbolic links for all the files contained in the dataset in the output folder.

## Producing histograms
Now we have everything ready to process the files in `file_list_singlelepton.txt` and/or `file_list_dilepton`.
To produce the nominal (non-systematics) histograms for dilepton channel do:

```
cd ../ #To get to the main folder
./build/bin/read-dilep Test
```

The argument (`Test`) can be any string. The code will create a new folder in `../OutputHistos/Dilep/` with the name that you pass as as argument and put all the output files in the new folder.

Similarly, for the single lepton channel do
```
./build/bin/read-singlelep Test
```

You can also process scale factor systematics (systematics done by reweighting) or systematics that need to be read from different tree.
To get scale factor systematics do:

```
./build/bin/read-dilep Test full sfsyst
```

To process the tree systematics do:

```
./build/bin/read-dilep Test full syst
```

The second argument, `full`, tells the code to process all type of files, but you can also specify a single file to process (works also for nominal production), e.g. (`ttbar_PP8_dilep_FS`) via:
```
./build/bin/read-dilep Test ttbar_PP8_dilep_FS syst
```

To run the tree systematics in parallel, you can pass a fourth argument, which will be the name of the tree that is to be processed. This will also be used as a suffix for the output file name. E.g.
```
./build/bin/read-dilep Test ttbar_PP8_dilep_FS syst MUON_SAGITTA_RESBIAS__1down
```

And similarly for the single lepton channel.

**Note**: You first need to run the nominal production to be able to run the systematics if you want to run them in sequence!

**Note2**: When running the nominal processing, a temporary files with `.temp` will be created and only when everything is processes the files will be renamed.
This is done to prevent accidental rewriting of already produced files.

## Producing plots
When at least the nominal production of histograms is finished, you can produce plots by running:
```
./build/bin/plot-dilep Test
```

You can also pass a second argument `syst` which will tell the code to produce histograms with full systematic error band as specified in `util/plot-dilep.cxx` (you need to have the relevant histograms produced).
The plots are stored in `../Plots/Dilep/`

Similarly, you can also run 
```
./build/bin/plot-singlelep Test
```

to produce the single lepton plots.

## Comparing histograms
You can compare histograms between folders, files and directories inside the produced ROOT files.
You need to pass 7 arguments to `compare` binary.
The arguments are: folder name, file name, directory in ROOT name, folder name, file name, directory in ROOT name.
The first three arguments will be combined to select one particular set of plots and they will be compared with the second set.
The last argument can be `ee`, `mumu`, `emu`, `el` or `mu`, this argument tells the code which (sub-)channel will be plotted.
The code checks all histograms and then compares them.
The plots are saved in `../Plots/Dilep` folder for the dilepton sub-channels, and in `../Plots/SingleLep` for the single lepton channels.

Example usage:
```
./build/bin/compare Test ttbar_PP8_FS nominal Test ttbar_PP8_AFII nominal el
``` 

This example code will compare fast simulation and full simulation ttbar histograms for the nominal variation for the electron channel in the single lepton channel from histograms stored in `../OutputHistos/SingleLep/Test` folder.

## Contact
Code written by Tomas Dado: tomas.dado@cern.ch, in case of any questions, fell free to contact me
