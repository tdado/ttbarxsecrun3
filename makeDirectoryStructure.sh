#!/bin/bash

mkdir -p ../OutputHistos
mkdir -p ../OutputHistos/SingleLep
mkdir -p ../OutputHistos/Dilep

mkdir -p ../Plots
mkdir -p ../Plots/SingleLep
mkdir -p ../Plots/Dilep

mkdir -p ../Comparison
mkdir -p ../Comparison/SingleLep
mkdir -p ../Comparison/Dilep
