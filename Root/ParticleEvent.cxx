#include "TTbarXsec/ParticleEvent.h"

ParticleEvent::ParticleEvent(TTree *tree) : fChain(0) 
{
   Init(tree);
}

ParticleEvent::~ParticleEvent()
{
}

Int_t ParticleEvent::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ParticleEvent::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

void ParticleEvent::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_pt_bare = 0;
   el_eta_bare = 0;
   el_phi_bare = 0;
   el_e_bare = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   mu_pt_bare = 0;
   mu_eta_bare = 0;
   mu_phi_bare = 0;
   mu_e_bare = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_nGhosts_bHadron = 0;
   jet_nGhosts_cHadron = 0;
   mc_generator_weights = 0;
   leptonBornPt = 0;
   leptonBornEta = 0;
   leptonBornPhi = 0;
   leptonBornE = 0;
   leptonBornPdgId = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;

   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_beamspot", &weight_beamspot, &b_weight_beamspot);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
   fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   fChain->SetBranchAddress("el_pt_bare", &el_pt_bare, &b_el_pt_bare);
   fChain->SetBranchAddress("el_eta_bare", &el_eta_bare, &b_el_eta_bare);
   fChain->SetBranchAddress("el_phi_bare", &el_phi_bare, &b_el_phi_bare);
   fChain->SetBranchAddress("el_e_bare", &el_e_bare, &b_el_e_bare);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
   fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
   fChain->SetBranchAddress("mu_pt_bare", &mu_pt_bare, &b_mu_pt_bare);
   fChain->SetBranchAddress("mu_eta_bare", &mu_eta_bare, &b_mu_eta_bare);
   fChain->SetBranchAddress("mu_phi_bare", &mu_phi_bare, &b_mu_phi_bare);
   fChain->SetBranchAddress("mu_e_bare", &mu_e_bare, &b_mu_e_bare);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_nGhosts_bHadron", &jet_nGhosts_bHadron, &b_jet_nGhosts_bHadron);
   fChain->SetBranchAddress("jet_nGhosts_cHadron", &jet_nGhosts_cHadron, &b_jet_nGhosts_cHadron);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("emu_2022", &emu_2022, &b_emu_2022);
   fChain->SetBranchAddress("ee_2022", &ee_2022, &b_ee_2022);
   fChain->SetBranchAddress("mumu_2022", &mumu_2022, &b_mumu_2022);
   fChain->SetBranchAddress("leptonBornPt", &leptonBornPt, &b_leptonBornPt);
   fChain->SetBranchAddress("leptonBornEta", &leptonBornEta, &b_leptonBornEta);
   fChain->SetBranchAddress("leptonBornPhi", &leptonBornPhi, &b_leptonBornPhi);
   fChain->SetBranchAddress("leptonBornE", &leptonBornE, &b_leptonBornE);
   fChain->SetBranchAddress("leptonBornPdgId", &leptonBornPdgId, &b_leptonBornPdgId);
}
