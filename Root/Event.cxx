#include "TTbarXsec/Event.h"

#include <iostream>

Event::Event(TTree *tree, bool isMC, bool isLoose, bool isSyst, Common::CHANNEL ch) :
    m_channel(ch),
    m_isMC(isMC),
    m_isLoose(isLoose),
    m_isSyst(isSyst),
    fChain(nullptr) {

    if (!tree) {
        std::cerr << "Event::Event: Tree is nullptr" << std::endl;
    } else {
        Init(tree);
    }
}

Event::~Event() {
}

Int_t Event::GetEntry(Long64_t entry) {
// Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}

Long64_t Event::LoadTree(Long64_t entry) {
// Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
       fCurrent = fChain->GetTreeNumber();
    }
    return centry;
}

void Event::Init(TTree *tree) {
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).
    
    // Set object pointer
    mc_generator_weights = 0;
    weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP = 0;
    weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN = 0;
    weight_bTagSF_DL1dv01_77_eigenvars_B_up = 0;
    weight_bTagSF_DL1dv01_77_eigenvars_C_up = 0;
    weight_bTagSF_DL1dv01_77_eigenvars_Light_up = 0;
    weight_bTagSF_DL1dv01_77_eigenvars_B_down = 0;
    weight_bTagSF_DL1dv01_77_eigenvars_C_down = 0;
    weight_bTagSF_DL1dv01_77_eigenvars_Light_down = 0;
    el_pt = 0;
    el_eta = 0;
    //el_cl_eta = 0;
    el_phi = 0;
    el_e = 0;
    el_charge = 0;
    //el_topoetcone20 = 0;
    //el_ptvarcone20 = 0;
    //el_CF = 0;
    //el_d0sig = 0;
    //el_delta_z0_sintheta = 0;
    el_true_type = 0;
    el_true_origin = 0;
    el_true_isPrompt = 0;
    el_isTight = 0;
    //el_true_firstEgMotherTruthType = 0;
    //el_true_firstEgMotherTruthOrigin = 0;
    //el_true_firstEgMotherPdgId = 0;
    //el_true_isChargeFl = 0;
    mu_pt = 0;
    mu_eta = 0;
    mu_phi = 0;
    mu_e = 0;
    mu_charge = 0;
    //mu_topoetcone20 = 0;
    //mu_ptvarcone20 = 0;
    //mu_d0sig = 0;
    //mu_delta_z0_sintheta = 0;
    mu_true_type = 0;
    mu_true_origin = 0;
    mu_true_isPrompt = 0;
    mu_isTight = 0;
    jet_pt = 0;
    jet_eta = 0;
    jet_phi = 0;
    jet_e = 0;
    jet_jvt = 0;
    //jet_truthflav = 0;
    //jet_truthPartonLabel = 0;
    //jet_isTrueHS = 0;
    //jet_truthflavExtended = 0;
    //jet_truthJetFlavour = 0;
    jet_DL1dv01 = 0;
    jet_DL1dv01_pu = 0;
    jet_DL1dv01_pc = 0;
    jet_DL1dv01_pb = 0;
    jet_isbtagged_DL1dv01_85 = 0;
    jet_isbtagged_DL1dv01_77 = 0;
    jet_isbtagged_DL1dv01_70 = 0;
    jet_isbtagged_DL1dv01_60 = 0;
    jet_tagWeightBin_DL1dv01_Continuous = 0;

    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);
    
    if (m_isMC) {
        fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
        fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
        fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
        fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77", &weight_bTagSF_DL1dv01_77, &b_weight_bTagSF_DL1dv01_77);
        fChain->SetBranchAddress("weight_beamspot", &weight_beamspot, &b_weight_beamspot);
        fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
        fChain->SetBranchAddress("weight_trigger", &weight_trigger, &b_weight_trigger);
        if (!m_isSyst){
            fChain->SetBranchAddress("weight_trigger_EL_SF_Trigger_UP", &weight_trigger_EL_SF_Trigger_UP, &b_weight_trigger_EL_SF_Trigger_UP);
            fChain->SetBranchAddress("weight_trigger_EL_SF_Trigger_DOWN", &weight_trigger_EL_SF_Trigger_DOWN, &b_weight_trigger_EL_SF_Trigger_DOWN);
            fChain->SetBranchAddress("weight_trigger_MU_SF_STAT_UP", &weight_trigger_MU_SF_STAT_UP, &b_weight_trigger_MU_SF_STAT_UP);
            fChain->SetBranchAddress("weight_trigger_MU_SF_STAT_DOWN", &weight_trigger_MU_SF_STAT_DOWN, &b_weight_trigger_MU_SF_STAT_DOWN);
            fChain->SetBranchAddress("weight_trigger_MU_SF_SYST_UP", &weight_trigger_MU_SF_SYST_UP, &b_weight_trigger_MU_SF_SYST_UP);
            fChain->SetBranchAddress("weight_trigger_MU_SF_SYST_DOWN", &weight_trigger_MU_SF_SYST_DOWN, &b_weight_trigger_MU_SF_SYST_DOWN);
            fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
            fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
            fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP", &weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP, &b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN", &weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN, &b_weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
            fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_BKG_FRACTION_UP", &weight_leptonSF_MU_SF_ID_BKG_FRACTION_UP, &b_weight_leptonSF_MU_SF_ID_BKG_FRACTION_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_BKG_FRACTION_DOWN", &weight_leptonSF_MU_SF_ID_BKG_FRACTION_DOWN, &b_weight_leptonSF_MU_SF_ID_BKG_FRACTION_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_UP", &weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_FIT_MODEL_LOWPT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_LUMI_UNCERT_UP", &weight_leptonSF_MU_SF_ID_LUMI_UNCERT_UP, &b_weight_leptonSF_MU_SF_ID_LUMI_UNCERT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_LUMI_UNCERT_DOWN", &weight_leptonSF_MU_SF_ID_LUMI_UNCERT_DOWN, &b_weight_leptonSF_MU_SF_ID_LUMI_UNCERT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_MATCHING_UP", &weight_leptonSF_MU_SF_ID_MATCHING_UP, &b_weight_leptonSF_MU_SF_ID_MATCHING_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_MATCHING_DOWN", &weight_leptonSF_MU_SF_ID_MATCHING_DOWN, &b_weight_leptonSF_MU_SF_ID_MATCHING_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_UP", &weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_MATCHING_LOWPT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_MC_XSEC_UP", &weight_leptonSF_MU_SF_ID_MC_XSEC_UP, &b_weight_leptonSF_MU_SF_ID_MC_XSEC_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_MC_XSEC_DOWN", &weight_leptonSF_MU_SF_ID_MC_XSEC_DOWN, &b_weight_leptonSF_MU_SF_ID_MC_XSEC_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_UP", &weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_UP, &b_weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_DOWN", &weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_DOWN, &b_weight_leptonSF_MU_SF_ID_PT_DEPENDENCY_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_UP", &weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_UP, &b_weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_DOWN", &weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_DOWN, &b_weight_leptonSF_MU_SF_ID_QCD_TEMPLATE_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_UP", &weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_UP, &b_weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_DOWN", &weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_DOWN, &b_weight_leptonSF_MU_SF_ID_SUPRESSION_SCALE_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_TRUTH_UP", &weight_leptonSF_MU_SF_ID_TRUTH_UP, &b_weight_leptonSF_MU_SF_ID_TRUTH_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_TRUTH_DOWN", &weight_leptonSF_MU_SF_ID_TRUTH_DOWN, &b_weight_leptonSF_MU_SF_ID_TRUTH_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_UP", &weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_TRUTH_LOWPT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_CR1_UP", &weight_leptonSF_MU_SF_ID_CR1_UP, &b_weight_leptonSF_MU_SF_ID_CR1_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_CR1_DOWN", &weight_leptonSF_MU_SF_ID_CR1_DOWN, &b_weight_leptonSF_MU_SF_ID_CR1_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_CR2_UP", &weight_leptonSF_MU_SF_ID_CR2_UP, &b_weight_leptonSF_MU_SF_ID_CR2_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_CR2_DOWN", &weight_leptonSF_MU_SF_ID_CR2_DOWN, &b_weight_leptonSF_MU_SF_ID_CR2_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_CR3_UP", &weight_leptonSF_MU_SF_ID_CR3_UP, &b_weight_leptonSF_MU_SF_ID_CR3_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_CR3_DOWN", &weight_leptonSF_MU_SF_ID_CR3_DOWN, &b_weight_leptonSF_MU_SF_ID_CR3_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_UP", &weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_UP, &b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_DOWN", &weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_DOWN, &b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEIP_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_UP", &weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_UP, &b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_DOWN", &weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_DOWN, &b_weight_leptonSF_MU_SF_ID_HIGHETA_PROBEISO_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_TAGPT_UP", &weight_leptonSF_MU_SF_ID_TAGPT_UP, &b_weight_leptonSF_MU_SF_ID_TAGPT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_TAGPT_DOWN", &weight_leptonSF_MU_SF_ID_TAGPT_DOWN, &b_weight_leptonSF_MU_SF_ID_TAGPT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_EXTRAPOLATION_UP", &weight_leptonSF_MU_SF_ID_EXTRAPOLATION_UP, &b_weight_leptonSF_MU_SF_ID_EXTRAPOLATION_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_EXTRAPOLATION_DOWN", &weight_leptonSF_MU_SF_ID_EXTRAPOLATION_DOWN, &b_weight_leptonSF_MU_SF_ID_EXTRAPOLATION_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_BKG_FRACTION_UP", &weight_leptonSF_MU_SF_Isol_BKG_FRACTION_UP, &b_weight_leptonSF_MU_SF_Isol_BKG_FRACTION_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_BKG_FRACTION_DOWN", &weight_leptonSF_MU_SF_Isol_BKG_FRACTION_DOWN, &b_weight_leptonSF_MU_SF_Isol_BKG_FRACTION_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_DRMUJ_UP", &weight_leptonSF_MU_SF_Isol_DRMUJ_UP, &b_weight_leptonSF_MU_SF_Isol_DRMUJ_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_DRMUJ_DOWN", &weight_leptonSF_MU_SF_Isol_DRMUJ_DOWN, &b_weight_leptonSF_MU_SF_Isol_DRMUJ_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_UP", &weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_UP, &b_weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_DOWN", &weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_DOWN, &b_weight_leptonSF_MU_SF_Isol_LUMI_UNCERT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_MC_XSEC_UP", &weight_leptonSF_MU_SF_Isol_MC_XSEC_UP, &b_weight_leptonSF_MU_SF_Isol_MC_XSEC_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_MC_XSEC_DOWN", &weight_leptonSF_MU_SF_Isol_MC_XSEC_DOWN, &b_weight_leptonSF_MU_SF_Isol_MC_XSEC_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_MLLWINDOW_UP", &weight_leptonSF_MU_SF_Isol_MLLWINDOW_UP, &b_weight_leptonSF_MU_SF_Isol_MLLWINDOW_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_MLLWINDOW_DOWN", &weight_leptonSF_MU_SF_Isol_MLLWINDOW_DOWN, &b_weight_leptonSF_MU_SF_Isol_MLLWINDOW_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_UP", &weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_UP, &b_weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_DOWN", &weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_DOWN, &b_weight_leptonSF_MU_SF_Isol_QCD_TEMPLATE_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_UP", &weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_UP, &b_weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_DOWN", &weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_DOWN, &b_weight_leptonSF_MU_SF_Isol_SHERPA_POWHEG_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_UP", &weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_UP, &b_weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_DOWN", &weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_DOWN, &b_weight_leptonSF_MU_SF_Isol_SUPRESSION_SCALE_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_UP", &weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_UP, &b_weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_DOWN", &weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_DOWN, &b_weight_leptonSF_MU_SF_Isol_EXTRAPOLATION_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_UP", &weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_UP, &b_weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_DOWN", &weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_DOWN, &b_weight_leptonSF_MU_SF_TTVA_BKG_FRACTION_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_UP", &weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_UP, &b_weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_DOWN", &weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_LUMI_UNCERT_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_MC_XSEC_UP", &weight_leptonSF_MU_SF_TTVA_MC_XSEC_UP, &b_weight_leptonSF_MU_SF_TTVA_MC_XSEC_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_MC_XSEC_DOWN", &weight_leptonSF_MU_SF_TTVA_MC_XSEC_DOWN, &b_weight_leptonSF_MU_SF_TTVA_MC_XSEC_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_UP", &weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_UP, &b_weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_DOWN", &weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_DOWN, &b_weight_leptonSF_MU_SF_TTVA_QCD_TEMPLATE_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_UP", &weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_UP, &b_weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_DOWN", &weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SUPRESSION_SCALE_DOWN);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_UP", &weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_UP, &b_weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_UP);
            fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_DOWN", &weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_DOWN, &b_weight_leptonSF_MU_SF_TTVA_EXTRAPOLATION_DOWN);
            fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
            fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_B_up", &weight_bTagSF_DL1dv01_77_eigenvars_B_up, &b_weight_bTagSF_DL1dv01_77_eigenvars_B_up);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_C_up", &weight_bTagSF_DL1dv01_77_eigenvars_C_up, &b_weight_bTagSF_DL1dv01_77_eigenvars_C_up);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_Light_up", &weight_bTagSF_DL1dv01_77_eigenvars_Light_up, &b_weight_bTagSF_DL1dv01_77_eigenvars_Light_up);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_B_down", &weight_bTagSF_DL1dv01_77_eigenvars_B_down, &b_weight_bTagSF_DL1dv01_77_eigenvars_B_down);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_C_down", &weight_bTagSF_DL1dv01_77_eigenvars_C_down, &b_weight_bTagSF_DL1dv01_77_eigenvars_C_down);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_Light_down", &weight_bTagSF_DL1dv01_77_eigenvars_Light_down, &b_weight_bTagSF_DL1dv01_77_eigenvars_Light_down);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up", &weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up, &b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up);
            fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down", &weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down, &b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down);
        }
    }
    fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
    fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
    fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
    //fChain->SetBranchStatus("eventNumber", 0);
    //fChain->SetBranchStatus("runNumber", 0);
    //fChain->SetBranchStatus("mcChannelNumber", 0);
    fChain->SetBranchAddress("mu", &mu, &b_mu);
    fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
    if (!m_isMC) {
        fChain->SetBranchAddress("mu_original_xAOD", &mu_original_xAOD, &b_mu_original_xAOD);
        fChain->SetBranchAddress("mu_actual_original_xAOD", &mu_actual_original_xAOD, &b_mu_actual_original_xAOD);
    }
    //fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
    fChain->SetBranchStatus("backgroundFlags", 0);
    fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
    fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
    //fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
    fChain->SetBranchStatus("el_cl_eta", 0);
    fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
    fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
    fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
    //fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
    //fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
    //fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
    //fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
    //fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
    fChain->SetBranchStatus("el_topoetcone20", 0);
    fChain->SetBranchStatus("el_ptvarcone20", 0);
    fChain->SetBranchStatus("el_d0sig", 0);
    fChain->SetBranchStatus("el_delta_z0_sintheta", 0);
    fChain->SetBranchStatus("el_CF", 0);
    if (m_isMC) {
        fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
        fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
        fChain->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
        //fChain->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
        //fChain->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
        //fChain->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
        //fChain->SetBranchAddress("el_true_isChargeFl", &el_true_isChargeFl, &b_el_true_isChargeFl);
        fChain->SetBranchStatus("el_true_first*", 0);
        fChain->SetBranchStatus("el_true_isChargeFl", 0);
        fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
        fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
        fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
    }
    fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
    fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
    fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
    fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
    fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
    //fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
    //fChain->SetBranchAddress("mu_ptvarcone20", &mu_ptvarcone20, &b_mu_ptvarcone20);
    //fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
    //fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
    fChain->SetBranchStatus("mu_topoetcone20", 0);
    fChain->SetBranchStatus("mu_ptvarcone20", 0);
    fChain->SetBranchStatus("mu_d0sig", 0);
    fChain->SetBranchStatus("mu_delta_z0_sintheta", 0);
    if (m_isLoose && m_channel == Common::CHANNEL::SINGLELEPTON) {
        fChain->SetBranchAddress("el_isTight", &el_isTight, &b_el_isTight);
        fChain->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
    }
    fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
    fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
    fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
    fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
    fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
    fChain->SetBranchAddress("jet_DL1dv01", &jet_DL1dv01, &b_jet_DL1dv01);
    fChain->SetBranchAddress("jet_DL1dv01_pb", &jet_DL1dv01_pb, &b_jet_DL1dv01_pb);
    fChain->SetBranchAddress("jet_DL1dv01_pc", &jet_DL1dv01_pc, &b_jet_DL1dv01_pc);
    fChain->SetBranchAddress("jet_DL1dv01_pu", &jet_DL1dv01_pu, &b_jet_DL1dv01_pu);
    fChain->SetBranchAddress("jet_isbtagged_DL1dv01_85", &jet_isbtagged_DL1dv01_85, &b_jet_isbtagged_DL1dv01_85);
    fChain->SetBranchAddress("jet_isbtagged_DL1dv01_77", &jet_isbtagged_DL1dv01_77, &b_jet_isbtagged_DL1dv01_77);
    fChain->SetBranchAddress("jet_isbtagged_DL1dv01_70", &jet_isbtagged_DL1dv01_70, &b_jet_isbtagged_DL1dv01_70);
    fChain->SetBranchAddress("jet_isbtagged_DL1dv01_60", &jet_isbtagged_DL1dv01_60, &b_jet_isbtagged_DL1dv01_60);
    fChain->SetBranchAddress("jet_tagWeightBin_DL1dv01_Continuous", &jet_tagWeightBin_DL1dv01_Continuous, &b_jet_tagWeightBin_DL1dv01_Continuous);
    if (m_isMC) {
      //fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
      //fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
      //fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
      //fChain->SetBranchAddress("jet_truthflavExtended", &jet_truthflavExtended, &b_jet_truthflavExtended);
      //fChain->SetBranchAddress("jet_truthJetFlavour", &jet_truthJetFlavour, &b_jet_truthJetFlavour);

      fChain->SetBranchStatus("jet_truth*", 0);
    }
    fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
    fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
    fChain->SetBranchAddress("met_sumet", &met_sumet, &b_met_sumet);
    fChain->SetBranchAddress("HLT_mu24_ivarmedium_L1MU14FCH", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
    fChain->SetBranchAddress("HLT_mu50_L1MU14FCH", &HLT_mu50_L1MU14FCH, &b_HLT_mu50_L1MU14FCH);
    fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_L1EM22VHI", &HLT_e26_lhtight_ivarloose_L1EM22VHI, &b_HLT_e26_lhtight_ivarloose_L1EM22VHI);
    fChain->SetBranchAddress("HLT_e60_lhmedium_L1EM22VHI", &HLT_e60_lhmedium_L1EM22VHI, &b_HLT_e60_lhmedium_L1EM22VHI);
    fChain->SetBranchAddress("HLT_e140_lhloose_L1EM22VHI", &HLT_e140_lhloose_L1EM22VHI, &b_HLT_e140_lhloose_L1EM22VHI);
    if (m_channel == Common::CHANNEL::DILEPTON) {
        fChain->SetBranchAddress("ee_2022", &ee_2022, &b_ee_2022);
        fChain->SetBranchAddress("mumu_2022", &mumu_2022, &b_mumu_2022);
        fChain->SetBranchAddress("emu_2022", &emu_2022, &b_emu_2022);
        fChain->SetBranchAddress("dileptonMass", &dileptonMass, &b_dileptonMass);
        fChain->SetBranchAddress("dileptonDeltaR", &dileptonDeltaR, &b_dileptonDeltaR);
        fChain->SetBranchAddress("dileptonDeltaPhi", &dileptonDeltaPhi, &b_dileptonDeltaPhi);
    }
    if (m_channel == Common::CHANNEL::SINGLELEPTON) {
        fChain->SetBranchAddress("ejets_2022", &ejets_2022, &b_ejets_2022);
        fChain->SetBranchAddress("mujets_2022", &mujets_2022, &b_mujets_2022);
        fChain->SetBranchAddress("hadBjetIndex", &hadBjetIndex, &b_hadBjetIndex);
        fChain->SetBranchAddress("lightJetIndex1", &lightJetIndex1, &b_lightJetIndex1);
        fChain->SetBranchAddress("lightJetIndex2", &lightJetIndex2, &b_lightJetIndex2);
        fChain->SetBranchAddress("chi2", &chi2, &b_chi2);
    }
}
