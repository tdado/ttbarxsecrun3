#include "TTbarXsec/Plotter.h"
#include "TTbarXsec/Common.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TArrow.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TLine.h"
#include "TLegend.h"
#include "TPad.h"
#include "TSystem.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <fstream>

Plotter::Plotter(const std::string& directory, const Common::CHANNEL chan) :
    m_directory(directory),
    m_data_name("SetMe"),
    m_data_file_el(nullptr),
    m_data_file_mu(nullptr),
    m_data_file_ee(nullptr),
    m_data_file_mumu(nullptr),
    m_data_file_emu(nullptr),
    m_channel(chan),
    m_atlas_label("Internal"),
    m_lumi_label("139"),
    m_collection(""),
    m_nominal_ttbar_name(""),
    m_nominal_ttbar_index(999),
    m_nominal_Z_name(""),
    m_nominal_Z_index(999),
    m_syst_shape_only(false),
    m_run_syst(false),
    m_scale(1.),
    m_storeDileptonPt(false),
    m_storeLeadingLeptonPt(false){

    /// Create directory
    if (m_channel == Common::CHANNEL::SINGLELEPTON) {
        gSystem->mkdir(("../Plots/SingleLep/"+m_directory).c_str());
    }
    if (m_channel == Common::CHANNEL::DILEPTON) {
        gSystem->mkdir(("../Plots/Dilep/"+m_directory).c_str());
    }

    SetAtlasStyle();
    FillStyleMap();
    SetColourMap();
    SetLabelMap();
}

void Plotter::SetTTbarNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetTTbarNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_ttbar_names = names;
}

void Plotter::SetZNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetZNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_Z_names = names;
}

void Plotter::SetBackgroundNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetBackgroundNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_background_names = names;
}

void Plotter::SetSpecialNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetSpecialNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_special_names = names;
}

void Plotter::SetDataName(const std::string& name) {
    m_data_name = name;
}

void Plotter::SetAtlasLabel(const std::string& label) {
    m_atlas_label = label;
}

void Plotter::SetLumiLabel(const std::string& label) {
    m_lumi_label = label;
}

void Plotter::SetCollection(const std::string& collection) {
    m_collection = collection;
}

void Plotter::SetNominalTTbarName(const std::string& name) {
    m_nominal_ttbar_name = name;

    auto itr = std::find(m_ttbar_names.begin(), m_ttbar_names.end(), name);
    if (itr == m_ttbar_names.end()) {
        std::cerr << "Plotter::SetNominalTTbarName: The nominal ttbar name is not in the list of ttbar names" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_nominal_ttbar_index = std::distance(m_ttbar_names.begin(), itr);
}

void Plotter::SetNominalZName(const std::string& name) {
    m_nominal_Z_name = name;

    auto itr = std::find(m_Z_names.begin(), m_Z_names.end(), name);
    if (itr == m_Z_names.end()) {
        std::cerr << "Plotter::SetNominalZName: The nominal Z name is not in the list of Z names" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_nominal_Z_index = std::distance(m_Z_names.begin(), itr);
}

void Plotter::SetRegionNames(const std::vector<std::string>& regNames, const std::vector<std::string>& regNamesEMu) {
    m_regNames = regNames;
    m_regNamesEMu = regNamesEMu;

    std::vector<std::string> regions = regNames;
    regions.insert(regions.end(), regNamesEMu.begin(), regNamesEMu.end());
    std::sort(regions.begin(), regions.end());
    auto last = std::unique(regions.begin(), regions.end());
    regions.erase(last, regions.end());

    // prepare folder names
    for (const auto& ireg : regions) {
        if (m_channel == Common::CHANNEL::SINGLELEPTON) {
            gSystem->mkdir(("../Plots/SingleLep/"+m_directory+"/"+ireg).c_str());
        } else {
            gSystem->mkdir(("../Plots/Dilep/"+m_directory+"/"+ireg).c_str());
        }
    }
}

void Plotter::OpenRootFiles() {

    /// TTbar files
    for (const auto& ittbar : m_ttbar_names) {
        if (m_channel == Common::CHANNEL::SINGLELEPTON) {
            std::unique_ptr<TFile> f_el(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+ittbar+"_"+m_directory+"_el.root").c_str(), "READ"));
            if (!f_el) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+ittbar+"_"+m_directory+"_el.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mu(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+ittbar+"_"+m_directory+"_mu.root").c_str(), "READ"));
            if (!f_mu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+ittbar+"_"+m_directory+"_mu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            m_ttbar_files_el.emplace_back(std::move(f_el));
            m_ttbar_files_mu.emplace_back(std::move(f_mu));
        }
        if (m_channel == Common::CHANNEL::DILEPTON) {
            std::unique_ptr<TFile> f_ee(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+ittbar+"_"+m_directory+"_ee.root").c_str(), "READ"));
            if (!f_ee) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+ittbar+"_"+m_directory+"_ee.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mumu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+ittbar+"_"+m_directory+"_mumu.root").c_str(), "READ"));
            if (!f_mumu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+ittbar+"_"+m_directory+"_mumu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_emu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+ittbar+"_"+m_directory+"_emu.root").c_str(), "READ"));
            if (!f_emu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+ittbar+"_"+m_directory+"_emu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            m_ttbar_files_ee.emplace_back(std::move(f_ee));
            m_ttbar_files_mumu.emplace_back(std::move(f_mumu));
            m_ttbar_files_emu.emplace_back(std::move(f_emu));
        }
    }

    for (const auto& iz : m_Z_names) {
        if (m_channel == Common::CHANNEL::SINGLELEPTON) {
            std::unique_ptr<TFile> f_el(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+iz+"_"+m_directory+"_el.root").c_str(), "READ"));
            if (!f_el) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+iz+"_"+m_directory+"_el.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mu(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+iz+"_"+m_directory+"_mu.root").c_str(), "READ"));
            if (!f_mu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+iz+"_"+m_directory+"_mu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            m_Z_files_el.emplace_back(std::move(f_el));
            m_Z_files_mu.emplace_back(std::move(f_mu));
        }
        if (m_channel == Common::CHANNEL::DILEPTON) {
            std::unique_ptr<TFile> f_ee(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+iz+"_"+m_directory+"_ee.root").c_str(), "READ"));
            if (!f_ee) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+iz+"_"+m_directory+"_ee.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mumu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+iz+"_"+m_directory+"_mumu.root").c_str(), "READ"));
            if (!f_mumu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+iz+"_"+m_directory+"_mumu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_emu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+iz+"_"+m_directory+"_emu.root").c_str(), "READ"));
            if (!f_emu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+iz+"_"+m_directory+"_emu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            m_Z_files_ee.emplace_back(std::move(f_ee));
            m_Z_files_mumu.emplace_back(std::move(f_mumu));
            m_Z_files_emu.emplace_back(std::move(f_emu));
        }
    }

    /// Background files
    for (const auto& ibkg : m_background_names) {
        if (m_channel == Common::CHANNEL::SINGLELEPTON) {
            std::unique_ptr<TFile> f_el(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+ibkg+"_"+m_directory+"_el.root").c_str(), "READ"));
            if (!f_el) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+ibkg+"_"+m_directory+"_el.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mu(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+ibkg+"_"+m_directory+"_mu.root").c_str(), "READ"));
            if (!f_mu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+ibkg+"_"+m_directory+"_mu.root" << std::endl;
                exit(EXIT_FAILURE);
            }
            m_background_files_el.emplace_back(std::move(f_el));
            m_background_files_mu.emplace_back(std::move(f_mu));
        }

        if (m_channel == Common::CHANNEL::DILEPTON) {
            std::unique_ptr<TFile> f_ee(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+ibkg+"_"+m_directory+"_ee.root").c_str(), "READ"));
            if (!f_ee) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+ibkg+"_"+m_directory+"_ee.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mumu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+ibkg+"_"+m_directory+"_mumu.root").c_str(), "READ"));
            if (!f_mumu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+ibkg+"_"+m_directory+"_mumu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_emu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+ibkg+"_"+m_directory+"_emu.root").c_str(), "READ"));
            if (!f_emu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+ibkg+"_"+m_directory+"_emu.root" << std::endl;
                exit(EXIT_FAILURE);
            }
            m_background_files_ee.emplace_back(std::move(f_ee));
            m_background_files_mumu.emplace_back(std::move(f_mumu));
            m_background_files_emu.emplace_back(std::move(f_emu));
        }
    }

    /// Special files
    for (const auto& isp : m_special_names) {
        if (m_channel == Common::CHANNEL::SINGLELEPTON) {
            std::unique_ptr<TFile> f_el(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+isp+"_"+m_directory+"_el.root").c_str(), "READ"));
            if (!f_el) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+isp+"_"+m_directory+"_el.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mu(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+isp+"_"+m_directory+"_mu.root").c_str(), "READ"));
            if (!f_mu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+isp+"_"+m_directory+"_mu.root" << std::endl;
                exit(EXIT_FAILURE);
            }
            m_special_files_el.emplace_back(std::move(f_el));
            m_special_files_mu.emplace_back(std::move(f_mu));
        }

        if (m_channel == Common::CHANNEL::DILEPTON) {
            std::unique_ptr<TFile> f_ee(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+isp+"_"+m_directory+"_ee.root").c_str(), "READ"));
            if (!f_ee) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+isp+"_"+m_directory+"_ee.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_mumu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+isp+"_"+m_directory+"_mumu.root").c_str(), "READ"));
            if (!f_mumu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+isp+"_"+m_directory+"_mumu.root" << std::endl;
                exit(EXIT_FAILURE);
            }

            std::unique_ptr<TFile> f_emu(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+isp+"_"+m_directory+"_emu.root").c_str(), "READ"));
            if (!f_emu) {
                std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+isp+"_"+m_directory+"_emu.root" << std::endl;
                exit(EXIT_FAILURE);
            }
            m_special_files_ee.emplace_back(std::move(f_ee));
            m_special_files_mumu.emplace_back(std::move(f_mumu));
            m_special_files_emu.emplace_back(std::move(f_emu));
        }
    }

    /// Data File
    if (m_channel == Common::CHANNEL::SINGLELEPTON) {
        m_data_file_el = std::unique_ptr<TFile>(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_el.root").c_str(), "READ"));
        if (!m_data_file_el) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_el.root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_data_file_mu = std::unique_ptr<TFile>(TFile::Open(("../OutputHistos/SingleLep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_mu.root").c_str(), "READ"));
        if (!m_data_file_mu) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/SingleLep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_mu.root" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if (m_channel == Common::CHANNEL::DILEPTON) {
        m_data_file_ee = std::unique_ptr<TFile>(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_ee.root").c_str(), "READ"));
        if (!m_data_file_ee) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_ee.root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_data_file_mumu = std::unique_ptr<TFile>(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_mumu.root").c_str(), "READ"));
        if (!m_data_file_mumu) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_mumu.root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_data_file_emu = std::unique_ptr<TFile>(TFile::Open(("../OutputHistos/Dilep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_emu.root").c_str(), "READ"));
        if (!m_data_file_emu) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/Dilep/"+m_directory+"/"+m_data_name+"_"+m_directory+"_emu.root" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

}

void Plotter::PlotDataMCPlots(const std::string& name,
                              const std::string& axis,
                              const std::string& elements,
                              const std::string& units,
                              const bool& logY,
                              const Common::LEPTONTYPE& lepton_type) const {
    const auto& regVec = (lepton_type == Common::LEPTONTYPE::EMU) ? m_regNamesEMu : m_regNames;
    for (const auto& ireg : regVec) {
        this->PlotDataMCPlots(name, axis, elements, units, logY, lepton_type, ireg);
    }
}

void Plotter::PlotDataMCPlots(const std::string& name,
                              const std::string& axis,
                              const std::string& elements,
                              const std::string& units,
                              const bool& logY,
                              const Common::LEPTONTYPE& lepton_type,
                              const std::string& regName) const {
    std::cout << "Plotter::PlotDataMCPlots: Plotting data/MC plot: " << name << " for region " << regName <<"\n";

    std::unique_ptr<TH1D> ttbar_histo(nullptr);

    if (lepton_type == Common::LEPTONTYPE::EL) {
        ttbar_histo.reset(static_cast<TH1D*>(m_ttbar_files_el.at(m_nominal_ttbar_index)->Get(("nominal/"+regName + "/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        ttbar_histo.reset(static_cast<TH1D*>(m_ttbar_files_mu.at(m_nominal_ttbar_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        ttbar_histo.reset(static_cast<TH1D*>(m_ttbar_files_ee.at(m_nominal_ttbar_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        ttbar_histo.reset(static_cast<TH1D*>(m_ttbar_files_mumu.at(m_nominal_ttbar_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        ttbar_histo.reset(static_cast<TH1D*>(m_ttbar_files_emu.at(m_nominal_ttbar_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    }
    if (!ttbar_histo) {
        std::cerr << "Plotter::PlotDataMCPlots: Cannot read ttbar histo: " << name << ", skipping\n";
        return;
    }

    ttbar_histo->Scale(m_scale);

    std::unique_ptr<TH1D> z_histo(nullptr);

    if (lepton_type == Common::LEPTONTYPE::EL) {
        z_histo.reset(static_cast<TH1D*>(m_Z_files_el.at(m_nominal_Z_index)->Get(("nominal/"+regName + "/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        z_histo.reset(static_cast<TH1D*>(m_Z_files_mu.at(m_nominal_Z_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        z_histo.reset(static_cast<TH1D*>(m_Z_files_ee.at(m_nominal_Z_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        z_histo.reset(static_cast<TH1D*>(m_Z_files_mumu.at(m_nominal_Z_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        z_histo.reset(static_cast<TH1D*>(m_Z_files_emu.at(m_nominal_Z_index)->Get(("nominal/"+regName+"/"+name).c_str())));
    }
    if (!z_histo) {
        std::cerr << "Plotter::PlotDataMCPlots: Cannot read Z histo: " << name << ", skipping\n";
        return;
    }

    z_histo->Scale(m_scale);

    std::vector<std::unique_ptr<TH1D> > background_histos;
    const std::size_t nfiles = m_channel == Common::CHANNEL::SINGLELEPTON ? m_background_files_el.size() : m_background_files_ee.size();
    for (std::size_t ibkg = 0; ibkg < nfiles; ++ibkg) {
        std::unique_ptr<TH1D> h(nullptr);
        if (lepton_type == Common::LEPTONTYPE::EL) {
            h.reset(static_cast<TH1D*>(m_background_files_el.at(ibkg)->Get(("nominal/"+regName+"/"+name).c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MU) {
            h.reset(static_cast<TH1D*>(m_background_files_mu.at(ibkg)->Get(("nominal/"+regName+"/"+name).c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EE) {
            h.reset(static_cast<TH1D*>(m_background_files_ee.at(ibkg)->Get(("nominal/"+regName+"/"+name).c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
            h.reset(static_cast<TH1D*>(m_background_files_mumu.at(ibkg)->Get(("nominal/"+regName+"/"+name).c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EMU) {
            h.reset(static_cast<TH1D*>(m_background_files_emu.at(ibkg)->Get(("nominal/"+regName+"/"+name).c_str())));
        }
        if (!h) {
            std::cerr << "Plotter::PlotDataMCPlots: Cannot read background histos: " << name << ", skipping\n";
            return;
        }

        background_histos.emplace_back(std::move(h));
        if ((m_channel == Common::CHANNEL::SINGLELEPTON) && (m_background_names.at(ibkg) == "Multijet")) continue;
        background_histos.back()->Scale(m_scale);
    }

    std::unique_ptr<TH1D> data_histo(nullptr);
    if (lepton_type == Common::LEPTONTYPE::EL) {
        data_histo.reset(static_cast<TH1D*>(m_data_file_el->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        data_histo.reset(static_cast<TH1D*>(m_data_file_mu->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        data_histo.reset(static_cast<TH1D*>(m_data_file_ee->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        data_histo.reset(static_cast<TH1D*>(m_data_file_mumu->Get(("nominal/"+regName+"/"+name).c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        data_histo.reset(static_cast<TH1D*>(m_data_file_emu->Get(("nominal/"+regName+"/"+name).c_str())));
    }

    if (!data_histo) {
        std::cerr << "Plotter::PlotDataMCPlots: Cannot read data histogram: " << name << ", skipping\n";
        return;
    }

    if (regName == "OS_Zmass") {
        background_histos.emplace_back(std::move(ttbar_histo));
        ttbar_histo.reset(static_cast<TH1D*>(z_histo->Clone()));
    } else {
        z_histo->SetFillColor(95);
        z_histo->SetLineColor(95);
        background_histos.emplace_back(std::move(z_histo));
    }
    /// Stack background histograms
    for (std::size_t ibkg = 0; ibkg < background_histos.size(); ++ibkg) {
        for (std::size_t i = ibkg + 1; i < background_histos.size(); ++i) {
            background_histos.at(ibkg)->Add(background_histos.at(i).get());
        }
    }

    /// Stack signal as well
    ttbar_histo->Add(background_histos.at(0).get());

    if (m_syst_shape_only) {
        float scaleData(1.0);
        if (data_histo->Integral() > 1e-6 && ttbar_histo->Integral() > 1e-6) {
            scaleData = data_histo->Integral()/ttbar_histo->Integral();
            ttbar_histo->Scale(scaleData);
        }
        for (auto& ibkg : background_histos) {
            ibkg->Scale(scaleData);
        }
    }

    /// Cosmetics
    ttbar_histo->SetFillColor(regName == "OS_Zmass" ? 95 : 0);
    ttbar_histo->SetLineColor(regName == "OS_Zmass" ? 95 : kBlack);
    ttbar_histo->SetMarkerStyle(21);
    ttbar_histo->SetLineWidth(1);
    if (logY) {
        ttbar_histo->SetMinimum(1.01);
        if (regName == "OS_Zmass") {
            ttbar_histo->SetMaximum(3e5*ttbar_histo->GetMaximum());
        } else {
            if (regName == "OS" || regName == "OS_1b") {
                ttbar_histo->SetMaximum(1e3*ttbar_histo->GetMaximum());
            } else {
                ttbar_histo->SetMaximum(1e4*ttbar_histo->GetMaximum());
            }
        }
    } else {
        ttbar_histo->SetMinimum(0.0);
        ttbar_histo->SetMaximum((name == "dilepton_m_peak" ? 1.4 : 2.2)*ttbar_histo->GetMaximum());
    }

    const float bin_width = ttbar_histo->GetBinWidth(1);
    const bool isJetN = ((name.find("jet_n") != std::string::npos) || (name.find("jet_tagbin") != std::string::npos));
    if (isJetN) {
        if (m_syst_shape_only) {
            ttbar_histo->GetYaxis()->SetTitle("Normalised to data");
        } else {
            ttbar_histo->GetYaxis()->SetTitle(elements.c_str());
        }
    } else if (bin_width < 1) {
        if (m_syst_shape_only) {
            ttbar_histo->GetYaxis()->SetTitle(Form("Normalised to data / %.2f %s", bin_width, units.c_str()));
        } else {
            ttbar_histo->GetYaxis()->SetTitle(Form("%s / %.2f %s", elements.c_str(), bin_width, units.c_str()));
        }
    } else if (bin_width == 1) {
            ttbar_histo->GetYaxis()->SetTitle(Form("%s / %s", elements.c_str(), units.c_str()));
    } else {
        if (m_syst_shape_only) {
            ttbar_histo->GetYaxis()->SetTitle(Form("Normalised to data / %.f %s", bin_width, units.c_str()));
        } else {
            ttbar_histo->GetYaxis()->SetTitle(Form("%s / %.f %s", elements.c_str(), bin_width, units.c_str()));
        }
    }

    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        const bool isZplot = (regName == "OS_Zmass" && (m_background_names.at(ibkg) != "Zjets_PP8_FS" && m_background_names.at(ibkg) != "Zjets_Sherpa_FS") );
        auto itr = m_colour_map.find(isZplot ? "Other" : m_background_names.at(ibkg));
        if (itr == m_colour_map.end()) {
            std::cerr << "Plotter::PlotDataMCPlots: Cannot find " << m_background_names.at(ibkg) << ", in the colour map\n";
            exit(EXIT_FAILURE);
        }

        background_histos.at(ibkg)->SetFillColor(itr->second);
        background_histos.at(ibkg)->SetMarkerStyle(21);
        background_histos.at(ibkg)->SetLineColor(itr->second);
        background_histos.at(ibkg)->SetLineWidth(1);
    }

    auto itr = m_colour_map.find(regName == "OS_Zmass" ? "Other" : m_Z_names.at(m_nominal_Z_index));
    if (itr == m_colour_map.end()) {
        std::cerr << "Plotter::PlotDataMCPlots: Cannot find the string in the map, in the colour map\n";
        exit(EXIT_FAILURE);
    }
    background_histos.back()->SetFillColor(itr->second);
    background_histos.back()->SetMarkerStyle(21);
    background_histos.back()->SetLineColor(itr->second);
    background_histos.back()->SetLineWidth(1);

    /// Canvas
    TCanvas c("","",800,600);
    TPad pad1("pad1","pad1",0.0, 0.35, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.35);
    pad1.SetTopMargin(0.08);
    pad1.SetBottomMargin(0.027);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.4);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    /// Remove MC stat uncertainties
    for (int ibin = 1; ibin <= ttbar_histo->GetNbinsX(); ++ibin) {
        ttbar_histo->SetBinError(ibin, 0);
    }

    std::unique_ptr<TH1D> total_up  (static_cast<TH1D*>(ttbar_histo->Clone()));
    std::unique_ptr<TH1D> total_down(static_cast<TH1D*>(ttbar_histo->Clone()));

    if (m_run_syst) {
        GetTotalUpDownUncertainty(total_up.get(), total_down.get(), name, lepton_type, regName);
    }

    TGraphAsymmErrors error(ttbar_histo.get());

    if (m_run_syst) {
        TransformErrorHistoToTGraph(&error, total_up.get(), total_down.get());
    }

    error.SetFillStyle(3354);
    error.SetFillColor(kBlue-7);
    error.SetLineColor(kWhite);
    error.SetMarkerStyle(0);
    error.SetLineWidth(0);

    DrawUpperDataMCPlot(&pad1, ttbar_histo.get(), background_histos, data_histo.get(), &error, name);

    DrawLabels(&pad1, 0.20, 0.82, true, false, lepton_type, regName);

    pad1.RedrawAxis();

    if (m_storeDileptonPt && (lepton_type == Common::LEPTONTYPE::MUMU) && (regName == "OS_Zmass") && (name == "dilepton_pt")) {
        this->StoreDileptonPt(ttbar_histo.get(), data_histo.get());
    }
    if (m_storeLeadingLeptonPt && (lepton_type == Common::LEPTONTYPE::EMU) && (regName == "OS_1b") && (name == "lepton1_pt")) {
        this->StoreLeadingLeptonPt(ttbar_histo.get(), data_histo.get());
    }

    /// Draw legend
    static const std::vector<std::string> exclude = {"Pythia8", "aMCNLO", "Herwig", "Sherpa", "FS", "AFII", "PP8", "dilep","chan"};
    const bool isOSZmass = regName == "OS_Zmass";
    TLegend leg(isOSZmass ? 0.65 : 0.58, isOSZmass ? 0.65 : 0.45, 0.85, 0.88);
    leg.AddEntry(data_histo.get(), "Data", "ep");
    if (regName == "OS_Zmass") {
        leg.AddEntry(ttbar_histo.get(), "Z #rightarrow ll", "f");
        leg.AddEntry(background_histos.at(0).get(), "Other SM", "f");
    } else {
        leg.AddEntry(ttbar_histo.get(), "t#bar{t}", "f");
        for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
            if (m_background_names.at(ibkg) == "ttH_PP8_FS") continue;
            if (m_background_names.at(ibkg) == "ttV_Sherpa_FS") continue;
            if (m_channel == Common::CHANNEL::DILEPTON && m_background_names.at(ibkg) == "Multijet") {
                leg.AddEntry(background_histos.at(ibkg).get(), "Misid. leptons", "f");
                continue;
            }
            auto label_itr = m_label_map.find(m_background_names.at(ibkg));
            if (label_itr == m_label_map.end()) {
                leg.AddEntry(background_histos.at(ibkg).get(), m_background_names.at(ibkg).c_str(), "f");
            } else {
                leg.AddEntry(background_histos.at(ibkg).get(), label_itr->second.c_str(), "f");
            }
        }
        leg.AddEntry(background_histos.back().get(), "Z #rightarrow #tau#tau", "f");
    }
    if (m_run_syst) {
        if (m_syst_shape_only) {
            leg.AddEntry(&error, "Shape unc.", "f");
        } else {
            leg.AddEntry(&error, "Uncertainty", "f");
        }
    }

    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(42);
    leg.SetTextSize(0.06);
    leg.Draw("same");

    std::unique_ptr<TH1D> combined_ratio (static_cast<TH1D*>(ttbar_histo->Clone()));
    std::unique_ptr<TH1D> data_ratio (static_cast<TH1D*>(data_histo->Clone()));

    TGraphAsymmErrors error_ratio(combined_ratio.get());
    for (int ibin = 0; ibin < error_ratio.GetN(); ++ibin) {
        double x,y;
        error_ratio.GetPoint(ibin, x, y);
        error_ratio.SetPoint(ibin, x, 1.);
    }

    std::unique_ptr<TH1D> error_up(static_cast<TH1D*>(total_up->Clone()));
    std::unique_ptr<TH1D> error_down(static_cast<TH1D*>(total_down->Clone()));
    error_up->Divide(combined_ratio.get());
    error_down->Divide(combined_ratio.get());

    error_ratio.SetFillStyle(3354);
    error_ratio.SetFillColor(kBlue-7);
    error_ratio.SetLineColor(kWhite);
    error_ratio.SetMarkerStyle(0);
    error_ratio.SetLineWidth(0);

    if (m_run_syst) {
        TransformErrorHistoToTGraph(&error_ratio, error_up.get(), error_down.get());
    }

    std::vector<std::unique_ptr<TArrow> > arrows;
    DrawRatioDataMCPlot(&pad2, combined_ratio.get(), data_ratio.get(), axis, &error_ratio, arrows, name);

    /// Draw line
    const float min = data_ratio->GetXaxis()->GetBinLowEdge(1);
    const float max = data_ratio->GetXaxis()->GetBinUpEdge(data_ratio->GetNbinsX());
    TLine line (min, 1, max, 1);
    line.SetLineColor(kRed);
    line.SetLineStyle(2);
    line.SetLineWidth(3);
    line.Draw("same");

    std::string log_label("");
    if (logY) {
        pad1.SetLogy();
        log_label = "_log";
    }

    const std::string suffix = Common::TypeToString(lepton_type);

    if (m_channel == Common::CHANNEL::SINGLELEPTON) {
        c.Print(("../Plots/SingleLep/"+m_directory+"/"+regName+"/"+name+log_label+"_"+suffix+".png").c_str());
        c.Print(("../Plots/SingleLep/"+m_directory+"/"+regName+"/"+name+log_label+"_"+suffix+".pdf").c_str());
    }
    if (m_channel == Common::CHANNEL::DILEPTON) {
        c.Print(("../Plots/Dilep/"+m_directory+"/"+regName+"/"+name+log_label+"_"+suffix+".png").c_str());
        c.Print(("../Plots/Dilep/"+m_directory+"/"+regName+"/"+name+log_label+"_"+suffix+".pdf").c_str());
    }
}

void Plotter::CalculateYields(const Common::LEPTONTYPE& lepton_type) const {
    const auto& regVec = (lepton_type == Common::LEPTONTYPE::EMU) ? m_regNamesEMu : m_regNames;
    for (const auto& ireg : regVec) {
        this->CalculateYields(lepton_type, ireg);
    }
}

void Plotter::CalculateYields(const Common::LEPTONTYPE& lepton_type, const std::string& ireg) const {
    std::cout << "Plotter::CalculateYields: Calculating event yields for region " << ireg << "\n";

    std::unique_ptr<std::ofstream> html = std::make_unique<std::ofstream>();
    if (lepton_type == Common::LEPTONTYPE::EL) {
        html->open(("../Plots/SingleLep/"+m_directory+"/"+ireg+"/EventYields_el.html").c_str(), std::ios::trunc);
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        html->open(("../Plots/SingleLep/"+m_directory+"/"+ireg+"/EventYields_mu.html").c_str(), std::ios::trunc);
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        html->open(("../Plots/Dilep/"+m_directory+"/"+ireg+"/EventYields_ee.html").c_str(), std::ios::trunc);
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        html->open(("../Plots/Dilep/"+m_directory+"/"+ireg+"/EventYields_mumu.html").c_str(), std::ios::trunc);
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        html->open(("../Plots/Dilep/"+m_directory+"/"+ireg+"/EventYields_emu.html").c_str(), std::ios::trunc);
    }
    if (!html->is_open() || !html->good()) {
        std::cerr << "Plotter::CalculateYields: Cannot open html page at: " << "../Plots/"+m_directory+"/"+ireg+"/EventYields.html" << std::endl;
        exit(EXIT_FAILURE);
    }
    *html << "<html><head><title>EventYields</title></head><body>"
    			<< "<table border = 1> <tr>"
    			<< "</tr>\n";
    *html << "<td>";
    *html << "<table border style=\"float&#58;left;margin-right:7cm;margin-left:5cm;\">\n";
    *html << "<tr>  ";
    *html << "<td> Type <td> EventYields\n";

    float total(0);

    /// Read the event yields
    std::unique_ptr<TH1D> ttbar(nullptr);
    if (lepton_type == Common::LEPTONTYPE::EL) {
        ttbar.reset(static_cast<TH1D*>(m_ttbar_files_el.at(m_nominal_ttbar_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        ttbar.reset(static_cast<TH1D*>(m_ttbar_files_mu.at(m_nominal_ttbar_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        ttbar.reset(static_cast<TH1D*>(m_ttbar_files_ee.at(m_nominal_ttbar_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        ttbar.reset(static_cast<TH1D*>(m_ttbar_files_mumu.at(m_nominal_ttbar_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        ttbar.reset(static_cast<TH1D*>(m_ttbar_files_emu.at(m_nominal_ttbar_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    }
    if (!ttbar) {
        std::cerr << "Plotter::CalculateYields: Cannot read ttbar histo!" << std::endl;
        exit(EXIT_FAILURE);
    }
    ttbar->Scale(m_scale);
    total+= ttbar->Integral();
    *html << "<tr> <td> " << m_ttbar_names.at(m_nominal_ttbar_index) << " <td> " << std::fixed << std::setprecision(1) << ttbar->Integral() << "\n";

    /// Read the event yields
    std::unique_ptr<TH1D> z(nullptr);
    if (lepton_type == Common::LEPTONTYPE::EL) {
        z.reset(static_cast<TH1D*>(m_Z_files_el.at(m_nominal_Z_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        z.reset(static_cast<TH1D*>(m_Z_files_mu.at(m_nominal_Z_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        z.reset(static_cast<TH1D*>(m_Z_files_ee.at(m_nominal_Z_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        z.reset(static_cast<TH1D*>(m_Z_files_mumu.at(m_nominal_Z_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        z.reset(static_cast<TH1D*>(m_Z_files_emu.at(m_nominal_Z_index)->Get(("nominal/"+ireg+"/jet_n").c_str())));
    }
    if (!z) {
        std::cerr << "Plotter::CalculateYields: Cannot read Z histo!" << std::endl;
        exit(EXIT_FAILURE);
    }
    z->Scale(m_scale);
    total+= z->Integral();
    *html << "<tr> <td> " << m_Z_names.at(m_nominal_Z_index) << " <td> " << std::fixed << std::setprecision(1) << z->Integral() << "\n";

    /// Backgrounds
    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        std::unique_ptr<TH1D> h(nullptr);
        if (lepton_type == Common::LEPTONTYPE::EL) {
            h.reset(static_cast<TH1D*>(m_background_files_el.at(ibkg)->Get(("nominal/"+ireg+"/jet_n").c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MU) {
            h.reset(static_cast<TH1D*>(m_background_files_mu.at(ibkg)->Get(("nominal/"+ireg+"/jet_n").c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EE) {
            h.reset(static_cast<TH1D*>(m_background_files_ee.at(ibkg)->Get(("nominal/"+ireg+"/jet_n").c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
            h.reset(static_cast<TH1D*>(m_background_files_mumu.at(ibkg)->Get(("nominal/"+ireg+"/jet_n").c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EMU) {
            h.reset(static_cast<TH1D*>(m_background_files_emu.at(ibkg)->Get(("nominal/"+ireg+"/jet_n").c_str())));
        }
        if (!h) {
            std::cerr << "Plotter::CalculateYields: Cannot read bkg histos file!" << std::endl;
            exit(EXIT_FAILURE);
        }
        h->Scale(m_scale);
        total+= h->Integral();
        *html << "<tr> <td> " << m_background_names.at(ibkg) << " <td> " << std::fixed << std::setprecision(1) << h->Integral() << "\n";
    }

    /// Total
    *html << "<tr bgcolor=lightblue> <td> Total pred. <td> " << std::fixed << std::setprecision(1) << total << "\n";

    /// Data
    std::unique_ptr<TH1D> data(nullptr);
    if (lepton_type == Common::LEPTONTYPE::EL) {
        data.reset(static_cast<TH1D*>(m_data_file_el->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        data.reset(static_cast<TH1D*>(m_data_file_mu->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        data.reset(static_cast<TH1D*>(m_data_file_ee->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        data.reset(static_cast<TH1D*>(m_data_file_mumu->Get(("nominal/"+ireg+"/jet_n").c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        data.reset(static_cast<TH1D*>(m_data_file_emu->Get(("nominal/"+ireg+"/jet_n").c_str())));
    }
    if (!data) {
        std::cerr << "Plotter::CalculateYields: Cannot read data histo!" << std::endl;
        exit(EXIT_FAILURE);
    }
    *html << "<tr bgcolor=pink> <td> Data <td> " << std::fixed << std::setprecision(1) << data->Integral() << "\n";
    *html << "<tr bgcolor=#66b3ff> <td> " << "Data/MC " << " <td> " << std::fixed << std::setprecision(3) << data->Integral()/total << "\n";

    *html << "</tr>\n";
    *html << "</table>\n";
    *html << "</td>\n";
    html->close();
}

void Plotter::CloseRootFiles() {
    for (auto& ifile : m_ttbar_files_el) {
        ifile->Close();
    }
    for (auto& ifile : m_ttbar_files_mu) {
        ifile->Close();
    }
    for (auto& ifile : m_ttbar_files_ee) {
        ifile->Close();
    }
    for (auto& ifile : m_ttbar_files_mumu) {
        ifile->Close();
    }
    for (auto& ifile : m_ttbar_files_emu) {
        ifile->Close();
    }
    for (auto& ifile : m_Z_files_el) {
        ifile->Close();
    }
    for (auto& ifile : m_Z_files_mu) {
        ifile->Close();
    }
    for (auto& ifile : m_Z_files_ee) {
        ifile->Close();
    }
    for (auto& ifile : m_Z_files_mumu) {
        ifile->Close();
    }
    for (auto& ifile : m_Z_files_emu) {
        ifile->Close();
    }
    for (auto& ifile : m_background_files_el) {
        ifile->Close();
    }
    for (auto& ifile : m_background_files_mu) {
        ifile->Close();
    }
    for (auto& ifile : m_background_files_ee) {
        ifile->Close();
    }
    for (auto& ifile : m_background_files_mumu) {
        ifile->Close();
    }
    for (auto& ifile : m_background_files_emu) {
        ifile->Close();
    }
    for (auto& ifile : m_special_files_el) {
        ifile->Close();
    }
    for (auto& ifile : m_special_files_mu) {
        ifile->Close();
    }
    for (auto& ifile : m_special_files_ee) {
        ifile->Close();
    }
    for (auto& ifile : m_special_files_mumu) {
        ifile->Close();
    }
    for (auto& ifile : m_special_files_emu) {
        ifile->Close();
    }

    if (m_channel == Common::CHANNEL::SINGLELEPTON) {
        m_data_file_el->Close();
        m_data_file_mu->Close();
    }
    if (m_channel == Common::CHANNEL::DILEPTON) {
        m_data_file_ee->Close();
        m_data_file_mumu->Close();
        m_data_file_emu->Close();
    }
}

void Plotter::FillStyleMap() {
    m_styles.push_back(std::make_pair(kBlack, 1));
    m_styles.push_back(std::make_pair(kBlue, 1));
    m_styles.push_back(std::make_pair(kRed, 1));
    m_styles.push_back(std::make_pair(kGreen+2, 1));
    m_styles.push_back(std::make_pair(kMagenta, 1));
}

float Plotter::MaxResize(const std::vector<std::unique_ptr<TH1D> >& histos, const bool& is_log) const {
    float result(-9999);

    for (const auto& ihist : histos) {
        if (result < ihist->GetMaximum()) {
            result = ihist->GetMaximum();
        }
    }

    if (is_log) return result*1e6;

    return result*1.9;
}

void Plotter::DrawLabels(TPad *pad,
                         const float& x,
                         const float& y,
                         const bool& add_lumi,
                         const bool& is_simulation,
                         const Common::LEPTONTYPE& type,
                         const std::string& ireg) const{
    pad->cd();

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.08);
    l1.SetNDC();
    l1.DrawLatex(x, y, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.08);
    l2.SetTextFont(42);
    l2.SetNDC();
    if (is_simulation) {
        l2.DrawLatex(x+0.14, y, ("Simulation " + m_atlas_label).c_str());
    } else {
        l2.DrawLatex(x+0.14, y, m_atlas_label.c_str());
    }

    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.08);
    l3.SetTextFont(42);
    l3.SetNDC();
    if (add_lumi) {
        l3.DrawLatex(x, y-0.08, ("#sqrt{s} = 13.6 TeV, "+m_lumi_label).c_str());
    } else {
        l3.DrawLatex(x, y-0.07, "#sqrt{s} = 13.6 TeV");
    }

    if (ireg == "OS_1b") {
        l3.DrawLatex(x, y-0.17, "e#mu, #geq 1b");
    } else if (ireg == "OS") {
        l3.DrawLatex(x, y-0.17, "e#mu");
    } else {
        l3.DrawLatex(x, y-0.17, Common::RegionToStringPlots(ireg).c_str());
        std::string channel_string("");
        if (type == Common::LEPTONTYPE::EL) {
            channel_string = "el+jets";
        } else if (type == Common::LEPTONTYPE::MU) {
            channel_string = "#mu+jets";
        } else if (type == Common::LEPTONTYPE::EE) {
            channel_string = "ee";
        } else if (type == Common::LEPTONTYPE::MUMU) {
            channel_string = "#mu#mu";
        } else if (type == Common::LEPTONTYPE::EMU) {
            channel_string = "e#mu";
        }
        l3.DrawLatex(x, y-0.25, channel_string.c_str());
    }

    std::string collection;
    /// Add collection name
    if (m_collection == "topo")  {
        collection = "Anti-kt, R=0.4, EMTopo";
        l2.DrawLatex(x, y-0.15, collection.c_str());
    } else if (m_collection == "pflow") {
        collection = "Anti-kt, R=0.4, EMPFlow";
        l2.DrawLatex(x, y-0.15, collection.c_str());
    }

}

void Plotter::DrawUpperDataMCPlot(TPad* pad,
                                  TH1D* ttbar_histo,
                                  const std::vector<std::unique_ptr<TH1D> >& background_histos,
                                  TH1D* data_histo,
                                  TGraphAsymmErrors* error,
                                  const std::string& name) const {

    pad->cd();
    data_histo->SetMarkerStyle(20);
    data_histo->SetMarkerSize(1.45);
    data_histo->SetLineColor(kBlack);
    data_histo->SetLineStyle(1);

    ttbar_histo->GetXaxis()->SetLabelOffset(1000);
    ttbar_histo->GetYaxis()->SetLabelSize(0.07);
    ttbar_histo->GetYaxis()->SetLabelFont(42);
    ttbar_histo->GetYaxis()->SetLabelOffset(0.01);
    ttbar_histo->GetYaxis()->SetTitleFont(42);
    ttbar_histo->GetYaxis()->SetTitleSize(0.08);
    ttbar_histo->GetYaxis()->SetTitleOffset(0.9);

    if ((name.find("jet_n") != std::string::npos) || (name.find("jet_tagbin") != std::string::npos)) {
        for (int ibin = 1; ibin <= ttbar_histo->GetNbinsX(); ++ibin) {
            ttbar_histo->GetXaxis()->SetBinLabel(ibin, "tmp");
        }
    }
    ttbar_histo->Draw("HIST");
    for (auto& ibkg : background_histos) {
        ibkg->Draw("HIST SAME");
    }

    data_histo->Draw("X0EPZ SAME");
    error->Draw("E2 SAME");

}

void Plotter::DrawRatioDataMCPlot(TPad* pad,
                                  TH1D* combined,
                                  TH1D* data,
                                  const std::string& axis,
                                  TGraphAsymmErrors* error,
                                  std::vector<std::unique_ptr<TArrow> >& arrows,
                                  const std::string& name) const {

    // set all MC stat to 0
    for (int ibin = 0; ibin <= combined->GetNbinsX(); ++ibin) {
      combined->SetBinError(ibin, 0.);
    }

    pad->cd();

    const double min = m_syst_shape_only ? 0.8 : 0.7;
    const double max = m_syst_shape_only ? 1.2 : 1.3;

    data->Divide(combined);
    data->SetMarkerStyle(8);
    data->SetMarkerSize(1);
    if (name.find("jet_n") != std::string::npos) {
        for (int ibin = 1; ibin <= data->GetNbinsX(); ++ibin) {
            const int center = static_cast<int>(data->GetXaxis()->GetBinCenter(ibin));
            if (ibin < data->GetNbinsX()) {
                data->GetXaxis()->SetBinLabel(ibin, std::to_string(center).c_str());
            } else {
                data->GetXaxis()->SetBinLabel(ibin, ("#geq" + std::to_string(center)).c_str());
            }
        }
        data->GetXaxis()->SetLabelFont(42);
        data->GetXaxis()->SetLabelSize(0.25);
        data->GetXaxis()->SetLabelOffset(0.03);
        data->GetXaxis()->SetTitleFont(42);
        data->GetXaxis()->SetTitleSize(0.25);
        data->GetXaxis()->SetTitleOffset(0.9);
    } else if (name.find("jet_tagbin") != std::string::npos) {
        for (int ibin = 1; ibin <= data->GetNbinsX(); ++ibin) {
            if (ibin == 1) {
                data->GetXaxis()->SetBinLabel(ibin, ">85% WP");
            } else if (ibin == 2) {
                data->GetXaxis()->SetBinLabel(ibin, "85% WP");
            } else if (ibin == 3) {
                data->GetXaxis()->SetBinLabel(ibin, "77% WP");
            } else if (ibin == 4) {
                data->GetXaxis()->SetBinLabel(ibin, "70% WP");
            } else if (ibin == 5) {
                data->GetXaxis()->SetBinLabel(ibin, "60% WP");
            }
        }
        data->GetXaxis()->SetLabelFont(42);
        data->GetXaxis()->SetLabelSize(0.25);
        data->GetXaxis()->SetLabelOffset(0.03);
        data->GetXaxis()->SetTitleFont(42);
    } else {
        data->GetXaxis()->SetLabelFont(42);
        data->GetXaxis()->SetLabelSize(0.15);
        data->GetXaxis()->SetLabelOffset(0.01);
        data->GetXaxis()->SetTitleFont(42);
    }
    data->GetXaxis()->SetTitleOffset(1.1);
    data->GetXaxis()->SetTitleSize(0.15);
    data->GetXaxis()->SetTitle(axis.c_str());

    data->GetYaxis()->SetRangeUser(min,max);
    data->GetYaxis()->SetLabelFont(42);
    data->GetYaxis()->SetLabelSize(0.15);
    data->GetYaxis()->SetLabelOffset(0.03);
    data->GetYaxis()->SetTitleFont(42);
    data->GetYaxis()->SetTitleSize(0.15);
    data->GetYaxis()->SetTitleOffset(0.50);
    data->GetYaxis()->SetNdivisions(503);
    data->GetYaxis()->SetTitle("Data/Pred.");

    data->Draw("EX0P SAME");
    error->Draw("E2 SAME");

    for (int ibin = 1; ibin <= data->GetNbinsX(); ++ibin) {
        const double val = data->GetBinContent(ibin);
        if (combined->GetBinContent(ibin) < 1e-9) continue;

        int isUp(0);
        if (val < min) {
            isUp = -1;
        } else if (val > max) {
            isUp = 1;
        }

        if (isUp == 0) continue;
        std::unique_ptr<TArrow> arrow(nullptr);
        if (isUp == 1) arrow = std::make_unique<TArrow>(data->GetXaxis()->GetBinCenter(ibin),max-0.05*(max-min), data->GetXaxis()->GetBinCenter(ibin),max,0.02,"|>");
        else           arrow = std::make_unique<TArrow>(data->GetXaxis()->GetBinCenter(ibin),min+0.05*(max-min), data->GetXaxis()->GetBinCenter(ibin),min,0.02,"|>");
        arrow->SetFillColor(10);
        arrow->SetFillStyle(1001);
        arrow->SetLineColor(kBlue-7);
        arrow->SetLineWidth(2);
        arrow->SetAngle(40);
        arrow->Draw();
        arrows.emplace_back(std::move(arrow));
    }
}

void Plotter::SetColourMap() {
    m_colour_map["SingleTop_PP8_s_chan_FS"] = kBlue-1;
    m_colour_map["SingleTop_PP8_t_chan_FS"] = kBlue+1;
    m_colour_map["SingleTop_PP8_tW_chan_FS"] = kBlue;
    m_colour_map["SingleTop_PP8_tW_chan_dilep_FS"] = kBlue;
    m_colour_map["Wjets_Sherpa_FS"] = 92;
    m_colour_map["Zjets_Sherpa_FS"] = 95;
    m_colour_map["Zjets_PP8_FS"] = 95;
    m_colour_map["Diboson_Sherpa_FS"] = 5;
    m_colour_map["ttV_aMCNLO_Pythia8_FS"] = kWhite;
    m_colour_map["ttV_Sherpa_FS"] = kWhite;
    m_colour_map["ttH_PP8_FS"] = kRed;
    m_colour_map["Multijet"] = 619;
    m_colour_map["Other"] = kGreen+3;
}

void Plotter::GetTotalUpDownUncertainty(TH1D* total_up,
                                        TH1D* total_down,
                                        const std::string& name,
                                        const Common::LEPTONTYPE& lepton_type,
                                        const std::string& regName) const {
    /// Set bin content to 0
    const int nbins = total_up->GetNbinsX();

    if (total_down->GetNbinsX() != nbins) {
        std::cerr << "Plotter::GetTotalUpDownUncertainty: Up and Down histograms have different number of bins!" << std::endl;
        exit(EXIT_FAILURE);
    }

    for (int ibin = 1; ibin <= nbins; ++ibin) {
        total_up->SetBinContent(ibin, 0);
        total_up->SetBinError(ibin, 0);
        total_down->SetBinContent(ibin, 0);
        total_down->SetBinError(ibin, 0);
    }

    /// Loop over systematics and add uncertainties
    for (const auto& isyst : m_systematics) {
        AddSingleSyst(total_up, total_down, name, isyst, lepton_type, regName);
    }
}

void Plotter::AddSingleSyst(TH1D* total_up,
                            TH1D* total_down,
                            const std::string& name,
                            const Systematic& systematic,
                            const Common::LEPTONTYPE& lepton_type,
                            const std::string& regName) const {

    std::string in_file_up = systematic.up_file;
    std::string in_file_down = systematic.down_file;
    if (in_file_down == "") in_file_down = in_file_up;

    std::string up_name = systematic.up_histo + "/" + regName + "/" + name;
    std::string down_name = systematic.down_histo + "/" + regName + "/" + name;
    if (systematic.type == SYSTEMATICTYPE::TWOSIDED && down_name == "") {
        std::cerr << "Plotter::AddSingleSyst: You need to provide both up and down name for TWOSIDED systematic" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (down_name == "") down_name == up_name;

    std::string reference_file = systematic.reference_file;
    if (reference_file == "") reference_file = in_file_up;

    std::string reference_histo = "nominal/"+regName+"/"+name;
    if (systematic.reference_histo != "") reference_histo = systematic.reference_histo + "/" + regName + "/" + name;

    const bool scale = !((m_channel == Common::CHANNEL::SINGLELEPTON) && (in_file_up == "Multijet"));

    /// Read the histograms
    std::unique_ptr<TH1D> histo_nominal = GetHistoFromAll(reference_file, reference_histo, lepton_type, scale);
    std::unique_ptr<TH1D> histo_up(nullptr);
    std::unique_ptr<TH1D> histo_down(nullptr);

    if (!histo_nominal) {
        std::cerr << "Plotter::AddSingleSyst: Cannot read the necessary histograms in " << reference_file << " histo: " << reference_histo << std::endl;
        exit(EXIT_FAILURE);
    }

    if (systematic.type == SYSTEMATICTYPE::TWOSIDED) {
        histo_up   = GetHistoFromAll(in_file_up, up_name, lepton_type, scale);
        histo_down = GetHistoFromAll(in_file_down, down_name, lepton_type, scale);
    } else if (systematic.type == SYSTEMATICTYPE::ONESIDED) {
        histo_up   = GetHistoFromAll(in_file_up, up_name, lepton_type, scale);
        /// Symmetrise it
        std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(histo_up->Clone()));
        tmp->Add(histo_nominal.get(), -1);
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down->Add(tmp.get(), -1);
    } else if (systematic.type == SYSTEMATICTYPE::NORMALISATION) {
        histo_up   = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_up  ->Scale(1.+systematic.norm_up);
        histo_down->Scale(1.-systematic.norm_down);
    } else if (systematic.type == SYSTEMATICTYPE::MCSTAT) {
        histo_up   = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));

        for (int ibin = 1; ibin <= histo_nominal->GetNbinsX(); ++ibin) {
            const double nominal = histo_nominal->GetBinContent(ibin);
            const double unc = histo_nominal->GetBinError(ibin);
            
            histo_up  ->SetBinContent(ibin, nominal + unc);
            histo_down->SetBinContent(ibin, nominal - unc);
        }
    }

    if (!histo_up || !histo_down) {
        std::cerr << "Plotter::AddSingleSyst: Cannot read the necessary histograms for systematic up file: " << systematic.up_file << ", down file: " << systematic.down_file << ", histo up: " << up_name << ", down: " << down_name << std::endl;
        exit(EXIT_FAILURE);
    }

    if (total_up->GetNbinsX() != histo_down->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Total up and up syst histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (histo_up->GetNbinsX() != histo_down->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Up and down syst histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (histo_up->GetNbinsX() != histo_nominal->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Up syst and nominal histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// subtract the pseudodata, i.e. Variation - PseudoData + nominal
    if (systematic.subtractUpName != "") {
        std::unique_ptr<TH1D> sub = GetHistoFromAll(in_file_up, systematic.subtractUpName+"/" + regName + "/"+name, lepton_type, scale);
        if (!sub) {
            std::cerr << "Plotter::AddSingleSyst: Cannot find the subtract histogram for file: " << in_file_up << ", histo: " <<  systematic.subtractUpName+"/" + regName + "/"+name << "\n";
            exit(EXIT_FAILURE);
        }
        histo_up->Add(sub.get(), -1);
        histo_up->Add(histo_nominal.get());
    }

    if (systematic.subtractDownName != "") {
        std::unique_ptr<TH1D> sub = GetHistoFromAll(in_file_up, systematic.subtractDownName+"/"+regName +"/"+name, lepton_type, scale);
        if (!sub) {
            std::cerr << "Plotter::AddSingleSyst: Cannot find the subtract histogram: " << systematic.subtractDownName+"/"+regName + "/"+name << "\n";
            exit(EXIT_FAILURE);
        }
        histo_down->Add(sub.get(), -1);
        histo_down->Add(histo_nominal.get());
    }

    if (m_syst_shape_only) {
        if (std::fabs(histo_up->Integral()) > 1e-6) histo_up->Scale(histo_nominal->Integral()/histo_up->Integral());
        if (std::fabs(histo_down->Integral()) > 1e-6) histo_down->Scale(histo_nominal->Integral()/histo_down->Integral());
    }

    AddHistosInSquares(total_up, total_down, histo_up.get(), histo_down.get(), histo_nominal.get(), systematic.scale);
}

std::unique_ptr<TH1D> Plotter::GetHistoFromAll(const std::string& file_name,
                                               const std::string& histo_name,
                                               const Common::LEPTONTYPE& lepton_type,
                                               const bool scale) const {
    std::unique_ptr<TH1D> result(nullptr);
    /// search signal files
    auto itr_signal = std::find(m_ttbar_names.begin(), m_ttbar_names.end(), file_name);
    if (itr_signal != m_ttbar_names.end()) {
        const std::size_t pos_signal = std::distance(m_ttbar_names.begin(), itr_signal);
        if (lepton_type == Common::LEPTONTYPE::EL) {
            result =  std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_files_el.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MU) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_files_mu.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EE) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_files_ee.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_files_mumu.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EMU) {
            result =  std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_files_emu.at(pos_signal)->Get(histo_name.c_str())));
        }

        if (result && scale) result->Scale(m_scale);
        return result;
    }

    auto itr_z = std::find(m_Z_names.begin(), m_Z_names.end(), file_name);
    if (itr_z != m_Z_names.end()) {
        const std::size_t pos_signal = std::distance(m_Z_names.begin(), itr_z);
        if (lepton_type == Common::LEPTONTYPE::EL) {
            result =  std::unique_ptr<TH1D>(static_cast<TH1D*>(m_Z_files_el.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MU) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_Z_files_mu.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EE) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_Z_files_ee.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_Z_files_mumu.at(pos_signal)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EMU) {
            result =  std::unique_ptr<TH1D>(static_cast<TH1D*>(m_Z_files_emu.at(pos_signal)->Get(histo_name.c_str())));
        }

        if (result && scale) result->Scale(m_scale);
        return result;
    }

    /// Try bkg file
    auto itr_background = std::find(m_background_names.begin(), m_background_names.end(), file_name);
    if (itr_background != m_background_names.end()) {
        const std::size_t pos_bkg = std::distance(m_background_names.begin(), itr_background);

        if (lepton_type == Common::LEPTONTYPE::EL) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files_el.at(pos_bkg)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MU) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files_mu.at(pos_bkg)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EE) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files_ee.at(pos_bkg)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files_mumu.at(pos_bkg)->Get(histo_name.c_str())));
        } else if (lepton_type == Common::LEPTONTYPE::EMU) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files_emu.at(pos_bkg)->Get(histo_name.c_str())));
        }

        if (result && scale) result->Scale(m_scale);
        return result;
    }


    /// Has to be special file
    auto itr_special = std::find(m_special_names.begin(), m_special_names.end(), file_name);
    if (itr_special == m_special_names.end()) {
        std::cerr << "Plotter::GetHistoFromAll: Cannot find file: " << file_name << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::size_t pos_special = std::distance(m_special_names.begin(), itr_special);

    if (lepton_type == Common::LEPTONTYPE::EL) {
        result.reset(static_cast<TH1D*>(m_special_files_el.at(pos_special)->Get(histo_name.c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MU) {
        result.reset(static_cast<TH1D*>(m_special_files_mu.at(pos_special)->Get(histo_name.c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EE) {
        result.reset(static_cast<TH1D*>(m_special_files_ee.at(pos_special)->Get(histo_name.c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::MUMU) {
        result.reset(static_cast<TH1D*>(m_special_files_mumu.at(pos_special)->Get(histo_name.c_str())));
    } else if (lepton_type == Common::LEPTONTYPE::EMU) {
        result.reset(static_cast<TH1D*>(m_special_files_emu.at(pos_special)->Get(histo_name.c_str())));
    }

    if (result && scale) result->Scale(m_scale);
    return result;
}

void Plotter::AddHistosInSquares(TH1D* total_up,
                                 TH1D* total_down,
                                 TH1D* histo_up,
                                 TH1D* histo_down,
                                 TH1D* histo_nominal,
                                 const double scale) const {

    /// We know the histograms have the same binning
    const int nbins = total_up->GetNbinsX();
    for (int ibin = 1; ibin <= nbins; ++ibin) {
        double up(0);
        double down(0);
        if (histo_up->GetBinContent(ibin) > histo_nominal->GetBinContent(ibin)) {
            const double diff = scale*(histo_up->GetBinContent(ibin) - histo_nominal->GetBinContent(ibin));
            up += std::hypot(up, diff);
        } else {
            const double diff = scale*(histo_nominal->GetBinContent(ibin) - histo_up->GetBinContent(ibin));
            down += std::hypot(down, diff);
        }
        if (histo_down->GetBinContent(ibin) > histo_nominal->GetBinContent(ibin)) {
            const double diff = scale*(histo_down->GetBinContent(ibin) - histo_nominal->GetBinContent(ibin));
            up += std::hypot(up, diff);
        } else {
            const double diff = scale*(histo_nominal->GetBinContent(ibin) - histo_down->GetBinContent(ibin));
            down += std::hypot(down, diff);
        }

        /// now add it to the total hist
        const double current_up   = total_up->GetBinContent(ibin);
        const double current_down = total_down->GetBinContent(ibin);

        total_up  ->SetBinContent(ibin,  std::hypot(current_up, up));
        total_down->SetBinContent(ibin, -std::hypot(current_down, down));
    }
}

void Plotter::TransformErrorHistoToTGraph(TGraphAsymmErrors* error,
                                          TH1D* up,
                                          TH1D* down) const {
    const int nbins = error->GetN();

    for (int ibin = 0; ibin < nbins; ++ibin) {
        error->SetPointEYhigh(ibin, up->GetBinContent(ibin+1));
        error->SetPointEYlow (ibin, -down->GetBinContent(ibin+1));
    }
}

void Plotter::SetLabelMap() {
    m_label_map["ttbar_PP8_FS"] = "t#bar{t}";
    m_label_map["ttbar_PP8_AFII"] = "t#bar{t}";
    m_label_map["ttbar_PP8_dilep_FS"] = "t#bar{t}";
    m_label_map["ttbar_PP8_dilep_AFII"] = "t#bar{t}";
    m_label_map["Wjets_Sherpa_FS"] = "W+jets";
    m_label_map["Zjets_Sherpa_FS"] = "Z #rightarrow ll";
    m_label_map["Zjets_PP8_FS"] = "Z #rightarrow ll";
    m_label_map["Diboson_Sherpa_FS"] = "Diboson";
    m_label_map["ttV_aMCNLO_Pythia8_FS"] = "t#bar{t}";
    m_label_map["ttV_Sherpa_FS"] = "t#bar{t}";
    m_label_map["ttH_PP8_FS"] = "t#bar{t}+V";
    m_label_map["SingleTop_PP8_s_chan_FS"] = "SingleTop s-chan";
    m_label_map["SingleTop_PP8_t_chan_FS"] = "SingleTop t-chan";
    m_label_map["SingleTop_PP8_tW_chan_FS"] = "SingleTop tW-chan";
    m_label_map["SingleTop_PP8_tW_chan_dilep_FS"] = "SingleTop tW-channel";
    m_label_map["SingleTop_PP8_tW_chan_dilep_FS"] = "SingleTop tW-channel";
}

void Plotter::StoreActualMu(const TH1D* total, const TH1D* data) const {
    std::unique_ptr<TH1D> totalCopy(static_cast<TH1D*>(total->Clone()));
    std::unique_ptr<TH1D> dataCopy(static_cast<TH1D*>(data->Clone()));

    totalCopy->Scale(dataCopy->Integral()/totalCopy->Integral());

    dataCopy->Divide(totalCopy.get());

    std::unique_ptr<TFile> f(TFile::Open("data/PileupReweight.root","RECREATE"));
    if (!f) {
        std::cerr << "Plotter::StoreActualMu: Cannot open file at data/PileupReweight.root\n";
        std::exit(EXIT_FAILURE);
    }

    f->cd();
    dataCopy->Write("actual_mu");
    f->Close();
}

void Plotter::StoreDileptonPt(const TH1D* total, const TH1D* data) const {
    std::unique_ptr<TH1D> totalCopy(static_cast<TH1D*>(total->Clone()));
    std::unique_ptr<TH1D> dataCopy(static_cast<TH1D*>(data->Clone()));

    totalCopy->Scale(dataCopy->Integral()/totalCopy->Integral());

    dataCopy->Divide(totalCopy.get());

    std::unique_ptr<TFile> f(TFile::Open("data/DileptonPt.root","RECREATE"));
    if (!f) {
        std::cerr << "Plotter::StoreDileptonPt: Cannot open file at data/DileptonPt.root\n";
        std::exit(EXIT_FAILURE);
    }

    f->cd();
    dataCopy->Write("dilepton_pt");
    f->Close();
}

void Plotter::StoreLeadingLeptonPt(const TH1D* total, const TH1D* data) const {
    std::unique_ptr<TH1D> totalCopy(static_cast<TH1D*>(total->Clone()));
    std::unique_ptr<TH1D> dataCopy(static_cast<TH1D*>(data->Clone()));

    totalCopy->Scale(dataCopy->Integral()/totalCopy->Integral());

    dataCopy->Divide(totalCopy.get());

    std::unique_ptr<TFile> f(TFile::Open("data/LeadingLeptonPt.root","RECREATE"));
    if (!f) {
        std::cerr << "Plotter::StoreLeadingLeptonPt: Cannot open file at data/LeadingLeptonPt.root\n";
        std::exit(EXIT_FAILURE);
    }

    f->cd();
    dataCopy->Write("lepton1_pt");
    f->Close();
}
