#include "TTbarXsec/Comparator.h"
#include "TTbarXsec/Common.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TCanvas.h"
#include "TDirectory.h"
#include "TH1D.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLine.h"
#include "TPad.h"
#include "TSystem.h"

#include <iostream>
#include <memory>

Comparator::Comparator() :
    m_folder1_name(""),
    m_file1_name(""),
    m_histo1_name(""),
    m_histo1_dn_name(""),
    m_folder2_name(""),
    m_file2_name(""),
    m_histo2_name(""),
    m_normalise(false),
    m_has_up_down(false) {

    SetAtlasStyle();
    AddAllLabels();
}

void Comparator::SetLeptonType(const Common::LEPTONTYPE& type) {

    m_lepton_type = type;
}

void Comparator::SetFirstHisto(const std::string& folder,
                               const std::string& file,
                               const std::string& name,
                               const std::string& name_dn) {
    m_folder1_name = folder;
    m_file1_name = file;
    m_histo1_name = name;
    m_histo1_dn_name = name_dn;

    if (name_dn != "") {
        m_has_up_down = true;
    }
}

void Comparator::SetSecondHisto(const std::string& folder,
                                const std::string& file,
                                const std::string& name) {
    m_folder2_name = folder;
    m_file2_name = file;
    m_histo2_name = name;
}

void Comparator::OpenRootFiles() {
    std::string path1{""};
    std::string path2{""};
    if (m_lepton_type == Common::LEPTONTYPE::EL) {
        path1 = "../OutputHistos/SingleLep/"+m_folder1_name+"/"+m_file1_name+"_"+m_folder1_name+"_el.root";
        path2 = "../OutputHistos/SingleLep/"+m_folder2_name+"/"+m_file2_name+"_"+m_folder2_name+"_el.root";
    } else if (m_lepton_type == Common::LEPTONTYPE::MU) {
        path1 = "../OutputHistos/SingleLep/"+m_folder1_name+"/"+m_file1_name+"_"+m_folder1_name+"_mu.root";
        path2 = "../OutputHistos/SingleLep/"+m_folder2_name+"/"+m_file2_name+"_"+m_folder2_name+"_mu.root";
    } else if (m_lepton_type == Common::LEPTONTYPE::ELMU) {
        path1 = "../OutputHistos/SingleLep/"+m_folder1_name+"/"+m_file1_name+"_"+m_folder1_name+"_elmu.root";
        path2 = "../OutputHistos/SingleLep/"+m_folder2_name+"/"+m_file2_name+"_"+m_folder2_name+"_elmu.root";
    } else if (m_lepton_type == Common::LEPTONTYPE::EE) {
        path1 = "../OutputHistos/Dilep/"+m_folder1_name+"/"+m_file1_name+"_"+m_folder1_name+"_ee.root";
        path2 = "../OutputHistos/Dilep/"+m_folder2_name+"/"+m_file2_name+"_"+m_folder2_name+"_ee.root";
    } else if (m_lepton_type == Common::LEPTONTYPE::MUMU) {
        path1 = "../OutputHistos/Dilep/"+m_folder1_name+"/"+m_file1_name+"_"+m_folder1_name+"_mumu.root";
        path2 = "../OutputHistos/Dilep/"+m_folder2_name+"/"+m_file2_name+"_"+m_folder2_name+"_mumu.root";
    } else if (m_lepton_type == Common::LEPTONTYPE::EMU) {
        path1 = "../OutputHistos/Dilep/"+m_folder1_name+"/"+m_file1_name+"_"+m_folder1_name+"_emu.root";
        path2 = "../OutputHistos/Dilep/"+m_folder2_name+"/"+m_file2_name+"_"+m_folder2_name+"_emu.root";
    } 
    m_file1 = std::unique_ptr<TFile>(TFile::Open(path1.c_str()));
    m_file2 = std::unique_ptr<TFile>(TFile::Open(path2.c_str()));

    if (!m_file1 || !m_file2) {
        std::cout << "Comparator::OpenRootFiles: Cannot open the ROOT files" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void Comparator::GetHistoList() {
    if (m_regionNames.empty()) {
        std::cerr << "Comparator::GetHistoList: Region name list is empty\n";
        exit(EXIT_FAILURE);
    }

    m_file1->cd((m_histo1_name+"/"+m_regionNames.at(0)).c_str());

    TIter next(gDirectory->GetListOfKeys());
    TKey *key;
    while ((key = static_cast<TKey*>(next()))) {
        if (key->IsFolder()) continue;
        m_histo_list.emplace_back(key->GetName());
    }
}

void Comparator::PlotAllHistos() {
    const std::string suffix = m_normalise ? "_shapeOnly" : "";
    const std::string out_name = m_folder1_name+"_"+m_file1_name+"_"+m_histo1_name+m_folder2_name+"_"+m_file2_name+"_"+m_histo2_name+suffix;
    if (m_lepton_type == Common::LEPTONTYPE::EL || m_lepton_type == Common::LEPTONTYPE::MU || m_lepton_type == Common::LEPTONTYPE::ELMU) {
        gSystem->mkdir(("../Comparison/SingleLep/"+out_name).c_str());
    } else {
        gSystem->mkdir(("../Comparison/Dilep/"+out_name).c_str());
    }
    
    for (const auto& ireg : m_regionNames) {
        if (m_lepton_type == Common::LEPTONTYPE::EL || m_lepton_type == Common::LEPTONTYPE::MU || m_lepton_type == Common::LEPTONTYPE::ELMU) {
            gSystem->mkdir(("../Comparison/SingleLep/"+out_name+"/"+ireg).c_str());
        } else {
            gSystem->mkdir(("../Comparison/Dilep/"+out_name+"/"+ireg).c_str());
        }
        for (const auto& iname : m_histo_list) {
            std::cout << "Comparator::PlotAllHistos: Plotting: " << iname << " for region " << ireg << "\n";
            PlotSingleHisto(iname, ireg);
        }
    }
}

void Comparator::CloseRootFiles() {
    m_file1->Close();
    m_file2->Close();
}

void Comparator::PlotSingleHisto(const std::string& name, const std::string& ireg) const {
    std::unique_ptr<TH1D> h1(static_cast<TH1D*>(m_file1->Get((m_histo1_name+"/"+ireg+"/"+name).c_str())));
    std::unique_ptr<TH1D> h2(static_cast<TH1D*>(m_file2->Get((m_histo2_name+"/"+ireg+"/"+name).c_str())));
    std::unique_ptr<TH1D> h1_dn(nullptr);
    if (m_has_up_down) {
        h1_dn.reset(static_cast<TH1D*>(m_file1->Get((m_histo1_dn_name+"/"+ireg+"/"+name).c_str())));
    }

    if (!h1 || !h2) {
        std::cout << "Comparator::PlotSingleHisto: Cannot read one of the histograms, skipping\n";
        return;
    }

    if (m_has_up_down && !h1_dn) {
        std::cout << "Comparator::PlotSingleHisto: Cannot read the down histograms, skipping\n";
        return;
    }

    if (m_normalise) {
        h1->Scale(1./h1->Integral());
        h2->Scale(1./h2->Integral());
        if (m_has_up_down) h1_dn->Scale(1./h1_dn->Integral());
    }

    auto itr = m_labels_map.find(name);
    if (itr == m_labels_map.end()) {
        std::cout << "Comparator::PlotSingleHisto: Cannot find labels map for: " << name << ", skipping\n";
        return;
    }

    const Labels label = itr->second;
    
    /// Canvas
    TCanvas c("","",800,600);
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.5);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    pad1.cd();

    h1->SetLineColor(kBlack);
    h2->SetLineColor(kRed);
    if (m_has_up_down) h1_dn->SetLineColor(kBlue);

    const float max = Common::GetMax(h1.get(),h2.get());
    h1->GetYaxis()->SetRangeUser(0.0001, 1.6*max);
    const float bin_width = h1->GetBinWidth(1);
    if (m_normalise) {
        if (bin_width < 1) {
            h1->GetYaxis()->SetTitle(Form("Normalised / %.2f %s", bin_width, label.units.c_str()));
        } else {
            h1->GetYaxis()->SetTitle(Form("Normalised / %.1f %s", bin_width, label.units.c_str()));
        }
    } else {
        if (bin_width < 1) {
            h1->GetYaxis()->SetTitle(Form("Events / %.2f %s", bin_width, label.units.c_str()));
        } else {
            h1->GetYaxis()->SetTitle(Form("Events / %.1f %s", bin_width, label.units.c_str()));
        }
    }

    h1->GetYaxis()->SetLabelSize(0.05);
    h1->GetYaxis()->SetLabelFont(42);
    h1->GetYaxis()->SetTitleFont(42);
    h1->GetYaxis()->SetTitleSize(0.07);
    h1->GetYaxis()->SetTitleOffset(1.1);
    h1->Draw("HIST");
    h2->Draw("HIST same");
    if (m_has_up_down) h1_dn->Draw("HIST same");

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.06);
    l1.SetNDC();
    l1.DrawLatex(0.21, 0.88, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.06);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(0.33, 0.88, "Internal");
    
    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.06);
    l3.SetTextFont(42);
    l3.SetNDC();
    l3.DrawLatex(0.21, 0.81, "#sqrt{s} = 13.6 TeV");

    TLegend leg(0.55, 0.75, 0.9, 0.92);
    leg.AddEntry(h1.get(), m_file1_name.c_str(), "l");
    leg.AddEntry(h2.get(), m_file2_name.c_str(), "l");
    if (m_has_up_down) leg.AddEntry(h1_dn.get(), m_file1_name.c_str(), "l");
    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(72);
    leg.SetTextSize(0.045);
    if (m_file1_name != m_file2_name) {
        leg.Draw("same");
    }
    
    TLegend leg2(0.55, 0.60, 0.90, 0.70);
    leg2.AddEntry(h1.get(), m_folder1_name.c_str(), "l");
    leg2.AddEntry(h2.get(), m_folder2_name.c_str(), "l");
    if (m_has_up_down) leg2.AddEntry(h1_dn.get(), m_folder1_name.c_str(), "l");
    leg2.SetFillColor(0);
    leg2.SetLineColor(0);
    leg2.SetBorderSize(0);
    leg2.SetTextFont(72);
    leg2.SetTextSize(0.045);
    if (m_folder1_name != m_folder2_name) {
        leg2.Draw("same");
    }

    TLegend leg3(0.20, 0.65, 0.40, 0.75);
    leg3.AddEntry(h1.get(), m_histo1_name.c_str(), "l");
    leg3.AddEntry(h2.get(), m_histo2_name.c_str(), "l");
    if (m_has_up_down) leg3.AddEntry(h1_dn.get(), m_histo1_dn_name.c_str(), "l");
    leg3.SetFillColor(0);
    leg3.SetLineColor(0);
    leg3.SetBorderSize(0);
    leg3.SetTextFont(72);
    leg3.SetTextSize(0.045);
    if (m_histo1_name != m_histo2_name) {
        leg3.Draw("same");
    }

    pad1.RedrawAxis();

    pad2.cd();
    std::unique_ptr<TH1D> ratio(static_cast<TH1D*>(h1->Clone()));
    ratio->Divide(h2.get());

    std::unique_ptr<TH1D> ratio_dn(nullptr);
    if (m_has_up_down) {
        ratio_dn.reset(static_cast<TH1D*>(h1_dn->Clone()));
        ratio_dn->Divide(h2.get());
    }
    
    ratio->GetXaxis()->SetLabelFont(42);
    ratio->GetXaxis()->SetLabelSize(0.15);
    ratio->GetXaxis()->SetLabelOffset(0.01);
    ratio->GetXaxis()->SetTitleFont(42);
    ratio->GetXaxis()->SetTitleSize(0.20);
    ratio->GetXaxis()->SetTitleOffset(0.9);
    ratio->GetXaxis()->SetNdivisions(505);
    ratio->GetXaxis()->SetTitle(label.x_axis.c_str());
    ratio->GetYaxis()->SetRangeUser(0.8,1.2);
    ratio->GetYaxis()->SetLabelFont(42);
    ratio->GetYaxis()->SetLabelSize(0.15);
    ratio->GetYaxis()->SetLabelOffset(0.03);
    ratio->GetYaxis()->SetTitleFont(42);
    ratio->GetYaxis()->SetTitleSize(0.15);
    ratio->GetYaxis()->SetTitleOffset(0.5);
    ratio->GetYaxis()->SetNdivisions(505);
    ratio->GetYaxis()->SetTitle("ratio");
    
    ratio->Draw("HIST");
    if (m_has_up_down) {
        ratio_dn->Draw("HIST same");
    }
    
    /// Draw line
    const float min_line = ratio->GetXaxis()->GetBinLowEdge(1);
    const float max_line = ratio->GetXaxis()->GetBinUpEdge(ratio->GetNbinsX());
    TLine line(min_line, 1, max_line, 1);
    line.SetLineColor(kRed);
    line.SetLineStyle(2);
    line.SetLineWidth(3);
    line.Draw("same");

    pad2.RedrawAxis();

    const std::string suffix = m_normalise ? "_shapeOnly" : "";
    const std::string out_name = m_folder1_name+"_"+m_file1_name+"_"+m_histo1_name+m_folder2_name+"_"+m_file2_name+"_"+m_histo2_name+suffix;

    if (m_lepton_type == Common::LEPTONTYPE::EL) {
        c.Print(("../Comparison/SingleLep/"+out_name+"/"+ireg+"/"+name+"_el.png").c_str());
        c.Print(("../Comparison/SingleLep/"+out_name+"/"+ireg+"/"+name+"_el.pdf").c_str());
    } else if (m_lepton_type == Common::LEPTONTYPE::MU) {
        c.Print(("../Comparison/SingleLep/"+out_name+"/"+ireg+"/"+name+"_mu.png").c_str());
        c.Print(("../Comparison/SingleLep/"+out_name+"/"+ireg+"/"+name+"_mu.pdf").c_str());
    } else if (m_lepton_type == Common::LEPTONTYPE::ELMU) {
        c.Print(("../Comparison/SingleLep/"+out_name+"/"+ireg+"/"+name+"_elmu.png").c_str());
        c.Print(("../Comparison/SingleLep/"+out_name+"/"+ireg+"/"+name+"_elmu.pdf").c_str());
    } else if (m_lepton_type == Common::LEPTONTYPE::EE) {
        c.Print(("../Comparison/Dilep/"+out_name+"/"+ireg+"/"+name+"_ee.png").c_str());
        c.Print(("../Comparison/Dilep/"+out_name+"/"+ireg+"/"+name+"_ee.pdf").c_str());
    } else if (m_lepton_type == Common::LEPTONTYPE::MUMU) {
        c.Print(("../Comparison/Dilep/"+out_name+"/"+ireg+"/"+name+"_mumu.png").c_str());
        c.Print(("../Comparison/Dilep/"+out_name+"/"+ireg+"/"+name+"_mumu.pdf").c_str());
    } else if (m_lepton_type == Common::LEPTONTYPE::EMU) {
        c.Print(("../Comparison/Dilep/"+out_name+"/"+ireg+"/"+name+"_emu.png").c_str());
        c.Print(("../Comparison/Dilep/"+out_name+"/"+ireg+"/"+name+"_emu.pdf").c_str());
    }
}

void Comparator::AddAllLabels() {
    AddSingleLabel("jet1_pt", "leading jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet1_eta", "leading jet #eta [-]", "");
    AddSingleLabel("jet1_phi", "leading jet #phi [-]", "");
    AddSingleLabel("jet1_e", "leading jet energy [GeV]", "");
    AddSingleLabel("jet2_pt", "second jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet2_eta", "second jet #eta [-]", "");
    AddSingleLabel("jet2_phi", "second jet #phi [-]", "");
    AddSingleLabel("jet2_e", "second jet energy [GeV]", "");
    AddSingleLabel("jet3_pt", "third jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet3_eta", "third jet #eta [-]", "");
    AddSingleLabel("jet3_phi", "third jet #phi [-]", "");
    AddSingleLabel("jet3_e", "third jet energy [GeV]", "");
    AddSingleLabel("jet4_pt", "fourth jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet4_eta", "fourth jet #eta [-]", "");
    AddSingleLabel("jet4_phi", "fourth jet #phi [-]", "");
    AddSingleLabel("jet4_e", "fourth jet energy [GeV]", "");
    AddSingleLabel("jet5_pt", "fifth jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet5_eta", "fifth jet #eta [-]", "");
    AddSingleLabel("jet5_phi", "fifth jet #phi [-]", "");
    AddSingleLabel("jet5_e", "fifth jet energy [GeV]", "");
    AddSingleLabel("jet6_pt", "sixth jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet6_eta", "sixth jet #eta [-]", "");
    AddSingleLabel("jet6_phi", "sixth jet #phi [-]", "");
    AddSingleLabel("jet6_e", "sixth jet energy [GeV]", "");
    AddSingleLabel("bjet_n_85", "b-jet multiplicity @85 WP[-]", "");
    AddSingleLabel("bjet_n_77", "b-jet multiplicity @77 WP[-]", "");
    AddSingleLabel("bjet_n_70", "b-jet multiplicity @70 WP[-]", "");
    AddSingleLabel("bjet_n_60", "b-jet multiplicity @60 WP[-]", "");
    AddSingleLabel("bjet1_pt", "leading b-jet p_{T} [GeV]", "GeV");
    AddSingleLabel("bjet_pt_85", "b-jet p_{T} @85 WP [GeV]", "GeV");
    AddSingleLabel("bjet_eta_85", "b-jet #eta @85 WP [-]", "");
    AddSingleLabel("bjet_phi_85", "b-jet #phi @85 WP [-]", "");
    AddSingleLabel("bjet_e_85", "b-jet energy @85 WP [GeV]", "");
    AddSingleLabel("bjet_pt_77", "b-jet p_{T} @77 WP [GeV]", "GeV");
    AddSingleLabel("bjet_eta_77", "b-jet #eta @77 WP [-]", "");
    AddSingleLabel("bjet_phi_77", "b-jet #phi @77 WP [-]", "");
    AddSingleLabel("bjet_e_77", "b-jet energy @77 WP [GeV]", "");
    AddSingleLabel("bjet_pt_70", "b-jet p_{T} @70 WP [GeV]", "GeV");
    AddSingleLabel("bjet_eta_70", "b-jet #eta @70 WP [-]", "");
    AddSingleLabel("bjet_phi_70", "b-jet #phi @70 WP [-]", "");
    AddSingleLabel("bjet_e_70", "b-jet energy @70 WP [GeV]", "");
    AddSingleLabel("bjet_pt_60", "b-jet p_{T} @60 WP [GeV]", "GeV");
    AddSingleLabel("bjet_eta_60", "b-jet #eta @60 WP [-]", "");
    AddSingleLabel("bjet_phi_60", "b-jet #phi @60 WP [-]", "");
    AddSingleLabel("bjet_e_60", "b-jet energy @60 WP [GeV]", "");
    AddSingleLabel("bjet1_pt", "leading b-jet p_{T} [GeV]", "");
    AddSingleLabel("bjet1_eta", "leading b-jet #eta [-]", "");
    AddSingleLabel("bjet1_phi", "leading b-jet #phi [-]", "");
    AddSingleLabel("bjet2_pt", "second b-jet p_{T} [GeV]", "GeV");
    AddSingleLabel("bjet2_eta", "second b-jet #eta [-]", "");
    AddSingleLabel("bjet2_phi", "second b-jet #phi [-]", "");
    AddSingleLabel("jet_tagbin", "Jet tag bin [-]", "");
    AddSingleLabel("bjet_pt_light", "Non-tagged jet p_{T} [GeV]", "GeV");
    AddSingleLabel("bjet_eta_light", "Non-tagged jet #eta [-]", "");
    AddSingleLabel("bjet_phi_light", "Non-tagged jet #phi [-]", "");
    AddSingleLabel("bjet_e_light", "Non-tagged jet energy [GeV]", "");
    
    AddSingleLabel("HT", "H_{T} [GeV]", "GeV");
    AddSingleLabel("jet_n", "jet multiplicity [-]", "");
    AddSingleLabel("bjet_n", "b-jet multiplicity [-]", "");
    
    AddSingleLabel("lepton_pt", "Lepton p_{T} [GeV]", "GeV");
    AddSingleLabel("lepton_eta", "Lepton #eta [-]", "");
    AddSingleLabel("lepton_phi", "Lepton #phi [-]", "");
    AddSingleLabel("lepton_e", "Lepton energy [GeV]", "");
    AddSingleLabel("electron_pt", "Electron p_{T} [GeV]", "GeV");
    AddSingleLabel("electron_eta", "Electron #eta [-]", "");
    AddSingleLabel("electron_phi", "Electron #phi [-]", "");
    AddSingleLabel("electron_e", "Electron energy [GeV]", "");
    AddSingleLabel("muon_pt", "Muon p_{T} [GeV]", "GeV");
    AddSingleLabel("muon_eta", "Muon #eta [-]", "");
    AddSingleLabel("muon_phi", "Muon #phi [-]", "");
    AddSingleLabel("muon_e", "Muon energy [GeV]", "");
    AddSingleLabel("lepton1_pt", "Leading lepton p_{T} [GeV]", "GeV");
    AddSingleLabel("lepton1_eta", "Leading lepton #eta [-]", "");
    AddSingleLabel("lepton1_phi", "Leading lepton #phi [-]", "");
    AddSingleLabel("lepton1_e", "Leading lepton energy [GeV]", "");
    AddSingleLabel("lepton2_pt", "Second lepton p_{T} [GeV]", "GeV");
    AddSingleLabel("lepton2_eta", "Second lepton #eta [-]", "");
    AddSingleLabel("lepton2_phi", "Second lepton #phi [-]", "");
    AddSingleLabel("lepton2_e", "Second lepton energy [GeV]", "");
    AddSingleLabel("electron_pt", "Electron p_{T} [GeV]", "GeV");
    AddSingleLabel("electron_eta", "Electron #eta [-]", "");
    AddSingleLabel("electron_phi", "Electron #phi [-]", "");
    AddSingleLabel("electron_e", "Electron energy [GeV]", "");
    AddSingleLabel("muon_pt", "Muon p_{T} [GeV]", "GeV");
    AddSingleLabel("muon_eta", "Muon #eta [-]", "");
    AddSingleLabel("muon_phi", "Muon #phi [-]", "");
    AddSingleLabel("muon_e", "Muon energy [GeV]", "");
    AddSingleLabel("dilepton_m", "m_{l,l} [GeV]", "GeV");
    AddSingleLabel("dilepton_m_peak", "m_{l,l} [GeV]", "GeV");
    AddSingleLabel("dilepton_deltaR", "#Delta R(l,l) [-]", "");
    AddSingleLabel("dilepton_deltaPhi", "#Delta #phi(l,l) [-]", "");
    AddSingleLabel("reco_mlb", "m_{l,b} [GeV]", "GeV");
    AddSingleLabel("reco_mlb150", "m_{l,b} [GeV]", "GeV");
    AddSingleLabel("reco_mlb40_150", "m_{l,b} [GeV]", "GeV");
    AddSingleLabel("reco_mbb", "m_{b,#bar{b}} [GeV]", "GeV");
    AddSingleLabel("reco_mbb_cut", "m_{b,#bar{b}} [GeV]", "GeV");
    AddSingleLabel("dr_lb", "#Delta R(l,b) [-]", "");
    AddSingleLabel("dr_lb1", "#Delta R(l,b) [-]", "");
    AddSingleLabel("met", "E_{T}^{miss} [GeV]", "GeV");
    AddSingleLabel("met_x", "E_{T,x}^{miss} [GeV]", "GeV");
    AddSingleLabel("met_y", "E_{T,y}^{miss} [GeV]", "GeV");
    AddSingleLabel("met_phi", "E_{T}^{miss} #phi [-]", "");
    AddSingleLabel("mu", "average #mu [-]", "");
    AddSingleLabel("mu_actual", "actual #mu [-]", "");
    
    AddSingleLabel("mwt", "m_{T}^{W} [GeV]", "GeV");
    AddSingleLabel("reco_bhad_pt", "had. b p_{T} [GeV]", "GeV");
    AddSingleLabel("reco_bhad_eta", "had. b #eta [-]", "");
    AddSingleLabel("reco_bhad_phi", "had. b #phi [-]", "");
    AddSingleLabel("reco_blep_pt", "lep. b p_{T} [GeV]", "GeV");
    AddSingleLabel("reco_blep_eta", "lep. b #eta [-]", "");
    AddSingleLabel("reco_blep_phi", "lep. b #phi [-]", "");
    AddSingleLabel("reco_ljet1_pt", "light jet 1 p_{T} [GeV]", "GeV");
    AddSingleLabel("reco_ljet1_eta", "light jet 1 #eta [-]", "");
    AddSingleLabel("reco_ljet1_phi", "light jet 1 #phi [-]", "");
    AddSingleLabel("reco_ljet2_pt", "light jet 2 p_{T} [GeV]", "GeV");
    AddSingleLabel("reco_ljet2_eta", "light jet 2 #eta [-]", "");
    AddSingleLabel("reco_ljet2_phi", "light jet 2 #phi [-]", "");
    
    AddSingleLabel("reco_Whad_pt", "had. W p_{T} [GeV]", "GeV");
    AddSingleLabel("reco_Whad_eta", "had. W #eta [-]", "");
    AddSingleLabel("reco_Whad_phi", "had. W #phi [-]", "");
    AddSingleLabel("reco_Whad_m", "had. W mass [GeV]", "GeV");
    AddSingleLabel("reco_thad_pt", "had. top p_{T} [GeV]", "GeV");
    AddSingleLabel("reco_thad_eta", "had. top #eta [-]", "");
    AddSingleLabel("reco_thad_phi", "had. top #phi [-]", "");
    AddSingleLabel("reco_thad_m", "had. top mass [GeV]", "GeV");
    AddSingleLabel("reco_R32", "R_{3/2} [GeV]", "GeV");
    AddSingleLabel("reco_D32", "D_{3/2} [GeV]", "GeV");
    AddSingleLabel("BDT", "BDT output score [-]", "-");
    AddSingleLabel("reco_Whad_m_75_90", "had. W mass [GeV]", "GeV");
    AddSingleLabel("reco_Whad_m_25bins", "had. W mass [GeV]", "GeV");
    AddSingleLabel("reco_Whad_m_30bins", "had. W mass [GeV]", "GeV");
    
    AddSingleLabel("truth_top_pt", "truth top p_{T} [GeV]", "GeV");
    AddSingleLabel("truth_antitop_pt", "truth anti-top p_{T} [GeV]", "GeV");
    AddSingleLabel("truth_ttbar_pt", "truth ttbar p_{T} [GeV]", "GeV");
    AddSingleLabel("truth_ttbar_m", "truth ttbar mass [GeV]", "GeV");
    
    AddSingleLabel("top_pt_beforeFSR", "top pT before FSR [GeV]", "GeV");
    AddSingleLabel("top_eta_beforeFSR", "top #eta before FSR [-]", "");
    AddSingleLabel("top_phi_beforeFSR", "top #phi before FSR [-]", "");
    AddSingleLabel("top_mass_beforeFSR", "top mass before FSR [GeV]", "GeV");
    AddSingleLabel("antitop_pt_beforeFSR", "antitop pT before FSR [GeV]", "GeV");
    AddSingleLabel("antitop_eta_beforeFSR", "antitop #eta before FSR [-]", "");
    AddSingleLabel("antitop_phi_beforeFSR", "antitop #phi before FSR [-]", "");
    AddSingleLabel("antitop_mass_beforeFSR", "antitop mass before FSR [GeV]", "GeV");
    AddSingleLabel("top_pt_afterFSR", "top pT after FSR [GeV]", "GeV");
    AddSingleLabel("top_eta_afterFSR", "top #eta after FSR [-]", "");
    AddSingleLabel("top_phi_afterFSR", "top #phi after FSR [-]", "");
    AddSingleLabel("top_mass_afterFSR", "top mass after FSR [GeV]", "GeV");
    AddSingleLabel("antitop_pt_afterFSR", "antitop pT after FSR [GeV]", "GeV");
    AddSingleLabel("antitop_eta_afterFSR", "antitop #eta after FSR [-]", "");
    AddSingleLabel("antitop_phi_afterFSR", "antitop #phi after FSR [-]", "");
    AddSingleLabel("antitop_mass_afterFSR", "antitop mass after FSR [GeV]", "GeV");
    AddSingleLabel("ttbar_pt_beforeFSR", "ttbar pT before FSR [GeV]", "GeV");
    AddSingleLabel("ttbar_eta_beforeFSR", "ttbar #eta before FSR [-]", "");
    AddSingleLabel("ttbar_phi_beforeFSR", "ttbar #phi before FSR [-]", "");
    AddSingleLabel("ttbar_mass_beforeFSR", "ttbar mass before FSR [GeV]", "GeV");
    AddSingleLabel("ttbar_pt_afterFSR", "ttbar pT after FSR [GeV]", "GeV");
    AddSingleLabel("ttbar_eta_afterFSR", "ttbar #eta after FSR [-]", "");
    AddSingleLabel("ttbar_phi_afterFSR", "ttbar #phi after FSR [-]", "");
    AddSingleLabel("ttbar_mass_afterFSR", "ttbar mass after FSR [GeV]", "GeV");
    AddSingleLabel("ttbar_mas_afterFSR", "ttbar mass after FSR [GeV]", "GeV");
    AddSingleLabel("b_pt", "b pT [GeV]", "GeV");
    AddSingleLabel("b_eta", "b #eta [-]", "");
    AddSingleLabel("b_phi", "b #phi [-]", "");
    AddSingleLabel("antib_pt", "anti b pT [GeV]", "GeV");
    AddSingleLabel("antib_eta", "anti b #eta [-]", "");
    AddSingleLabel("antib_phi", "anti b #phi [-]", "");
    AddSingleLabel("lquark1_pt", "light quark 1 pT [GeV]", "GeV");
    AddSingleLabel("lquark1_eta", "light quark 1 #eta [-]", "");
    AddSingleLabel("lquark1_phi", "light quark 1 #phi [-]", "");
    AddSingleLabel("lquark2_pt", "light quark 2 pT [GeV]", "GeV");
    AddSingleLabel("lquark2_eta", "light quark 2 #eta [-]", "");
    AddSingleLabel("lquark2_phi", "light quark 2 #phi [-]", "");
    AddSingleLabel("neutrino_pt", "neutrino pT [GeV]", "GeV");
    AddSingleLabel("neutrino_eta", "neutrino #eta [-]", "");
    AddSingleLabel("neutrino_phi", "neutrino #phi [-]", "");
}

void Comparator::AddSingleLabel(const std::string& name,
                                const std::string& x,
                                const std::string& units) {
    Labels label;
    label.x_axis = x;
    label.units = units;

    m_labels_map[name] = label;
}

Labels Comparator::GetLabel(const std::string& name) const {
    
    auto itr = m_labels_map.find(name);
    if (itr == m_labels_map.end()) {
        std::cout << "Comparator::PlotSingleHisto: Cannot find labels map for: " << name << ", skipping\n";
        return Labels{};
    }

    return itr->second;
}
