#include "TTbarXsec/DileptonMatcher.h"

#include "TLorentzVector.h"

DileptonMatcher::DileptonMatcher(const MATCHINGOPTION& opt) :
    m_option(opt)
{
}

std::pair<std::size_t, std::size_t> DileptonMatcher::FindMatching(const Event& event,
                                                                  const Common::LEPTONTYPE& type) const {

    std::pair<TLorentzVector, TLorentzVector> leptons = Common::GetLeptonsChargeOrdered(event, type);

    /// get list of bjets
    std::vector<std::size_t> b_indices;
    for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
        if (event.jet_isbtagged_DL1dv01_77->at(ijet)) b_indices.emplace_back(ijet);
    }

    std::size_t best_index_1(99);
    std::size_t best_index_2(99);
    double best_1(9999);
    double best_2(9999);
    
    if (m_option == DileptonMatcher::MATCHINGOPTION::DELTAR) {
        for (const auto& ib : b_indices) {
            TLorentzVector bjet;
            bjet.SetPtEtaPhiE(event.jet_pt->at(ib)/1e3,
                            event.jet_eta->at(ib),
                            event.jet_phi->at(ib),
                            event.jet_e->at(ib)/1e3);

            const double dr1 = leptons.first.DeltaR(bjet);
            if (dr1 < best_1) {
                best_1 = dr1;
                best_index_1 = ib;
            }
        }
        for (const auto& ib : b_indices) {
            if (ib == best_index_1) continue;

            TLorentzVector bjet;
            bjet.SetPtEtaPhiE(event.jet_pt->at(ib)/1e3,
                            event.jet_eta->at(ib),
                            event.jet_phi->at(ib),
                            event.jet_e->at(ib)/1e3);

            const double dr2 = leptons.second.DeltaR(bjet);
            
            if (dr2 < best_2) {
                best_2 = dr2;
                best_index_2 = ib;
            }
        }
    } else if (m_option == DileptonMatcher::MATCHINGOPTION::MLB) {
        for (const auto& ib : b_indices) {
            TLorentzVector bjet;
            bjet.SetPtEtaPhiE(event.jet_pt->at(ib)/1e3,
                            event.jet_eta->at(ib),
                            event.jet_phi->at(ib),
                            event.jet_e->at(ib)/1e3);
            
            const double mlb_1 = (leptons.first + bjet).M();
            
            if (mlb_1 < best_1) {
                best_1 = mlb_1;
                best_index_1 = ib;
            }
        }
        for (const auto& ib : b_indices) {
            if (ib == best_index_1) continue;
            
            TLorentzVector bjet;
            bjet.SetPtEtaPhiE(event.jet_pt->at(ib)/1e3,
                            event.jet_eta->at(ib),
                            event.jet_phi->at(ib),
                            event.jet_e->at(ib)/1e3);
            
            const double mlb_2 = (leptons.second + bjet).M();
            
            if (mlb_2 < best_2) {
                best_2 = mlb_2;
                best_index_2 = ib;
            }
        }
    }

    std::pair<std::size_t, std::size_t> result = std::make_pair(best_index_1, best_index_2);
    return result;
}
