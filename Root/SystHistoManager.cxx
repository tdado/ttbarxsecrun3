#include "TTbarXsec/SystHistoManager.h"

#include <iostream>

SystHistoManager::SystHistoManager() {
}

void SystHistoManager::AddOneSidedSyst(const std::string& sys_file,
                                       const std::string& sys_name,
                                       const std::string& reference_file,
                                       const std::string& reference_histo,
                                       const std::string& target,
                                       const std::string& name,
                                       const std::string& category,
                                       const float scale) {

    Systematic systematic;
    systematic.type = SYSTEMATICTYPE::ONESIDED;
    systematic.up_file = sys_file;
    systematic.down_file = sys_file;
    systematic.up_histo = sys_name;
    systematic.down_histo = sys_name;
    systematic.reference_file = reference_file;
    systematic.reference_histo = reference_histo;
    systematic.target = target;
    systematic.norm_up = -1;
    systematic.norm_down = -1;
    systematic.name = name;
    systematic.category = category;
    systematic.subtractUpName = "";
    systematic.subtractDownName = "";
    systematic.scale = scale;

    m_systematics.emplace_back(systematic);
}
    
void SystHistoManager::AddTwoSidedSyst(const std::string& sys_file_up,
                                       const std::string& sys_file_down,
                                       const std::string& up,
                                       const std::string& down,
                                       const std::string& reference_file,
                                       const std::string& reference_histo,
                                       const std::string& target,
                                       const std::string& name,
                                       const std::string& category,
                                       const std::string& subUp,
                                       const std::string& subDown,
                                       const float scale) {

    Systematic systematic;
    systematic.type = SYSTEMATICTYPE::TWOSIDED;
    systematic.up_histo = up;
    systematic.down_histo = down;
    systematic.up_file = sys_file_up;
    systematic.down_file = sys_file_down;
    systematic.reference_file = reference_file;
    systematic.reference_histo = reference_histo;
    systematic.target = target;
    systematic.norm_up = -1;
    systematic.norm_down = -1;
    systematic.name = name;
    systematic.category = category;
    systematic.subtractUpName = subUp;
    systematic.subtractDownName = subDown;
    systematic.scale = scale;

    m_systematics.emplace_back(systematic);
}

void SystHistoManager::AddNormSyst(const std::string& target,
                                   const float& up,
                                   const float& down,
                                   const std::string& name) {

    if (up < 0 || down < 0) {
        std::cerr << "SystHistoManager::AddNormSyst: Up and down uncertainty has to be positive!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (down >= 1) {
        std::cerr << "SystHistoManager::AddNormSyst: The down norm uncertainty cannot be >= 1.0" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (up >= 1.0) {
        std::cout << "SystHistoManager::AddNormSyst: The up norm uncertainty is larger than 100%\n";
    }

    Systematic systematic;
    systematic.type = SYSTEMATICTYPE::NORMALISATION;
    systematic.up_histo = "nominal";
    systematic.down_histo = "nominal";
    systematic.up_file = target;
    systematic.down_file = target;
    systematic.reference_file = target;
    systematic.reference_histo = "nominal";
    systematic.target = target;
    systematic.norm_up = up;
    systematic.norm_down = down;
    systematic.name = name;
    systematic.category = "Normalisation";
    systematic.subtractUpName = "";
    systematic.subtractDownName = "";

    m_systematics.emplace_back(systematic);
}
    
void SystHistoManager::AddMCstatSyst(const std::string& target) {
    Systematic systematic;
    systematic.type = SYSTEMATICTYPE::MCSTAT;
    systematic.up_histo = "nominal";
    systematic.down_histo = "nominal";
    systematic.up_file = target;
    systematic.down_file = target;
    systematic.reference_file = target;
    systematic.reference_histo = "nominal";
    systematic.target = target;
    systematic.norm_up = -1;
    systematic.norm_down = -1;
    systematic.name = "MC_stat";
    systematic.category = "MC stat";
    systematic.subtractUpName = "";
    systematic.subtractDownName = "";

    m_systematics.emplace_back(systematic);
}
