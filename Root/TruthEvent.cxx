#include "TTbarXsec/TruthEvent.h"

TruthEvent::TruthEvent(TTree *tree) : fChain(0) 
{
   Init(tree);
}

TruthEvent::~TruthEvent()
{
}

Int_t TruthEvent::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TruthEvent::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

void TruthEvent::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_beamspot", &weight_beamspot, &b_weight_beamspot);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("MC_Wdecay2_from_t_pdgId", &MC_Wdecay2_from_t_pdgId, &b_MC_Wdecay2_from_t_pdgId);
   fChain->SetBranchAddress("MC_Wdecay2_from_tbar_pdgId", &MC_Wdecay2_from_tbar_pdgId, &b_MC_Wdecay2_from_tbar_pdgId);
   fChain->SetBranchAddress("MC_Wdecay1_from_tbar_pdgId", &MC_Wdecay1_from_tbar_pdgId, &b_MC_Wdecay1_from_tbar_pdgId);
   fChain->SetBranchAddress("MC_Wdecay1_from_t_pdgId", &MC_Wdecay1_from_t_pdgId, &b_MC_Wdecay1_from_t_pdgId);
   fChain->SetBranchAddress("MC_Wdecay2_from_tbar_phi", &MC_Wdecay2_from_tbar_phi, &b_MC_Wdecay2_from_tbar_phi);
   fChain->SetBranchAddress("MC_Wdecay2_from_tbar_pt", &MC_Wdecay2_from_tbar_pt, &b_MC_Wdecay2_from_tbar_pt);
   fChain->SetBranchAddress("MC_Wdecay2_from_t_phi", &MC_Wdecay2_from_t_phi, &b_MC_Wdecay2_from_t_phi);
   fChain->SetBranchAddress("MC_Wdecay2_from_t_pt", &MC_Wdecay2_from_t_pt, &b_MC_Wdecay2_from_t_pt);
   fChain->SetBranchAddress("MC_Wdecay2_from_t_m", &MC_Wdecay2_from_t_m, &b_MC_Wdecay2_from_t_m);
   fChain->SetBranchAddress("MC_Wdecay1_from_tbar_phi", &MC_Wdecay1_from_tbar_phi, &b_MC_Wdecay1_from_tbar_phi);
   fChain->SetBranchAddress("MC_tbar_beforeFSR_pt", &MC_tbar_beforeFSR_pt, &b_MC_tbar_beforeFSR_pt);
   fChain->SetBranchAddress("MC_tbar_beforeFSR_m", &MC_tbar_beforeFSR_m, &b_MC_tbar_beforeFSR_m);
   fChain->SetBranchAddress("MC_tbar_beforeFSR_eta", &MC_tbar_beforeFSR_eta, &b_MC_tbar_beforeFSR_eta);
   fChain->SetBranchAddress("MC_W_from_tbar_eta", &MC_W_from_tbar_eta, &b_MC_W_from_tbar_eta);
   fChain->SetBranchAddress("MC_Wdecay1_from_t_eta", &MC_Wdecay1_from_t_eta, &b_MC_Wdecay1_from_t_eta);
   fChain->SetBranchAddress("MC_ttbar_beforeFSR_pt", &MC_ttbar_beforeFSR_pt, &b_MC_ttbar_beforeFSR_pt);
   fChain->SetBranchAddress("MC_t_afterFSR_SC_pt", &MC_t_afterFSR_SC_pt, &b_MC_t_afterFSR_SC_pt);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_beforeDecay_m", &MC_ttbar_afterFSR_beforeDecay_m, &b_MC_ttbar_afterFSR_beforeDecay_m);
   fChain->SetBranchAddress("MC_b_from_tbar_m", &MC_b_from_tbar_m, &b_MC_b_from_tbar_m);
   fChain->SetBranchAddress("MC_t_beforeFSR_phi", &MC_t_beforeFSR_phi, &b_MC_t_beforeFSR_phi);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_phi", &MC_ttbar_afterFSR_phi, &b_MC_ttbar_afterFSR_phi);
   fChain->SetBranchAddress("MC_t_beforeFSR_eta", &MC_t_beforeFSR_eta, &b_MC_t_beforeFSR_eta);
   fChain->SetBranchAddress("MC_b_from_tbar_pt", &MC_b_from_tbar_pt, &b_MC_b_from_tbar_pt);
   fChain->SetBranchAddress("MC_t_afterFSR_SC_phi", &MC_t_afterFSR_SC_phi, &b_MC_t_afterFSR_SC_phi);
   fChain->SetBranchAddress("MC_tbar_beforeFSR_phi", &MC_tbar_beforeFSR_phi, &b_MC_tbar_beforeFSR_phi);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_eta", &MC_ttbar_afterFSR_eta, &b_MC_ttbar_afterFSR_eta);
   fChain->SetBranchAddress("MC_t_beforeFSR_pt", &MC_t_beforeFSR_pt, &b_MC_t_beforeFSR_pt);
   fChain->SetBranchAddress("MC_ttbar_beforeFSR_eta", &MC_ttbar_beforeFSR_eta, &b_MC_ttbar_beforeFSR_eta);
   fChain->SetBranchAddress("MC_Wdecay2_from_t_eta", &MC_Wdecay2_from_t_eta, &b_MC_Wdecay2_from_t_eta);
   fChain->SetBranchAddress("MC_t_afterFSR_SC_eta", &MC_t_afterFSR_SC_eta, &b_MC_t_afterFSR_SC_eta);
   fChain->SetBranchAddress("MC_b_from_t_m", &MC_b_from_t_m, &b_MC_b_from_t_m);
   fChain->SetBranchAddress("MC_ttbar_beforeFSR_m", &MC_ttbar_beforeFSR_m, &b_MC_ttbar_beforeFSR_m);
   fChain->SetBranchAddress("MC_tbar_afterFSR_SC_pt", &MC_tbar_afterFSR_SC_pt, &b_MC_tbar_afterFSR_SC_pt);
   fChain->SetBranchAddress("MC_b_from_t_eta", &MC_b_from_t_eta, &b_MC_b_from_t_eta);
   fChain->SetBranchAddress("MC_tbar_afterFSR_m", &MC_tbar_afterFSR_m, &b_MC_tbar_afterFSR_m);
   fChain->SetBranchAddress("MC_t_afterFSR_pt", &MC_t_afterFSR_pt, &b_MC_t_afterFSR_pt);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_beforeDecay_eta", &MC_ttbar_afterFSR_beforeDecay_eta, &b_MC_ttbar_afterFSR_beforeDecay_eta);
   fChain->SetBranchAddress("MC_Wdecay2_from_tbar_m", &MC_Wdecay2_from_tbar_m, &b_MC_Wdecay2_from_tbar_m);
   fChain->SetBranchAddress("MC_ttbar_beforeFSR_phi", &MC_ttbar_beforeFSR_phi, &b_MC_ttbar_beforeFSR_phi);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_beforeDecay_phi", &MC_ttbar_afterFSR_beforeDecay_phi, &b_MC_ttbar_afterFSR_beforeDecay_phi);
   fChain->SetBranchAddress("MC_t_afterFSR_m", &MC_t_afterFSR_m, &b_MC_t_afterFSR_m);
   fChain->SetBranchAddress("MC_t_afterFSR_phi", &MC_t_afterFSR_phi, &b_MC_t_afterFSR_phi);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_beforeDecay_pt", &MC_ttbar_afterFSR_beforeDecay_pt, &b_MC_ttbar_afterFSR_beforeDecay_pt);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_m", &MC_ttbar_afterFSR_m, &b_MC_ttbar_afterFSR_m);
   fChain->SetBranchAddress("MC_tbar_afterFSR_pt", &MC_tbar_afterFSR_pt, &b_MC_tbar_afterFSR_pt);
   fChain->SetBranchAddress("MC_W_from_t_eta", &MC_W_from_t_eta, &b_MC_W_from_t_eta);
   fChain->SetBranchAddress("MC_Wdecay1_from_tbar_m", &MC_Wdecay1_from_tbar_m, &b_MC_Wdecay1_from_tbar_m);
   fChain->SetBranchAddress("MC_Wdecay2_from_tbar_eta", &MC_Wdecay2_from_tbar_eta, &b_MC_Wdecay2_from_tbar_eta);
   fChain->SetBranchAddress("MC_t_afterFSR_eta", &MC_t_afterFSR_eta, &b_MC_t_afterFSR_eta);
   fChain->SetBranchAddress("MC_tbar_afterFSR_eta", &MC_tbar_afterFSR_eta, &b_MC_tbar_afterFSR_eta);
   fChain->SetBranchAddress("MC_tbar_afterFSR_phi", &MC_tbar_afterFSR_phi, &b_MC_tbar_afterFSR_phi);
   fChain->SetBranchAddress("MC_tbar_afterFSR_SC_eta", &MC_tbar_afterFSR_SC_eta, &b_MC_tbar_afterFSR_SC_eta);
   fChain->SetBranchAddress("MC_tbar_afterFSR_SC_m", &MC_tbar_afterFSR_SC_m, &b_MC_tbar_afterFSR_SC_m);
   fChain->SetBranchAddress("MC_tbar_afterFSR_SC_phi", &MC_tbar_afterFSR_SC_phi, &b_MC_tbar_afterFSR_SC_phi);
   fChain->SetBranchAddress("MC_W_from_t_m", &MC_W_from_t_m, &b_MC_W_from_t_m);
   fChain->SetBranchAddress("MC_W_from_t_pt", &MC_W_from_t_pt, &b_MC_W_from_t_pt);
   fChain->SetBranchAddress("MC_W_from_tbar_pt", &MC_W_from_tbar_pt, &b_MC_W_from_tbar_pt);
   fChain->SetBranchAddress("MC_W_from_t_phi", &MC_W_from_t_phi, &b_MC_W_from_t_phi);
   fChain->SetBranchAddress("MC_W_from_tbar_m", &MC_W_from_tbar_m, &b_MC_W_from_tbar_m);
   fChain->SetBranchAddress("MC_Wdecay1_from_t_phi", &MC_Wdecay1_from_t_phi, &b_MC_Wdecay1_from_t_phi);
   fChain->SetBranchAddress("MC_W_from_tbar_phi", &MC_W_from_tbar_phi, &b_MC_W_from_tbar_phi);
   fChain->SetBranchAddress("MC_b_from_tbar_eta", &MC_b_from_tbar_eta, &b_MC_b_from_tbar_eta);
   fChain->SetBranchAddress("MC_b_from_t_pt", &MC_b_from_t_pt, &b_MC_b_from_t_pt);
   fChain->SetBranchAddress("MC_b_from_t_phi", &MC_b_from_t_phi, &b_MC_b_from_t_phi);
   fChain->SetBranchAddress("MC_t_afterFSR_SC_m", &MC_t_afterFSR_SC_m, &b_MC_t_afterFSR_SC_m);
   fChain->SetBranchAddress("MC_b_from_tbar_phi", &MC_b_from_tbar_phi, &b_MC_b_from_tbar_phi);
   fChain->SetBranchAddress("MC_Wdecay1_from_t_m", &MC_Wdecay1_from_t_m, &b_MC_Wdecay1_from_t_m);
   fChain->SetBranchAddress("MC_t_beforeFSR_m", &MC_t_beforeFSR_m, &b_MC_t_beforeFSR_m);
   fChain->SetBranchAddress("MC_Wdecay1_from_t_pt", &MC_Wdecay1_from_t_pt, &b_MC_Wdecay1_from_t_pt);
   fChain->SetBranchAddress("MC_ttbar_afterFSR_pt", &MC_ttbar_afterFSR_pt, &b_MC_ttbar_afterFSR_pt);
   fChain->SetBranchAddress("MC_Wdecay1_from_tbar_pt", &MC_Wdecay1_from_tbar_pt, &b_MC_Wdecay1_from_tbar_pt);
   fChain->SetBranchAddress("MC_Wdecay1_from_tbar_eta", &MC_Wdecay1_from_tbar_eta, &b_MC_Wdecay1_from_tbar_eta);
}

