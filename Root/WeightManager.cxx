#include "TTbarXsec/WeightManager.h"
#include "TTbarXsec/Event.h"
#include "TTbarXsec/TruthEvent.h"
#include "TTbarNNLOReweighter/TTbarNNLOReweighter/TTbarNNLORecursiveRew.h"

#include "TH1D.h"

#include <algorithm>
#include <fstream>
#include <iostream>

WeightManager::WeightManager() :
    m_dsid(-1),
    m_af2(""),
    m_campaign(""),
    m_xSec(-1),
    m_lumi(-1),
    m_nominal_normalisation(-1),
    m_type(""),
    m_elIsoHist(nullptr),
    m_muIsoHist(nullptr)
{

    InitialiseModelNames();
    m_sf_enum = SfSyst::GetAllSFs();
    this->ReadIsoHistos();
    this->InitDileptonPtHists();
}

bool WeightManager::ReadFile(const std::string& path) {
    std::cout << "\nWeightManager::ReadFile: Reading sumWeights file from: " << path << "\n";
    std::string af2, campaign, name;
    int dsid, index;
    double sumw;

    std::fstream file(path.c_str(), std::ios_base::in);

    if (!file.is_open()) {
        std::cerr << "WeightManager::ReadFile: Cannot open file for sumWeights" << std::endl;
        return false;
    }

    while (file >> dsid >> af2 >> campaign >> index >> name >> sumw) {
        const std::string key = std::to_string(dsid) + af2 + campaign;
        auto itr = m_weight_map.find(key);

        /// does not exist in the map
        if (itr == m_weight_map.end()) {
            WeightManager::Weight weight;
            weight.names.emplace_back(name);
            weight.sumW.emplace_back(sumw);
            weight.indices.emplace_back(index);
            m_weight_map[key] = weight;
        } else { /// already exist
            itr->second.names.emplace_back(name);
            itr->second.sumW.emplace_back(sumw);
            itr->second.indices.emplace_back(index);
        }
    }

    file.close();

    std::cout << "\nWeightManager::ReadFile: Finished reading the sumWeight file\n";

    return true;
}

void WeightManager::SetDsidAf2CampaignType(const int& dsid,
                                           const std::string& af2,
                                           const std::string& campaign,
                                           const std::string& type) {
    m_dsid = dsid;
    m_af2 = af2;
    m_campaign = campaign;
    m_type = type;
}

void WeightManager::SetXsecLumi(const double xSec,
                                const double lumi) {
    m_xSec = xSec;
    m_lumi = lumi;
}
double WeightManager::GetNominalWeight(const Event& event, const bool isSherpa) const {
    const double isoCorr = isSherpa ? Common::IsoCorrection(event, m_elIsoHist, m_muIsoHist) : 1.0;
    return m_nominal_normalisation*event.weight_mc*event.weight_pileup*event.weight_leptonSF*event.weight_bTagSF_DL1dv01_77*event.weight_trigger*event.weight_beamspot*event.weight_jvt*isoCorr;
}

std::vector<double> WeightManager::GetSfSystWeights(const Event& event, const bool isSherpa, const TruthEvent* truth, const TTbarNNLORecursiveRew* rew) const {
    std::vector<double> result;

    const double nominal = GetNominalWeight(event, isSherpa);
    const double isoCorr = isSherpa ? Common::IsoCorrection(event, m_elIsoHist, m_muIsoHist) : 1.0;

    /// SF syst
    for (const auto& isf : m_sf_enum) {
        const double wei = SfSyst::EnumToWeight(isf, event, truth, rew, m_dileptonPt.get());
        if (wei < -10000) {
            std::cerr << "WeightManager::GetSfSystWeights: No function setup to calculate this variation of SFs: " << SfSyst::EnumToString(isf) << "." << std::endl;
            exit(EXIT_FAILURE);
        }

        result.emplace_back(nominal*wei);
    }

    std::vector<std::string> model_names;
    auto itr = m_model_sf_names.find(m_type);
    if (itr != m_model_sf_names.end()) {
        model_names = itr->second;
    } else {
        return result;
    }

    const std::vector<std::size_t> model_indices = WeightManager::GetModelIndices(model_names);
    for (std::size_t i = 0; i < model_indices.size(); ++i) {
        const double wei = m_model_normalisation.at(i)*event.mc_generator_weights->at(model_indices.at(i))*event.weight_pileup*event.weight_leptonSF*event.weight_bTagSF_DL1dv01_77*event.weight_trigger*event.weight_beamspot*event.weight_jvt*isoCorr;

        result.emplace_back(wei);
    }

    return result;
}

void WeightManager::ProcessNominalNormalisation() {
    if (m_dsid < 0 || m_af2 == "" || m_campaign == "" || m_type == "") {
        std::cerr << "WeightManager::ProcessNominalNormalisation: DSID, AF2 or campaign not set. Setting normalisation to -1" << std::endl;
        m_nominal_normalisation = -1;
    }

    if (m_xSec < 0 || m_lumi < 0) {
        std::cerr << "WeightManager::ProcessNominalNormalisation: Cross-section or luminosity not set. Setting normalisation to -1" << std::endl;
        m_nominal_normalisation = -1;
    }

    const double sumW = GetNominalSumWeight();
    m_nominal_normalisation = m_xSec * m_lumi / sumW;
}

void WeightManager::ProcessModellingNormalisation() {
    if (m_dsid < 0 || m_af2 == "" || m_campaign == "" || m_type == "") {
        std::cerr << "WeightManager::ProcessModellingNormalisation: DSID, AF2 or campaign not set." << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_xSec < 0 || m_lumi < 0) {
        std::cerr << "WeightManager::ProcessModellingNormalisation: Cross-section or luminosity not set." << std::endl;
        exit(EXIT_FAILURE);
    }

    /// get the list of indices for modelling sf systs
    const WeightManager::Weight weight = FindWeight();

    std::vector<std::string> model_names;
    auto itr = m_model_sf_names.find(m_type);
    if (itr != m_model_sf_names.end()) {
        model_names = itr->second;
    } else {
        return;
    }

    const std::vector<std::size_t> model_indices = GetModelIndices(model_names);

    for (const auto& i : model_indices) {
        if (i >= weight.sumW.size()) {
            std::cerr << "WeightManager::ProcessModellingNormalisation: Illegal index: " << i  << " for the modelling uncertainty for DSID: " << m_dsid << ", ignoring" << std::endl;
            continue;
        }

        const double normalisation = m_xSec * m_lumi / weight.sumW.at(i);
        m_model_normalisation.emplace_back(normalisation);
        if (model_names.size() != 0) {
            if (std::find(model_names.begin(), model_names.end(), weight.names.at(i)) == model_names.end()) {
                std::cerr << "WeightManager::ProcessModellingNormalisation: The name of the sf syst modellig variation is not properly set: " << weight.names.at(i) << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
}

std::vector<std::string> WeightManager::GetSfSystList(const std::string& systematics,
                                                      const std::string& type) const {
    std::vector<std::string> result;

    if (systematics == "sfsyst") {
        for (const auto& isf : m_sf_enum) {
            const std::string name = SfSyst::EnumToString(isf);
            if (name == "") {
                std::cerr << "WeightManager::GetSfSystList: Wrong Sfsyst enum!" << std::endl;
                exit(EXIT_FAILURE);
            }
            result.emplace_back(name);
        }
    } else {
        result.emplace_back("nominal");
    }

    if (systematics == "sfsyst") {
        /// Add Modelling
        auto itr = m_model_sf_names.find(type);
        if (itr != m_model_sf_names.end()) {
            result.insert(result.end(), itr->second.begin(), itr->second.end());
        }
    }

    return result;
}

void WeightManager::Clear() {
    m_nominal_normalisation = -1;
    m_model_normalisation.clear();
}

double WeightManager::GetNominalSumWeight() const {

    const WeightManager::Weight weight = FindWeight();
    if (weight.names.size() == 1) {
        std::cout << "\t\tWeightManager::GetNominalSumWeight: Only one sumW, returning it\n";
        return weight.sumW.at(0);
    }
    static const std::vector<std::string> key_names = {"nominal","Default","1001","Weight","muR010000E+01muF010000E+01"};
    //static const std::vector<std::string> key_names = {"Default","nominal","1001","Weight","muR010000E+01muF010000E+01"};
    std::size_t position(9999);

    auto itr = std::find(weight.names.begin(), weight.names.end(), key_names.at(0));
    if (itr != weight.names.end()) {
       position = std::distance(weight.names.begin(), itr); 
    } else {
        std::cout << "\t\tWeightManager::GetNominalSumWeight: Weight " << key_names.at(0) << " not found\n";
        for (std::size_t ialt = 1; ialt < key_names.size(); ++ialt) {
            std::cout << "\t\tWeightManager::GetNominalSumWeight: Trying weight " << key_names.at(ialt) << "\n";
            itr = std::find(weight.names.begin(), weight.names.end(), key_names.at(ialt));
            if (itr != weight.names.end()) {
                position = std::distance(weight.names.begin(), itr); 
                break;
            }
        }
    }
    
    if (itr == weight.names.end()) {
        std::cerr << "WeightManager::GetNominalSumWeight: Cannot find the nominal weight!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (position != 0) {
        std::cout << "\n\t\tWeightManager::GetNominalSumWeight: The nominal weight is not the first one but : " << position <<  "\n";
    }

    return weight.sumW.at(position);
}

WeightManager::Weight WeightManager::FindWeight() const {

    const std::string key = std::to_string(m_dsid)+m_af2+m_campaign;
    auto itr = m_weight_map.find(key);

    if (itr == m_weight_map.end()) {
        std::cerr << "WeightManager::FindWeightVector: Cannot find DSID: " << m_dsid << ", af2: " << m_af2 << ", campaign: " << m_campaign << ", in the map" << std::endl;
        exit(EXIT_FAILURE);
    }

    return itr->second;
}

void WeightManager::InitialiseModelNames() {
    
    /// ttbar
    m_model_sf_names["ttbar_PP8_FS"] = {"MUR1_MUF2_PDF260000","MUR1_MUF05_PDF260000","MUR2_MUF1_PDF260000","MUR05_MUF1_PDF260000","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["ttbar_PP8_singlelep_FS"] = {"MUR1_MUF2_PDF260000","MUR1_MUF05_PDF260000","MUR2_MUF1_PDF260000","MUR05_MUF1_PDF260000","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["ttbar_PP8_dilep_FS"] = {"MUR1_MUF2_PDF260000","MUR1_MUF05_PDF260000","MUR2_MUF1_PDF260000","MUR05_MUF1_PDF260000","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};

    /// single top
    m_model_sf_names["SingleTop_PP8_s_chan_FS"] = {"MUR1_MUF2_PDF260000","MUR1_MUF05_PDF260000","MUR2_MUF1_PDF260000","MUR05_MUF1_PDF260000","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["SingleTop_PP8_t_chan_FS"] = {"MUR1_MUF2_PDF260000","MUR1_MUF05_PDF260000","MUR2_MUF1_PDF260000","MUR05_MUF1_PDF260000","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["SingleTop_PP8_tW_chan_FS"] = {"MUR1_MUF2_PDF260000","MUR1_MUF05_PDF260000","MUR2_MUF1_PDF260000","MUR05_MUF1_PDF260000","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};
    m_model_sf_names["SingleTop_PP8_tW_chan_dilep_FS"] = {"MUR1_MUF2_PDF260000","MUR1_MUF05_PDF260000","MUR2_MUF1_PDF260000","MUR05_MUF1_PDF260000","Var3cUp","Var3cDown","isrmuRfac10_fsrmuRfac20","isrmuRfac10_fsrmuRfac05"};

    // Zjets
    m_model_sf_names["Zjets_Sherpa_FS"] = {"MUR05_MUF05_PDF303200_PSMUR05_PSMUF05", "MUR05_MUF1_PDF303200_PSMUR05_PSMUF1", "MUR1_MUF05_PDF303200_PSMUR1_PSMUF05", "MUR1_MUF2_PDF303200_PSMUR1_PSMUF2", "MUR2_MUF1_PDF303200_PSMUR2_PSMUF1", "MUR2_MUF2_PDF303200_PSMUR2_PSMUF2", "ME_ONLY_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05", "ME_ONLY_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1", "ME_ONLY_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05", "ME_ONLY_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2", "ME_ONLY_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1", "ME_ONLY_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2"};
    
    m_model_sf_names["Zjets_PP8_FS"] = {"MUR05_MUF1_PDF10800", "MUR2_MUF1_PDF10800", "MUR05_MUF05_PDF10800", "MUR1_MUF05_PDF10800", "MUR1_MUF2_PDF10800", "MUR2_MUF2_PDF10800", "Var3cUp", "Var3cDown", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "isrmuRfac20_fsrmuRfac10", "isrmuRfac05_fsrmuRfac10", "isrmuRfac20_fsrmuRfac20", "isrmuRfac05_fsrmuRfac05", "hardHi", "hardLo"};
    
    m_model_sf_names["Zjets_aMCNLO_P8_FS"] = {"MUR05_MUF05_PDF325100", "MUR20_MUF10_PDF325100", "MUR05_MUF10_PDF325100", "MUR10_MUF05_PDF325100", "MUR10_MUF20_PDF325100", "MUR20_MUF20_PDF325100", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "isrmuRfac20_fsrmuRfac10", "isrmuRfac05_fsrmuRfac10", "isrmuRfac20_fsrmuRfac20", "isrmuRfac05_fsrmuRfac05", "hardHi", "hardLo"};

    /// PDFs
    for (int i = 93300; i <= 93342; ++i) {
        m_model_sf_names["ttbar_PP8_FS"].emplace_back("MUR1_MUF1_PDF"+std::to_string(i));
        m_model_sf_names["ttbar_PP8_singlelep_FS"].emplace_back("MUR1_MUF1_PDF"+std::to_string(i));
        m_model_sf_names["ttbar_PP8_dilep_FS"].emplace_back("MUR1_MUF1_PDF"+std::to_string(i));
        m_model_sf_names["Zjets_PP8_FS"].emplace_back("MUR1_MUF1_PDF"+std::to_string(i));
        m_model_sf_names["Zjets_Sherpa_FS"].emplace_back("MUR1_MUF1_PDF"+std::to_string(i));
        m_model_sf_names["Zjets_aMCNLO_P8_FS"].emplace_back("MUR10_MUF10_PDF"+std::to_string(i));
    }

    //for (int i = 303201; i <= 303300; ++i) {
    //    m_model_sf_names["Zjets_Sherpa_FS"].emplace_back("MUR1_MUF1_PDF"+std::to_string(i));
    //}
}

std::vector<std::size_t> WeightManager::GetModelIndices(const std::vector<std::string>& names) const {
    std::vector<std::size_t> result;

    const WeightManager::Weight weight = FindWeight();
    for (const auto& isf : names) {
        auto itr = std::find(weight.names.begin(), weight.names.end(), isf);
        if (itr == weight.names.end()) {
            std::cerr << "WeightManager::GetModelIndices: Cannot find weight : " << isf << " in the sum weights file for DSID : " << m_dsid << std::endl;
            exit(EXIT_FAILURE);
        }
        result.emplace_back(std::distance(weight.names.begin(), itr));
    }

    return result;
}

void WeightManager::ReadIsoHistos() {
    std::unique_ptr<TFile> file(TFile::Open("data/SF_iso_run3.root", "READ"));
    if (!file) {
        std::cerr << "Cannot open the isolation correction ROOT file\n";
        std::exit(EXIT_FAILURE); 
    }

    m_elIsoHist.reset(file->Get<TH1D>("he_el_pt_PP8_Sherpa"));
    m_muIsoHist.reset(file->Get<TH1D>("he_mu_pt_PP8_Sherpa"));

    if (!m_elIsoHist || !m_muIsoHist) {
        std::cerr << "Cannot read the isolation histograms\n";
        std::exit(EXIT_FAILURE);
    }

    file->Close();
}

void WeightManager::InitDileptonPtHists() {
    std::unique_ptr<TFile> f(TFile::Open("data/DileptonPt.root", "READ"));
    if (!f) {
        std::cerr << "WeightManager::InitDileptonPtHistos: Cannot open file: data/DileptonPt.root\n";
        std::exit(EXIT_FAILURE);
    }

    m_dileptonPt.reset(f->Get<TH1D>("dilepton_pt"));
    if (!m_dileptonPt) {
        std::cerr << "WeightManager::InitDileptonPtHistos: Cannot read the dileptonPt histo\n";
        std::exit(EXIT_FAILURE);
    }

    f->Close();
}