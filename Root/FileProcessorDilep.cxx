#include "TTbarXsec/FileProcessorDilep.h"
#include "TTbarXsec/HistoMakerDilep.h"
#include "TTbarXsec/WeightManager.h"
#include "Xsection/SampleXsection.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TKey.h"
#include "TSystem.h"

#include <algorithm>
#include <fstream>
#include <iostream>

FileProcessorDilep::FileProcessorDilep(const std::string& name,
                                       const std::string& dataset_type,
                                       const std::string& systematics,
                                       const std::string& parallelSyst) :
    m_name(name),
    m_dataset_type(dataset_type),
    m_systematics(systematics),
    m_parallelSyst(parallelSyst) {
}

void FileProcessorDilep::ReadConfig() {

    /// set names of processes
    m_process.emplace_back("ttbar_PP8_dilep_FS");
    m_process.emplace_back("ttbar_PP8_hdamp_FS");
    m_process.emplace_back("ttbar_PH7_FS");
    m_process.emplace_back("ttbar_Sherpa_FS");
    m_process.emplace_back("SingleTop_PP8_tW_chan_dilep_FS");
    m_process.emplace_back("SingleTop_DS_PP8_tW_chan_FS");
    m_process.emplace_back("Diboson_Sherpa_FS");
    m_process.emplace_back("Zjets_Sherpa_FS");
    m_process.emplace_back("Zjets_PP8_FS");
    m_process.emplace_back("ttV_Sherpa_FS");
    m_process.emplace_back("ttbar_PP8_dilep_mass_171p5_FS");
    m_process.emplace_back("ttbar_PP8_dilep_mass_173p5_FS");
    m_process.emplace_back("singleTop_PP8_tW_chan_mass_171p5_FS");
    m_process.emplace_back("singleTop_PP8_tW_chan_mass_173p5_FS");
    m_process.emplace_back("Multijet");
    m_process.emplace_back("Data");

    if (m_dataset_type != "full") {
        if (std::find(m_process.begin(), m_process.end(), m_dataset_type) != m_process.end()) {
            m_process.clear();
            m_process.emplace_back(m_dataset_type);
        } else {
            std::cerr << "FileProcessorDilep::ReadConfig: Unknown process type: " << m_dataset_type << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void FileProcessorDilep::ProcessAllFiles(const DileptonMatcher::MATCHINGOPTION& opt) {

    /// Read File list
    ReadFileList("scripts/file_list_dilep.txt");

    /// Read list of systematics
    if (m_systematics == "nominal") {
        m_syst_list.emplace_back("nominal");
    }
    if (m_systematics == "syst") {
        m_syst_list = ReadSystList();
    }

    if (m_systematics == "sfsyst") {
        m_syst_list.emplace_back("sfsyst");
    }

    /// Create empty output files
    std::string out_path = "../OutputHistos/Dilep/";
    if (m_systematics == "nominal") {
        std::cout << "FileProcessorDilep::ProcessAllFiles: Creating empty output files\n";
        CreateOutputFiles(out_path);
    }

    /// Get the cross-section tool
    SampleXsection xSecTool{};
    if (!xSecTool.readFromFile("data/XSection-MC21-13p6TeV.data")) {
        std::cerr << "FileProcessorDilep::ProcessAllFiles: Cannot open data file with cross-sections" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Get the WeightManager tool
    WeightManager weight_manager{};

    if (!weight_manager.ReadFile("scripts/sumW_dilep.txt")) {
        std::cerr << "FileProcessorDilep::ProcessAllFiles: Cannot read the file with sum weights" << std::endl;
        exit(EXIT_FAILURE);
    }

    static const std::vector<std::string> only_nominal = {"Multijet",
                                                          "ttbar_PP8_hdamp_FS",
                                                          "ttbar_PH7_FS",
                                                          "ttbar_Sherpa_FS",
                                                          "SingleTop_DS_PP8_tW_chan_FS",
                                                          "ttbar_PP8_dilep_mass_171p5_FS",
                                                          "ttbar_PP8_dilep_mass_173p5_FS",
                                                          "singleTop_PP8_tW_chan_mass_171p5_FS",
                                                          "singleTop_PP8_tW_chan_mass_173p5_FS"
                                                         };

    static const std::vector<std::string> only_fakes   = {
                                                          "ttbar_PP8_singlelep_FS",
                                                          "Wjets_Sherpa_FS",
                                                          "SingleTop_PP8_s_chan_FS",
                                                          "SingleTop_PP8_t_chan_FS",
                                                          "SingleTop_PP8_tW_chan_FS"
                                                         };

    static const std::vector<std::string> ignoreFakes = {
                                                         "Zjets_Sherpa_FS",
                                                         "Zjets_PP8_FS"
                                                        };

    /// Main loop over systematics
    for (std::size_t isyst = 0; isyst < m_syst_list.size(); ++isyst) {
        if (!m_parallelSyst.empty() && (m_syst_list.at(isyst) != m_parallelSyst)) continue;

        std::cout << "\nFileProcessorDilep::ProcessAllFiles: Looping over systematic: " << m_syst_list.at(isyst) << ", " << isyst+1 << " out of " << m_syst_list.size() << " systs\n";
        std::vector<HistoMakerDilep> histoMaker;

        for (const auto& iprocess : m_process) {
            std::vector<std::string> sfsyst_list = weight_manager.GetSfSystList(m_systematics, iprocess);
            if (iprocess == "Data" && m_systematics == "sfsyst") {
                sfsyst_list.clear();
                for (int i = 0; i < 100; ++i) {
                    const std::string name = "replica_" + std::to_string(i);
                    sfsyst_list.emplace_back(name);
                }
            }
            histoMaker.emplace_back(iprocess, m_systematics, sfsyst_list);
        }

        /// Loop over files
        for (std::size_t ifile = 0; ifile < m_file_path.size(); ++ifile) {

            if (m_systematics != "nominal" &&
                (std::find(only_nominal.begin(), only_nominal.end(), m_file_type.at(ifile)) != only_nominal.end()) ) continue;
            
            if (m_systematics != "nominal" &&
                (std::find(only_fakes.begin(), only_fakes.end(), m_file_type.at(ifile)) != only_fakes.end()) ) continue;

            if (m_systematics == "syst" && m_file_type.at(ifile) == "Data") continue;

            /// Skip files that are not specified
            if (m_dataset_type != "full") {
                if (m_file_type.at(ifile) != m_dataset_type) continue;
            }

            const bool skipFakes = std::find(ignoreFakes.begin(), ignoreFakes.end(), m_file_type.at(ifile)) != ignoreFakes.end();

            std::cout << "\n\tFileProcessorDilep::ProcessAllFiles: Processing file: " << m_file_path.at(ifile) << ", " << ifile+1 << " out of " << m_file_path.size() << " files\n";

            const bool isMC = m_dsid.at(ifile) > 100000;


            /// calculate Xsection
            const double xSec = xSecTool.getXsection(m_dsid.at(ifile));
            if (xSec < 0 && m_dsid.at(ifile) > 100000) {
                std::cerr << "FileProcessorDilep::ProcessAllFiles: Negative cross-section found, skipping the file" << std::endl;
                exit(EXIT_FAILURE);
            }

            const double lumi = 1000.;

            if (isMC) {
                /// pass relevant info to WeightManager
                weight_manager.Clear();
                weight_manager.SetDsidAf2CampaignType(m_dsid.at(ifile), m_af2.at(ifile), m_campaign.at(ifile), m_file_type.at(ifile));
                weight_manager.SetXsecLumi(xSec, lumi);
                weight_manager.ProcessNominalNormalisation();

                if (m_systematics == "sfsyst") {
                    /// want to calculate it now as it is expensive
                    weight_manager.ProcessModellingNormalisation();
                }
            }

            if (std::find(only_fakes.begin(), only_fakes.end(), m_file_type.at(ifile)) == only_fakes.end()) {

                /// Identify which element of the vector needs to be filled
                auto itr = std::find(m_process.begin(), m_process.end(), m_file_type.at(ifile));
                if (itr == m_process.end()) {
                    std::cerr << "FileProcessorDilept::ProcessAllFiles: Unknown file type: " << m_file_type.at(ifile) << " skipping the file" << std::endl;
                    exit(EXIT_FAILURE);
                }
                const std::size_t position = std::distance(m_process.begin(), itr);

                histoMaker.at(position).SetCurrentSyst(m_syst_list.at(isyst));
                histoMaker.at(position).SetIsMC(isMC);
                histoMaker.at(position).SetDSID(m_dsid.at(ifile));

                /// Fill histograms, contains event loop
                if (m_systematics == "nominal") {
                    histoMaker.at(position).FillHistos(weight_manager, "nominal", m_file_path.at(ifile), opt, skipFakes);

                } else if (m_systematics == "syst") {
                    /// Name of the tree is the name of the systematics
                    histoMaker.at(position).FillHistos(weight_manager, m_syst_list.at(isyst), m_file_path.at(ifile), opt, skipFakes);

                } else if (m_systematics == "sfsyst") {
                    /// SF systematics use the nominal tree
                    histoMaker.at(position).FillHistos(weight_manager, "nominal", m_file_path.at(ifile), opt, skipFakes);

                } else {
                    std::cerr << "FileProcessorDilep::ProcessAllFiles: Unknown syst type: " << m_systematics << std::endl;
                    exit(EXIT_FAILURE);
                }
            }

            /// Run fakes on Nominal MC
            if (m_systematics != "nominal") continue;
            if (skipFakes) continue;
            if (std::find(only_nominal.begin(), only_nominal.end(), m_file_type.at(ifile)) != only_nominal.end()) continue;

            std::cout << "\tFileProcessorDilep::ProcessAllFiles: Running on: " << m_file_type.at(ifile) << " for fakes" << std::endl;

            std::size_t position_fakes(9999);
            auto itr_fakes = std::find(m_process.begin(), m_process.end(), "Multijet");
            if (itr_fakes == m_process.end()) {
                std::cerr << "FileProcessorDilep::ProcessAllFiles: Unknown file type: " << m_file_type.at(ifile) << " skipping the file" << std::endl;
                continue;
            }
            position_fakes = std::distance(m_process.begin(), itr_fakes);
            histoMaker.at(position_fakes).SetCurrentSyst("nominal");
            histoMaker.at(position_fakes).SetIsMC(true);
            histoMaker.at(position_fakes).FillHistos(weight_manager, "nominal", m_file_path.at(ifile), opt, skipFakes);
        }

        std::cout << "\nFileProcessorDilepton::ProcessAllFiles: Writting to histos\n";
        /// Finalise and write the histos
        for (std::size_t iprocess = 0; iprocess < m_process.size(); ++iprocess) {
            if (m_systematics != "nominal" &&
                (std::find(only_nominal.begin(), only_nominal.end(), m_process.at(iprocess)) != only_nominal.end())) continue;
            histoMaker.at(iprocess).SetCurrentSyst(m_syst_list.at(isyst));
            histoMaker.at(iprocess).Finalise();
            histoMaker.at(iprocess).WriteHistosToFile(out_path, m_name, m_parallelSyst);
        }
    }

    /// Rename the files for the nominal run
    if (m_systematics == "nominal") {
        RenameOutputFiles(out_path);
    }
}

void FileProcessorDilep::ReadFileList(const std::string& path) {
    std::cout << "\nFileProcessorDilep::ReadFileList: Reading file list from: " << path << std::endl;
    std::string file_path, file_type, af2, campaign;
    int dsid;

    /// Open file
    std::fstream file(path.c_str(), std::ios_base::in);

    if (!file.is_open()) {
        std::cerr << "FileProcessorDilep::ReadFileList: Cannot open the file list at" << std::endl;
        exit(EXIT_FAILURE);
    }

    while (file >> file_path >> file_type >> af2 >> dsid >> campaign) {
        m_file_path.emplace_back(file_path);
        m_file_type.emplace_back(file_type);
        m_dsid.emplace_back(dsid);
        m_af2.emplace_back(af2);
        m_campaign.emplace_back(campaign);
    }

    if (m_file_path.size() == 0) {
        std::cerr << "FileProcessorDilep::ReadFileList: File list is empty?" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Check consistency
    if (m_file_path.size() != m_file_type.size()) {
        std::cerr << "FileProcessorDilep::ReadFileList: Size of file_path and file_type does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_file_path.size() != m_campaign.size()) {
        std::cerr << "FileProcessorDilep::ReadFileList: Size of file_path and campaign does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_file_path.size() != m_dsid.size()) {
        std::cerr << "FileProcessorDilep::ReadFileList: Size of file_path and dsid does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "\nFileProcessorDilep::ReadFileList: File list read successfully \n\n";
}

std::vector<std::string> FileProcessorDilep::ReadSystList() const {
    std::vector<std::string> result;

    std::size_t position(0);
    bool found(false);

    for (std::size_t i = 0; i < m_file_type.size(); ++i) {

        /// Get the list of systematics from the nominal ttbar file
        if (m_file_type.at(i) == "ttbar_PP8_dilep_FS") {
            found = true;
            position = i;
            break;
        }
    }

    if (!found) {
        std::cerr << "FileProcessor::ReadSystList: Did not find a ttbar_PP8_FS file which we need to read list of systs" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// open the file
    std::unique_ptr<TFile> file (TFile::Open(m_file_path.at(position).c_str(), "READ"));
    if (!file) {
        std::cerr << "FileProcessor::ReadSystList: Cannot open file at: " << m_file_path.at(position) << std::endl;
        exit(EXIT_FAILURE);
    }

    file->cd();
    TIter next(gDirectory->GetListOfKeys());
    TKey *key;

    static const std::vector<std::string> exclude = {"nominal",
                                                     "sumWeights",
                                                     "truth",
                                                     "particleLevel",
                                                     "AnalysisTracking",
                                                     "ee_2022",
                                                     "mumu_2022",
                                                     "emu_2022",
                                                     "2022_particle",
                                                     "emu_2022_particle",
                                                     "ee_2022_particle",
                                                     "mumu_2022_particle"};

    while ((key = static_cast<TKey*>(next()))) {
        if (key->IsFolder()) {
            const std::string name = key->GetName();
            if (std::find(exclude.begin(), exclude.end(), name) == exclude.end()) {
                result.emplace_back(name);
            }
        }
    }

    file->Close();

    return result;
}

void FileProcessorDilep::CreateOutputFiles(const std::string& path) const {
    gSystem->MakeDirectory((path+"/"+m_name).c_str());
    for (const auto& iprocess : m_process) {
        const std::string s = path +"/"+ m_name +"/"+ iprocess + "_"+m_name;
        std::unique_ptr<TFile> f_ee(TFile::Open((s+"_ee.root.temp").c_str(), "RECREATE"));
        f_ee->Close();
        std::unique_ptr<TFile> f_mumu(TFile::Open((s+"_mumu.root.temp").c_str(), "RECREATE"));
        f_mumu->Close();
        std::unique_ptr<TFile> f_emu(TFile::Open((s+"_emu.root.temp").c_str(), "RECREATE"));
        f_emu->Close();
    }
}

void FileProcessorDilep::RenameOutputFiles(const std::string& path) const {
    std::cout << "\nFileProcessorDilep::RenameOutputFiles: Everything went successfully, renaming files\n";
    for (const auto& iprocess : m_process) {
        const std::string in  = path+"/"+m_name+"/"+iprocess+"_"+m_name;
        const std::string out = path+"/"+m_name+"/"+iprocess+"_"+m_name;
        gSystem->Rename((in+"_ee.root.temp").c_str(),(out+"_ee.root").c_str());
        gSystem->Rename((in+"_mumu.root.temp").c_str(),(out+"_mumu.root").c_str());
        gSystem->Rename((in+"_emu.root.temp").c_str(),(out+"_emu.root").c_str());
    }
}
