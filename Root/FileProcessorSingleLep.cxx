#include "TTbarXsec/FileProcessorSingleLep.h"
#include "TTbarXsec/HistoMakerSingleLep.h"
#include "TTbarXsec/WeightManager.h"
#include "Xsection/SampleXsection.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TKey.h"
#include "TSystem.h"

#include <algorithm>
#include <fstream>
#include <iostream>

FileProcessorSingleLep::FileProcessorSingleLep(const std::string& name,
                                               const std::string& dataset_type,
                                               const std::string& systematics) :
    m_name(name),
    m_dataset_type(dataset_type),
    m_systematics(systematics) {
}

void FileProcessorSingleLep::ReadConfig() {
   
    /// set names of processes 
    m_process.emplace_back("ttbar_PP8_FS");
    m_process.emplace_back("ttbar_PP8_hdamp_FS");
    m_process.emplace_back("ttbar_PH7_FS");
    m_process.emplace_back("Wjets_Sherpa_FS");
    m_process.emplace_back("Zjets_Sherpa_FS");
    m_process.emplace_back("SingleTop_PP8_s_chan_FS");
    m_process.emplace_back("SingleTop_PP8_t_chan_FS");
    m_process.emplace_back("SingleTop_PP8_tW_chan_FS");
    //m_process.emplace_back("Diboson_Sherpa_FS");
    m_process.emplace_back("Multijet");
    m_process.emplace_back("Data");

    if (m_dataset_type != "full") {
        if (std::find(m_process.begin(), m_process.end(), m_dataset_type) != m_process.end()) {
            m_process.clear();
            m_process.emplace_back(m_dataset_type);
            if (m_dataset_type == "Data") m_process.emplace_back("Multijet");
        } else {
            std::cerr << "FileProcessorSingleLep::ReadConfig: Unknown process type: " << m_dataset_type << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void FileProcessorSingleLep::ProcessAllFiles() {

    /// Read File list
    ReadFileList("scripts/file_list_singlelep.txt");

    /// Read list of systematics
    if (m_systematics == "nominal") {
        m_syst_list.emplace_back("nominal");
    }
    if (m_systematics == "syst") {
        m_syst_list = ReadSystList();
    }

    if (m_systematics == "sfsyst") {
        m_syst_list.emplace_back("sfsyst");
    }

    /// Create empty output files
    std::string out_path = "../OutputHistos/SingleLep/";
    if (m_systematics == "nominal") {
        std::cout << "FileProcessorSingleLep::ProcessAllFiles: Creating empty output files\n";
        CreateOutputFiles(out_path);
    }

    /// Get the cross-section tool
    SampleXsection xSecTool{};
    if (!xSecTool.readFromFile("data/XSection-MC21-13p6TeV.data")) {
        std::cerr << "FileProcessorSingleLep::ProcessAllFiles: Cannot open data file with cross-sections" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Get the WeightManager tool
    WeightManager weight_manager{};

    if (!weight_manager.ReadFile("scripts/sumW_singlelep.txt")) {
        std::cerr << "FileProcessorSingleLep::ProcessAllFiles: Cannot read the file with sum weights" << std::endl;
        exit(EXIT_FAILURE);
    }

    static const std::vector<std::string> only_nominal = {"Data",
                                                          "Multijet",
                                                          "ttbar_PP8_hdamp_FS",
                                                          "ttbar_PH7_FS"
                                                          };

    /// Main loop over systematics
    for (std::size_t isyst = 0; isyst < m_syst_list.size(); ++isyst) {
        std::cout << "\nFileProcessorSingleLep::ProcessAllFiles: Looping over systematic: " << m_syst_list.at(isyst) << ", " << isyst+1 << " out of " << m_syst_list.size() << " systs\n";
        std::vector<HistoMakerSingleLep> histoMaker;

        for (const auto& iprocess : m_process) {
            std::vector<std::string> sfsyst_list = weight_manager.GetSfSystList(m_systematics, iprocess);
            histoMaker.emplace_back(iprocess, m_systematics, sfsyst_list);
        }

        /// Loop over files
        for (std::size_t ifile = 0; ifile < m_file_path.size(); ++ifile) {

            if (m_systematics != "nominal" && 
                (std::find(only_nominal.begin(), only_nominal.end(), m_file_type.at(ifile)) != only_nominal.end()) ) continue;

            /// Skip files that are not specified
            if (m_dataset_type != "full") {
                if (m_dataset_type == "ttbar_PP8_FS") {
                    if (m_file_type.at(ifile) != "ttbar_PP8_singlelep_FS" && m_file_type.at(ifile) != "ttbar_PP8_dilep_FS") continue;
                } else {
                    if (m_file_type.at(ifile) != m_dataset_type) continue;
                }
            }

            std::cout << "\n\tFileProcessorSingleLep::ProcessAllFiles: Processing file: " << m_file_path.at(ifile) << ", " << ifile+1 << " out of " << m_file_path.size() << " files\n";

            const bool isMC = m_dsid.at(ifile) > 100000;


            /// calculate Xsection
            const double xSec = xSecTool.getXsection(m_dsid.at(ifile));
            if (xSec < 0 && m_dsid.at(ifile) > 100000) {
                std::cerr << "FileProcessorSingleLep::ProcessAllFiles: Negative cross-section found, skipping the file" << std::endl;
                exit(EXIT_FAILURE);
            }
            
            const double lumi(1000.);

            if (isMC) {
                /// pass relevant info to WeightManager
                weight_manager.Clear();
                weight_manager.SetDsidAf2CampaignType(m_dsid.at(ifile), m_af2.at(ifile), m_campaign.at(ifile), m_file_type.at(ifile));
                weight_manager.SetXsecLumi(xSec, lumi);
                weight_manager.ProcessNominalNormalisation();

                if (m_systematics == "sfsyst") {
                    /// want to calculate it now as it is expensive 
                    weight_manager.ProcessModellingNormalisation();
                }
            }
           
            std::string fileTypeHisto = m_file_type.at(ifile);
            if (m_file_type.at(ifile) == "ttbar_PP8_dilep_FS" || m_file_type.at(ifile) == "ttbar_PP8_singlelep_FS") fileTypeHisto = "ttbar_PP8_FS";
            /// Identify which element of the vector needs to be filled
            auto itr = std::find(m_process.begin(), m_process.end(), fileTypeHisto);
            if (itr == m_process.end()) {
                std::cerr << "FileProcessorSingleLep::ProcessAllFiles: Unknown file type: " << m_file_type.at(ifile) << " skipping the file" << std::endl;
                exit(EXIT_FAILURE);
            }
            const std::size_t position = std::distance(m_process.begin(), itr);
            
            histoMaker.at(position).SetCurrentSyst(m_syst_list.at(isyst));
            histoMaker.at(position).SetIsMC(isMC);
            histoMaker.at(position).SetDSID(m_dsid.at(ifile));
            
            /// Fill histograms, contains event loop
            if (m_systematics == "nominal") {
                histoMaker.at(position).FillHistos(weight_manager, "nominal", m_file_path.at(ifile));

            } else if (m_systematics == "syst") {
                /// Name of the tree is the name of the systematics
                histoMaker.at(position).FillHistos(weight_manager, m_syst_list.at(isyst), m_file_path.at(ifile));

            } else if (m_systematics == "sfsyst") {
                /// SF systematics use the nominal tree
                histoMaker.at(position).FillHistos(weight_manager, "nominal", m_file_path.at(ifile));

            } else {
                std::cerr << "FileProcessorSingleLep::ProcessAllFiles: Unknown syst type: " << m_systematics << std::endl;
                exit(EXIT_FAILURE);
            }

            if (m_file_type.at(ifile) != "Data") continue;

            /// Fill QCD fakes
            std::cout << "FileProcessorSingleLep::ProcessAllFiles: Running on: " << m_file_type.at(ifile) << " for fakes" << std::endl;
            std::size_t position_fakes(9999);
            auto itr_fakes = std::find(m_process.begin(), m_process.end(), "Multijet");
            if (itr_fakes == m_process.end()) {
                std::cerr << "FileProcessorSingleLep::ProcessAllFiles: Unknown file type: " << m_file_type.at(ifile) << " skipping the file" << std::endl;
                continue;
            }
            position_fakes = std::distance(m_process.begin(), itr_fakes);
            histoMaker.at(position_fakes).SetCurrentSyst("nominal");
            histoMaker.at(position_fakes).SetIsMC(false);
            histoMaker.at(position_fakes).FillHistos(weight_manager, "nominal_Loose", m_file_path.at(ifile));
        }

        std::cout << "\nFileProcessorSingleLep::ProcessAllFiles: Writting to histos\n";
        /// Finalise and write the histos
        for (std::size_t iprocess = 0; iprocess < m_process.size(); ++iprocess) {
            if (m_systematics != "nominal" && 
                (std::find(only_nominal.begin(), only_nominal.end(), m_process.at(iprocess)) != only_nominal.end())) continue;
            histoMaker.at(iprocess).SetCurrentSyst(m_syst_list.at(isyst));
            histoMaker.at(iprocess).Finalise();
            histoMaker.at(iprocess).WriteHistosToFile(out_path, m_name);
        }
    }

    /// Rename the files for the nominal run
    if (m_systematics == "nominal") {
        RenameOutputFiles(out_path);
    }
    
}

void FileProcessorSingleLep::ReadFileList(const std::string& path) {
    std::cout << "\nFileProcessorSingleLep::ReadFileList: Reading file list from: " << path << std::endl;
    std::string file_path, file_type, af2, campaign;
    int dsid;

    /// Open file
    std::fstream file(path.c_str(), std::ios_base::in);

    if (!file.is_open()) {
        std::cerr << "FileProcessorSingleLep::ReadFileList: Cannot open the file list at" << std::endl;
        exit(EXIT_FAILURE);
    }

    while (file >> file_path >> file_type >> af2 >> dsid >> campaign) {
        m_file_path.emplace_back(file_path);
        m_file_type.emplace_back(file_type);
        m_dsid.emplace_back(dsid);
        m_af2.emplace_back(af2);
        m_campaign.emplace_back(campaign);
    }

    if (m_file_path.size() == 0) {
        std::cerr << "FileProcessorSingleLep::ReadFileList: File list is empty?" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Check consistency
    if (m_file_path.size() != m_file_type.size()) {
        std::cerr << "FileProcessor:SingleLep:ReadFileList: Size of file_path and file_type does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_file_path.size() != m_campaign.size()) {
        std::cerr << "FileProcessorSingleLep::ReadFileList: Size of file_path and campaign does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_file_path.size() != m_dsid.size()) {
        std::cerr << "FileProcessorSingleLep::ReadFileList: Size of file_path and dsid does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "\nFileProcessorSingleLep::ReadFileList: File list read successfully \n\n";
}

std::vector<std::string> FileProcessorSingleLep::ReadSystList() const {
    std::vector<std::string> result;

    std::size_t position(0);
    bool found(false);

    for (std::size_t i = 0; i < m_file_type.size(); ++i) {

        /// Get the list of systematics from the nominal ttbar file
        if (m_file_type.at(i) == "ttbar_PP8_singlelep_FS") {
            found = true;
            position = i;
            break;
        }
    }

    if (!found) {
        std::cerr << "FileProcessorSingleLep::ReadSystList: Did not find a ttbar_PP8_FS file which we need to read list of systs" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// open the file
    std::unique_ptr<TFile> file (TFile::Open(m_file_path.at(position).c_str(), "READ"));
    if (!file) {
        std::cerr << "FileProcessorSingleLep::ReadSystList: Cannot open file at: " << m_file_path.at(position) << std::endl;
        exit(EXIT_FAILURE);
    }

    file->cd();
    TIter next(gDirectory->GetListOfKeys());
    TKey *key;

    static const std::vector<std::string> exclude = {"nominal",
                                                     "sumWeights",
                                                     "truth",
                                                     "particleLevel",
                                                     "AnalysisTracking",
                                                     "ejets_2022",
                                                     "mujets_2022"};

    while ((key = static_cast<TKey*>(next()))) {
        if (key->IsFolder()) {
            const std::string name = key->GetName();
            if (std::find(exclude.begin(), exclude.end(), name) == exclude.end()) {
                result.emplace_back(name);
            }
        }
    }

    file->Close();

    return result;
}

void FileProcessorSingleLep::CreateOutputFiles(const std::string& path) const {
    gSystem->MakeDirectory((path+"/"+m_name).c_str());
    for (const auto& iprocess : m_process) {
        const std::string s = path +"/"+ m_name +"/"+ iprocess + "_"+m_name;
        std::unique_ptr<TFile> f_el(TFile::Open((s+"_el.root.temp").c_str(), "RECREATE"));
        f_el->Close();
        std::unique_ptr<TFile> f_mu(TFile::Open((s+"_mu.root.temp").c_str(), "RECREATE"));
        f_mu->Close();
    }
}

void FileProcessorSingleLep::RenameOutputFiles(const std::string& path) const {
    std::cout << "\nFileProcessorSingleLep::RenameOutputFiles: Everything went successfully, renaming files\n";
    for (const auto& iprocess : m_process) {
        const std::string in  = path+"/"+m_name+"/"+iprocess+"_"+m_name;
        const std::string out = path+"/"+m_name+"/"+iprocess+"_"+m_name;
        gSystem->Rename((in+"_el.root.temp").c_str(),(out+"_el.root").c_str());
        gSystem->Rename((in+"_mu.root.temp").c_str(),(out+"_mu.root").c_str());
    } 
}
