#include "TTbarXsec/HistoMakerDilep.h"
#include "TTbarXsec/Common.h"
#include "TTbarXsec/Event.h"
#include "TTbarXsec/ParticleEvent.h"
#include "TTbarXsec/TruthEvent.h"
#include "TTbarXsec/WeightManager.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TH1D.h"
#include "TRandom3.h"
#include "TTree.h"

#include <iostream>

HistoMakerDilep::HistoMakerDilep(const std::string& type,
                                 const std::string& systematics,
                                 const std::vector<std::string>& sfsyst_list) :
    m_type(type),
    m_systematics(systematics),
    m_current_syst("nominal"),
    m_isMC(true),
    m_dsid(-1),
    m_tree_name("nominal"),
    m_sfsyst_list(sfsyst_list),
    m_isSherpa(false),
    m_control_plots(nullptr) {
   
    /// Allocating on heap as these are huge objects
    m_control_plots = std::make_unique<ControlPlots>();
    m_control_plots->Init(m_sfsyst_list, true);
    m_control_plots->SetSyst(m_systematics);
    m_eventYields.emplace_back(std::make_pair(Common::LEPTONTYPE::EE, 0));
    m_eventYields.emplace_back(std::make_pair(Common::LEPTONTYPE::MUMU, 0));
    m_eventYields.emplace_back(std::make_pair(Common::LEPTONTYPE::EMU, 0));
    m_duplicateEventCheck.clear();
    if (type == "ttbar_PP8_dilep_FS" && systematics == "sfsyst") {
        m_reweighter = std::make_unique<TTbarNNLORecursiveRew>(410470, 0, false);
        m_reweighter->SetInputDirectory("TTbarNNLOReweighter/data");
        //myReweighter->SetInputSuffix("");
        m_reweighter->AddRew("TopPt");
        m_reweighter->Init();
    }
    if (type.find("Sherpa") != std::string::npos) {
        m_isSherpa = true;
    }
}

void HistoMakerDilep::FillHistos(const WeightManager& wei_manager,
                                 const std::string& tree_name,
                                 const std::string& path,
                                 const DileptonMatcher::MATCHINGOPTION& /*opt*/,
                                 const bool ignoreFakes) {

    
    /// Set the tree name
    m_tree_name = tree_name;

    std::unique_ptr<TFile> file(TFile::Open(path.c_str(), "READ"));
    if (!file) {
        std::cerr << "HistoMakerDilep::FillHistos: Cannot open input file: " << path << std::endl;
        exit(EXIT_FAILURE);
    }
    TTree *tree = (dynamic_cast<TTree*>(file->Get(tree_name.c_str())));
    if (!tree) {
        std::cerr << "HistoMakerDilep::FillHistos: Cannot read tree: " << tree_name << " from file: " << path << std::endl;
        exit(EXIT_FAILURE);
    }

    TTree *treeTruth(nullptr);
    TTree *particleTree(nullptr);
    const int nentries = tree->GetEntries();

    bool is_syst(false);
    if (m_systematics == "syst") is_syst = true;

    Event event(tree, m_isMC, false, is_syst, Common::CHANNEL::DILEPTON);
    std::unique_ptr<TruthEvent> truthEvent(nullptr);
    std::unique_ptr<ParticleEvent> particleEvent(nullptr);
    if (m_type == "ttbar_PP8_dilep_FS" && m_systematics == "sfsyst") {
        treeTruth = (static_cast<TTree*>(file->Get("truth")));
        if (!tree) {
            std::cerr << "HistoMakerDilep::FillHistos: Cannot read tree: truth from file: " << path << std::endl;
            exit(EXIT_FAILURE);
        }

        truthEvent = std::make_unique<TruthEvent>(treeTruth);
        const auto status = treeTruth->BuildIndex("mcChannelNumber", "eventNumber");
        if (status <= 0) {
            std::cerr << "HistoMakerDilep::FillHistos: Cannot match reco and truth\n";
            std::exit(EXIT_FAILURE);
        } 
    }
    if (m_type == "Zjets_PP8_FS" || m_type == "Zjets_Sherpa_FS") {
        particleTree = (static_cast<TTree*>(file->Get("particleLevel")));
        if (!tree) {
            std::cerr << "HistoMakerDilep::FillHistos: Cannot read tree: particleLevel from file: " << path << std::endl;
            exit(EXIT_FAILURE);
        }

        particleEvent = std::make_unique<ParticleEvent>(particleTree);
        const auto status = particleTree->BuildIndex("mcChannelNumber", "eventNumber");
        if (status <= 0) {
            std::cerr << "HistoMakerDilep::FillHistos: Cannot match reco and particleTruth\n";
            std::exit(EXIT_FAILURE);
        } 
    }


    //DileptonMatcher matcher(opt);
    TRandom3 rng(123456);

    /// Run event loop
    for (int ievent = 0; ievent < nentries; ++ievent) {
        event.GetEntry(ievent);
        if (ievent % 100000 == 0) {
            std::cout << "\t\tProcessing event " << ievent << ", out of " << nentries << " events\n";
        }

        //Common::RemoveJetsBelowX(&event, 60000);

        m_lepton_type = Common::FindLeptonType(event);

        if (m_isMC) {
            const bool has_fakes = ignoreFakes ? false : Common::EventHasLeptonFakes(event);
            if (has_fakes && m_type != "Multijet") continue;
            if (!has_fakes && m_type == "Multijet") continue;
        }

        /// Selection
        if (m_lepton_type != Common::LEPTONTYPE::EMU) {
            if (event.dileptonMass/1e3<15) continue;
        }

        if (m_systematics == "sfsyst" && m_type == "ttbar_PP8_dilep_FS") {
            const int i = treeTruth->GetEntryNumberWithIndex(event.mcChannelNumber, event.eventNumber);
            if (i < 0) {
                std::cerr << "HistoMakerDilep::FillHistos: Cannot match reco and truth!\n";
                exit(EXIT_FAILURE);
            }
            truthEvent->GetEntry(i);
        }

        bool isFiducial(true);

        if (m_type == "Zjets_PP8_FS" || m_type == "Zjets_Sherpa_FS") {
            const int i = particleTree->GetEntryNumberWithIndex(event.mcChannelNumber, event.eventNumber);
            if (i < 0) {
                std::cerr << "HistoMakerDilep::FillHistos: Cannot match reco and truth!\n";
                exit(EXIT_FAILURE);
            }
            particleEvent->GetEntry(i);
            isFiducial = Common::IsFiducialZ(*particleEvent);
        }

        /// Prepare vector with weights
        std::vector<double> wei_vec;
        if (m_isMC) {
            if (m_systematics == "sfsyst") {
                wei_vec = wei_manager.GetSfSystWeights(event, m_isSherpa, truthEvent.get(), m_reweighter.get());
            } else {
                wei_vec.emplace_back(wei_manager.GetNominalWeight(event, m_isSherpa));
            }
        } else {
            // weight is one for data
            if (m_systematics == "sfsyst") {
                rng.SetSeed(event.eventNumber);
                for (int i = 0; i < 100; ++i) {
                    wei_vec.emplace_back(static_cast<double>(rng.Poisson(1)));
                }
            } else {
                wei_vec.emplace_back(1.);
            }
        }

        if (!is_syst && wei_vec.size() == 1) {
            if (!m_duplicateEventCheck.insert({m_type == "Data" ? event.runNumber : event.mcChannelNumber, event.eventNumber}).second) {
                std::cout << "\t\t RunNumber << " << (m_type == "Data" ? event.runNumber : event.mcChannelNumber) << ", event number: " << event.eventNumber << " is duplicate\n";
            }
        }

        if (wei_vec.size() != m_sfsyst_list.size()) {
            std::cerr << "HistoMakerDilep::FillHistos: Size of sf names and sf weights do not match" << std::endl;
            std::cerr << "HistoMakerDilep::FillHistos: names size: " << m_sfsyst_list.size() << ", weights: " << wei_vec.size() << std::endl;
            exit(EXIT_FAILURE);
        }

        auto itr = std::find_if(m_eventYields.begin(), m_eventYields.end(), [this](const auto& element){return element.first == this->m_lepton_type;});
        if (itr == m_eventYields.end()) {
            std::cerr << "HistoMakerDilep::FillHistos: Cannot find lepton type\n";
            exit(EXIT_FAILURE);
        }

        itr->second += wei_vec.at(0);

        /// match leptons and bjets
        //const std::pair<std::size_t, std::size_t> pairs = matcher.FindMatching(event, m_lepton_type);

        /// Get the region
        const Common::REGION reg = Common::FindRegion(event);

        m_control_plots->FillAllHistos(event,
                                       {0,0},
                                       m_isMC,
                                       m_lepton_type,
                                       wei_vec,
                                       isFiducial,
                                       reg);
    }
        
    tree->SetDirectory(nullptr);
    delete tree;
    if (m_type == "ttbar_PP8_dilep_FS" && m_systematics == "sfsyst") {
        treeTruth->SetDirectory(nullptr);
        delete treeTruth;
    }
    file->Close();
}

void HistoMakerDilep::Finalise() {
    m_control_plots->Finalise();
    std::cout << "HistoMakerDilep::Finalise: Sample: " << m_type << "\n";
    for (const auto& i : m_eventYields) {
        std::cout << "HistoMakerDilep::Finalise: \t" << Common::TypeToString(i.first) << ", yields: " << i.second << "\n"; 
    }
}

void HistoMakerDilep::WriteHistosToFile(const std::string& path, const std::string& name, const std::string& parallelSyst) const {
    const std::string suffix = m_systematics == "nominal" ? ".temp" : ""; 
    std::string final_path = path+"/"+name+"/"+m_type+"_"+name;
    if (!parallelSyst.empty()) {
        final_path += "_" + parallelSyst;
    }

    std::unique_ptr<TFile> file_ee(TFile::Open((final_path+"_ee.root"+suffix).c_str(), "UPDATE"));
    std::unique_ptr<TFile> file_mumu(TFile::Open((final_path+"_mumu.root"+suffix).c_str(), "UPDATE"));
    std::unique_ptr<TFile> file_emu(TFile::Open((final_path+"_emu.root"+suffix).c_str(), "UPDATE"));
    if (!file_ee || !file_mumu || !file_emu) {
        std::cerr << "HistoMakerDilep::WriteHistosToFile: Cannot open file at: " << final_path << std::endl;
        exit(EXIT_FAILURE);
    }
    
    const std::vector<std::string> regNames = Common::GetRegionString(Common::CHANNEL::DILEPTON);
    const std::vector<std::string> regNamesEMu = Common::GetRegionString(Common::CHANNEL::DILEPTON, Common::LEPTONTYPE::EMU);

    auto makeRegString = [&regNames, &regNamesEMu](TFile *file, const std::string& folder, const bool isEMu = false) {
        const auto& regVec = isEMu ? regNamesEMu : regNames;
        for (const auto& i : regVec) {
            file->cd(folder.c_str());
            gDirectory->mkdir(i.c_str());
        }
    };

    /// Create the directory structure
    if (m_systematics == "nominal") {
        file_ee->cd();
        gDirectory->mkdir("nominal");
        file_mumu->cd();
        gDirectory->mkdir("nominal");
        file_emu->cd();
        gDirectory->mkdir("nominal");
        makeRegString(file_ee.get(), "nominal");
        makeRegString(file_mumu.get(), "nominal");
        makeRegString(file_emu.get(), "nominal", true);
    } else if (m_systematics == "syst") {
        if (!file_ee->Get(m_current_syst.c_str())) {
            file_ee->cd();
            gDirectory->mkdir(m_current_syst.c_str());
            makeRegString(file_ee.get(), m_current_syst);
        }
        if (!file_mumu->Get(m_current_syst.c_str())) {
            file_mumu->cd();
            gDirectory->mkdir(m_current_syst.c_str());
            makeRegString(file_mumu.get(), m_current_syst);
        }
        if (!file_emu->Get(m_current_syst.c_str())) {
            file_emu->cd();
            gDirectory->mkdir(m_current_syst.c_str());
            makeRegString(file_emu.get(), m_current_syst, true);
        }
    } else if (m_systematics == "sfsyst") {
        for (const auto& isf : m_sfsyst_list) {
            if (!file_ee->Get(isf.c_str())) {
                file_ee->cd();
                gDirectory->mkdir(isf.c_str());
                makeRegString(file_ee.get(), isf);
            }
            if (!file_mumu->Get(isf.c_str())) {
                file_mumu->cd();
                gDirectory->mkdir(isf.c_str());
                makeRegString(file_mumu.get(), isf);
            }
            if (!file_emu->Get(isf.c_str())) {
                file_emu->cd();
                gDirectory->mkdir(isf.c_str());
                makeRegString(file_emu.get(), isf, true);
            }
        }
    }

    m_control_plots->Write(m_current_syst, file_ee.get(), file_mumu.get(), file_emu.get(), nullptr, nullptr);

    file_ee->Close();
    file_mumu->Close();
    file_emu->Close();
}
