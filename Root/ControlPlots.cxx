#include "TTbarXsec/ControlPlots.h"
#include "TTbarXsec/Common.h"
#include "TTbarXsec/Event.h"

#include "TFile.h"
#include "Math/Vector4D.h"

#include <iostream>
#include <numeric>
#include <thread>

ControlPlots::ControlPlots() noexcept :
    m_syst("nominal"),
    m_isMC(true),
    m_isDilepton(true) {
}

void ControlPlots::Init(const std::vector<std::string>& sf_syst_names, const bool isDilepton) {

    m_sf_syst_names = sf_syst_names;
    m_isDilepton = isDilepton;
    m_regionNamesNotEMu = Common::GetRegionString(isDilepton ? Common::CHANNEL::DILEPTON : Common::CHANNEL::SINGLELEPTON);
    m_regionNamesEMu    = Common::GetRegionString(isDilepton ? Common::CHANNEL::DILEPTON : Common::CHANNEL::SINGLELEPTON, Common::LEPTONTYPE::EMU);
    std::vector<Histos> histoVec;
    for (std::size_t i = 0; i < m_regionNamesNotEMu.size(); ++i) {
        histoVec.emplace_back(Histos{});
    }
    std::vector<Histos> histoVecEMu;
    for (std::size_t i = 0; i < m_regionNamesEMu.size(); ++i) {
        histoVecEMu.emplace_back(Histos{});
    }
    if (isDilepton) {
        m_histoVec.emplace_back(std::make_pair(Common::LEPTONTYPE::EE, histoVec));
        m_histoVec.emplace_back(std::make_pair(Common::LEPTONTYPE::MUMU, histoVec));
        m_histoVec.emplace_back(std::make_pair(Common::LEPTONTYPE::EMU, histoVecEMu));
    } else {
        m_histoVec.emplace_back(std::make_pair(Common::LEPTONTYPE::EL, histoVec));
        m_histoVec.emplace_back(std::make_pair(Common::LEPTONTYPE::MU, histoVec));
    }

    for (auto& ihist : m_histoVec) {
        for (auto& iregHist : ihist.second) {
            for (std::size_t i = 0; i < m_sf_syst_names.size(); ++i) {
                iregHist.jet1_pt.emplace_back("","",25, 30, 430);
                iregHist.jet1_eta.emplace_back("","",25, -2.5, 2.5);
                iregHist.jet1_phi.emplace_back("","",25, -3.14, 3.14);
                iregHist.jet1_e.emplace_back("","",25, 0, 600);
                iregHist.jet2_pt.emplace_back("","",25, 30, 330);
                iregHist.jet2_eta.emplace_back("","",25, -2.5, 2.5);
                iregHist.jet2_phi.emplace_back("","",25, -3.14, 3.14);
                iregHist.jet2_e.emplace_back("","",25, 0, 500);
                iregHist.jet3_pt.emplace_back("","",25, 30, 230);
                iregHist.jet3_eta.emplace_back("","",25, -2.5, 2.5);
                iregHist.jet3_phi.emplace_back("","",25, -3.14, 3.14);
                iregHist.jet3_e.emplace_back("","",25, 0, 400);
                iregHist.jet4_pt.emplace_back("","",25, 30, 180);
                iregHist.jet4_eta.emplace_back("","",25, -2.5, 2.5);
                iregHist.jet4_phi.emplace_back("","",25, -3.14, 3.14);
                iregHist.jet4_e.emplace_back("","",25, 0, 300);
                iregHist.jet5_pt.emplace_back("","",25, 30, 130);
                iregHist.jet5_eta.emplace_back("","",25, -2.5, 2.5);
                iregHist.jet5_phi.emplace_back("","",25, -3.14, 3.14);
                iregHist.jet5_e.emplace_back("","",25, 0, 100);
                iregHist.jet6_pt.emplace_back("","",25, 30, 180);
                iregHist.jet6_eta.emplace_back("","",25, -2.5, 2.5);
                iregHist.jet6_phi.emplace_back("","",25, -3.14, 3.14);
                iregHist.jet6_e.emplace_back("","",25, 0, 150);
                iregHist.HT.emplace_back("","",25, 0, 1000);
                if (isDilepton) {
                    iregHist.jet_n.emplace_back("","", 7, -0.5, 6.5);
                } else {
                    iregHist.jet_n.emplace_back("","", 6, 3.5, 9.5);
                }
                iregHist.bjet_n_77.emplace_back("","",4, -0.5, 3.5);
                iregHist.jet_tagbin.emplace_back("","",5, 0.5, 5.5);
                iregHist.jet1_tagbin.emplace_back("","",5, 0.5, 5.5);
                iregHist.jet2_tagbin.emplace_back("","",5, 0.5, 5.5);
                iregHist.jet3_tagbin.emplace_back("","",5, 0.5, 5.5);
                iregHist.jet4_tagbin.emplace_back("","",5, 0.5, 5.5);
                iregHist.bjet_pt_77.emplace_back("","",25, 30, 430);
                iregHist.bjet_eta_77.emplace_back("","",25, -2.5, 2.5);
                iregHist.bjet_phi_77.emplace_back("","",25, -3.14, 3.14);
                iregHist.bjet_e_77.emplace_back("","",25, 0, 600);

                if (m_isDilepton) {
                    iregHist.lepton1_pt.emplace_back("","",25, 27, 327);
                    iregHist.lepton1_eta.emplace_back("","",25, -2.5, 2.5);
                    iregHist.lepton1_phi.emplace_back("","",25, -3.14, 3.14);
                    iregHist.lepton1_e.emplace_back("","",25, 0, 400);
                    iregHist.lepton2_pt.emplace_back("","",25, 27, 327);
                    iregHist.lepton2_eta.emplace_back("","",25, -2.5, 2.5);
                    iregHist.lepton2_phi.emplace_back("","",25, -3.14, 3.14);
                    iregHist.lepton2_e.emplace_back("","",25, 0, 400);
                    iregHist.electron_pt.emplace_back("","",25, 27, 327);
                    iregHist.electron_eta.emplace_back("","",25, -2.5, 2.5);
                    iregHist.electron_phi.emplace_back("","",25, -3.14, 3.14);
                    iregHist.electron_e.emplace_back("","",25, 0, 400);
                    iregHist.muon_pt.emplace_back("","",25, 27, 327);
                    iregHist.muon_eta.emplace_back("","",25, -2.5, 2.5);
                    iregHist.muon_phi.emplace_back("","",25, -3.14, 3.14);
                    iregHist.muon_e.emplace_back("","",25, 0, 400);
                    if (ihist.first == Common::LEPTONTYPE::EE || ihist.first == Common::LEPTONTYPE::MUMU) {
                        iregHist.dilepton_m.emplace_back("","",50, 0, 250);
                    } else {
                        iregHist.dilepton_m.emplace_back("","",50, 0, 500);
                    }
                    iregHist.dilepton_m_peak.emplace_back("","",50, 66, 116);
                    iregHist.dilepton_m_peak_fid.emplace_back("","",50, 66, 116);
                    iregHist.dilepton_m_peak_oof.emplace_back("","",50, 66, 116);
                    iregHist.dilepton_m_peak_5bins.emplace_back("","",5, 66, 116);
                    iregHist.dilepton_m_peak_5bins_fid.emplace_back("","",5, 66, 116);
                    iregHist.dilepton_m_peak_5bins_oof.emplace_back("","",5, 66, 116);
                    iregHist.dilepton_m_peak_10bins.emplace_back("","",10, 66, 116);
                    iregHist.dilepton_m_peak_10bins_fid.emplace_back("","",10, 66, 116);
                    iregHist.dilepton_m_peak_10bins_oof.emplace_back("","",10, 66, 116);
                    iregHist.dilepton_m_peak_15bins.emplace_back("","",15, 66, 116);
                    iregHist.dilepton_m_peak_15bins_fid.emplace_back("","",15, 66, 116);
                    iregHist.dilepton_m_peak_15bins_oof.emplace_back("","",15, 66, 116);
                    iregHist.dilepton_m_peak_20bins.emplace_back("","",20, 66, 116);
                    iregHist.dilepton_m_peak_20bins_fid.emplace_back("","",20, 66, 116);
                    iregHist.dilepton_m_peak_20bins_oof.emplace_back("","",20, 66, 116);
                    iregHist.dilepton_m_peak_25bins.emplace_back("","",25, 66, 116);
                    iregHist.dilepton_m_peak_25bins_fid.emplace_back("","",25, 66, 116);
                    iregHist.dilepton_m_peak_25bins_oof.emplace_back("","",25, 66, 116);
                    iregHist.dilepton_pt.emplace_back("","",50, 0, 300);
                    iregHist.dilepton_deltaR.emplace_back("","",25, 0, 5);
                    iregHist.dilepton_deltaPhi.emplace_back("","",25, -3.14, 3.14);
                    iregHist.n_geq0.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_geq0_fid.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_geq0_oof.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_1bjet.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_2bjet.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_geq0_30.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_1bjet_30.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_2bjet_30.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_geq0_35.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_1bjet_35.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_2bjet_35.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_geq0_leadingElectron.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_geq0_leadingMuon.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_1bjet_leadingElectron.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_1bjet_leadingMuon.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_2bjet_leadingElectron.emplace_back("","",1, 0.5, 1.5);
                    iregHist.n_2bjet_leadingMuon.emplace_back("","",1, 0.5, 1.5);
                    iregHist.jet_Z_balance_pt_17_30.emplace_back("","",25,0.,3.);
                    iregHist.jet_Z_balance_pt_30_50.emplace_back("","",25,0.,3.);
                    iregHist.jet_Z_balance_pt_50_100.emplace_back("","",25,0.,3.);
                    iregHist.jet_Z_balance_pt_100_200.emplace_back("","",25,0.,3.);
                    iregHist.jet_Z_balance_pt_200_inf.emplace_back("","",25,0.,3.);
                    iregHist.jet_Z_balance_jet_pt.emplace_back("","",50,30.,300.);
                    iregHist.jet_Z_balance_Z_pt.emplace_back("","",50,15.,300.);
                } else {
                    iregHist.lepton_pt.emplace_back("","",25, 27, 327);
                    iregHist.lepton_eta.emplace_back("","",25, -2.5, 2.5);
                    iregHist.lepton_phi.emplace_back("","",25, -3.14, 3.14);
                    iregHist.lepton_e.emplace_back("","",25, 0, 300);
                    iregHist.mwt.emplace_back("","",25, 0, 300);
                    iregHist.reco_W_mass.emplace_back("","",25, 0, 250);
                    iregHist.reco_top_mass.emplace_back("","",25, 0, 350);
                    iregHist.chi2.emplace_back("","",25, 0, 100);
                }
                iregHist.met.emplace_back("","",25, 0, 250);
                iregHist.met_phi.emplace_back("","",25, -3.14, 3.14);
                iregHist.met_x.emplace_back("","",25, 0, 150);
                iregHist.met_y.emplace_back("","",25, 0, 150);
                iregHist.met_sumet.emplace_back("","",25, 0, 1200);
                iregHist.average_mu.emplace_back("","",50, 0, 100);
                iregHist.actual_mu.emplace_back("","",50, 0, 100);
            }
        }
    }
}

void ControlPlots::SetSyst(const std::string& syst) {
    m_syst = syst;
}

void ControlPlots::FillAllHistos(const Event& event,
                                 const std::pair<std::size_t, std::size_t>& /*pairs*/,
                                 const bool& isMC,
                                 const Common::LEPTONTYPE& lepton_type,
                                 const std::vector<double>& weights,
                                 const bool isFiducial,
                                 const Common::REGION reg) {

    m_isMC = isMC;
    m_weights = weights;
    m_lepton_type = lepton_type;
    auto itr = std::find_if(m_histoVec.begin(), m_histoVec.end(), [&lepton_type](const auto& element){return element.first == lepton_type;});
    if (itr == m_histoVec.end()) {
        std::cerr << "ControlPlots::FillAllHistos: No known type of the lepton!\n";
        exit(EXIT_FAILURE);
    }

    const auto& regVector = (lepton_type == Common::LEPTONTYPE::EMU) ? m_regionNamesEMu : m_regionNamesNotEMu;
    /// find which region to fill
    auto itrReg = std::find(regVector.begin(), regVector.end(), Common::RegionToString(reg));
    if (itrReg == regVector.end()) {
        std::cerr << "ControlPlots::FillAllHistos: Unknown region " << Common::RegionToString(reg) << "!\n";
        exit(EXIT_FAILURE);
    }

    const std::size_t index = std::distance(regVector.begin(), itrReg);

    const std::size_t nTags = Common::GetNbtags_77(event);
        
    std::vector<std::thread> threads;

    if ((reg == Common::REGION::OPPOSITESIGN) && (nTags > 0)) {
        auto itrReg1b = std::find(regVector.begin(), regVector.end(), "OS_1b");
        if (itrReg1b == regVector.end()) {
            std::cerr << "ControlPlots::FillAllHistos: Unknown region " << Common::RegionToString(reg) << "!\n";
            exit(EXIT_FAILURE);
        }
        const std::size_t index1b = std::distance(regVector.begin(), itrReg1b);;

        threads.emplace_back([this, &event, &itr, &index1b] {ControlPlots::FillJetPlots(event, itr, index1b);});
        
        threads.emplace_back([this, &event, &itr, &index1b] {ControlPlots::FillTagPlots(event, itr, index1b);});
        
        threads.emplace_back([this, &event, &itr, &index1b] {ControlPlots::FillOtherPlots(event, itr, index1b);});
        
        threads.emplace_back([this, &event, &itr, &index1b, &isFiducial] {ControlPlots::FillDileptonPlots(event, itr, index1b, isFiducial);});
        
        threads.emplace_back([this, &event, &itr, &index1b] {ControlPlots::FillDileptonPlotsExtra(event, itr, index1b);});
    }

    threads.emplace_back([this, &event, &itr, &index] {ControlPlots::FillJetPlots(event, itr, index);});
    
    threads.emplace_back([this, &event, &itr, &index] {ControlPlots::FillTagPlots(event, itr, index);});

    threads.emplace_back([this, &event, &itr, &index] {ControlPlots::FillOtherPlots(event, itr, index);});

    if (m_isDilepton) {
        threads.emplace_back([this, &event, &itr, &index, &isFiducial] {ControlPlots::FillDileptonPlots(event, itr, index, isFiducial);});
        
        threads.emplace_back([this, &event, &itr, &index] {ControlPlots::FillDileptonPlotsExtra(event, itr, index);});
        
    } else {
        threads.emplace_back([this, &event, &itr, &index] {ControlPlots::FillSingleLepPlots(event, itr, index);});
    }

    for (auto& ithread : threads) {
        ithread.join();
    }
}

void ControlPlots::Finalise() {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        for (auto& ihist : m_histoVec) {
            for (auto& iregHist : ihist.second) {
                Common::AddOverflow(&iregHist.jet1_pt.at(i));
                Common::AddOverflow(&iregHist.jet1_eta.at(i));
                Common::AddOverflow(&iregHist.jet1_phi.at(i));
                Common::AddOverflow(&iregHist.jet1_e.at(i));
                Common::AddOverflow(&iregHist.jet2_pt.at(i));
                Common::AddOverflow(&iregHist.jet2_eta.at(i));
                Common::AddOverflow(&iregHist.jet2_phi.at(i));
                Common::AddOverflow(&iregHist.jet2_e.at(i));
                Common::AddOverflow(&iregHist.jet3_pt.at(i));
                Common::AddOverflow(&iregHist.jet3_eta.at(i));
                Common::AddOverflow(&iregHist.jet3_phi.at(i));
                Common::AddOverflow(&iregHist.jet3_e.at(i));
                Common::AddOverflow(&iregHist.jet4_pt.at(i));
                Common::AddOverflow(&iregHist.jet4_eta.at(i));
                Common::AddOverflow(&iregHist.jet4_phi.at(i));
                Common::AddOverflow(&iregHist.jet4_e.at(i));
                Common::AddOverflow(&iregHist.jet5_pt.at(i));
                Common::AddOverflow(&iregHist.jet5_eta.at(i));
                Common::AddOverflow(&iregHist.jet5_phi.at(i));
                Common::AddOverflow(&iregHist.jet5_e.at(i));
                Common::AddOverflow(&iregHist.jet6_pt.at(i));
                Common::AddOverflow(&iregHist.jet6_eta.at(i));
                Common::AddOverflow(&iregHist.jet6_phi.at(i));
                Common::AddOverflow(&iregHist.jet6_e.at(i));
                Common::AddOverflow(&iregHist.HT.at(i));
                Common::AddOverflow(&iregHist.jet_n.at(i));
                Common::AddOverflow(&iregHist.bjet_n_77.at(i));
                Common::AddOverflow(&iregHist.jet_tagbin.at(i));
                Common::AddOverflow(&iregHist.jet1_tagbin.at(i));
                Common::AddOverflow(&iregHist.jet2_tagbin.at(i));
                Common::AddOverflow(&iregHist.jet3_tagbin.at(i));
                Common::AddOverflow(&iregHist.jet4_tagbin.at(i));
                Common::AddOverflow(&iregHist.bjet_pt_77.at(i));
                Common::AddOverflow(&iregHist.bjet_eta_77.at(i));
                Common::AddOverflow(&iregHist.bjet_phi_77.at(i));
                Common::AddOverflow(&iregHist.bjet_e_77.at(i));

                if (m_isDilepton) {
                    Common::AddOverflow(&iregHist.lepton1_pt.at(i));
                    Common::AddOverflow(&iregHist.lepton1_eta.at(i));
                    Common::AddOverflow(&iregHist.lepton1_phi.at(i));
                    Common::AddOverflow(&iregHist.lepton1_e.at(i));
                    Common::AddOverflow(&iregHist.lepton2_pt.at(i));
                    Common::AddOverflow(&iregHist.lepton2_eta.at(i));
                    Common::AddOverflow(&iregHist.lepton2_phi.at(i));
                    Common::AddOverflow(&iregHist.lepton2_e.at(i));
                    Common::AddOverflow(&iregHist.electron_pt.at(i));
                    Common::AddOverflow(&iregHist.electron_eta.at(i));
                    Common::AddOverflow(&iregHist.electron_phi.at(i));
                    Common::AddOverflow(&iregHist.electron_e.at(i));
                    Common::AddOverflow(&iregHist.muon_pt.at(i));
                    Common::AddOverflow(&iregHist.muon_eta.at(i));
                    Common::AddOverflow(&iregHist.muon_phi.at(i));
                    Common::AddOverflow(&iregHist.muon_e.at(i));
                    Common::AddOverflow(&iregHist.dilepton_m.at(i));
                    Common::AddOverflow(&iregHist.dilepton_m_peak.at(i));
                    Common::AddOverflow(&iregHist.dilepton_m_peak_5bins.at(i));
                    Common::AddOverflow(&iregHist.dilepton_m_peak_10bins.at(i));
                    Common::AddOverflow(&iregHist.dilepton_m_peak_15bins.at(i));
                    Common::AddOverflow(&iregHist.dilepton_m_peak_20bins.at(i));
                    Common::AddOverflow(&iregHist.dilepton_m_peak_25bins.at(i));
                    Common::AddOverflow(&iregHist.dilepton_deltaR.at(i));
                    Common::AddOverflow(&iregHist.dilepton_deltaPhi.at(i));
                    Common::AddOverflow(&iregHist.dilepton_pt.at(i));
                    Common::AddOverflow(&iregHist.n_geq0.at(i));
                    Common::AddOverflow(&iregHist.n_1bjet.at(i));
                    Common::AddOverflow(&iregHist.n_2bjet.at(i));
                    Common::AddOverflow(&iregHist.n_geq0_30.at(i));
                    Common::AddOverflow(&iregHist.n_1bjet_30.at(i));
                    Common::AddOverflow(&iregHist.n_2bjet_30.at(i));
                    Common::AddOverflow(&iregHist.n_geq0_35.at(i));
                    Common::AddOverflow(&iregHist.n_1bjet_35.at(i));
                    Common::AddOverflow(&iregHist.n_2bjet_35.at(i));
                    Common::AddOverflow(&iregHist.n_geq0_leadingElectron.at(i));
                    Common::AddOverflow(&iregHist.n_geq0_leadingMuon.at(i));
                    Common::AddOverflow(&iregHist.n_1bjet_leadingElectron.at(i));
                    Common::AddOverflow(&iregHist.n_1bjet_leadingMuon.at(i));
                    Common::AddOverflow(&iregHist.n_2bjet_leadingElectron.at(i));
                    Common::AddOverflow(&iregHist.n_2bjet_leadingMuon.at(i));
                    Common::AddOverflow(&iregHist.jet_Z_balance_pt_17_30.at(i));
                    Common::AddOverflow(&iregHist.jet_Z_balance_pt_30_50.at(i));
                    Common::AddOverflow(&iregHist.jet_Z_balance_pt_50_100.at(i));
                    Common::AddOverflow(&iregHist.jet_Z_balance_pt_100_200.at(i));
                    Common::AddOverflow(&iregHist.jet_Z_balance_pt_200_inf.at(i));
                    Common::AddOverflow(&iregHist.jet_Z_balance_jet_pt.at(i));
                    Common::AddOverflow(&iregHist.jet_Z_balance_Z_pt.at(i));
                } else {
                    Common::AddOverflow(&iregHist.lepton_pt.at(i));
                    Common::AddOverflow(&iregHist.lepton_eta.at(i));
                    Common::AddOverflow(&iregHist.lepton_phi.at(i));
                    Common::AddOverflow(&iregHist.lepton_e.at(i));
                    Common::AddOverflow(&iregHist.mwt.at(i));
                    Common::AddOverflow(&iregHist.reco_W_mass.at(i));
                    Common::AddOverflow(&iregHist.reco_top_mass.at(i));
                }
                Common::AddOverflow(&iregHist.met.at(i));
                Common::AddOverflow(&iregHist.met_x.at(i));
                Common::AddOverflow(&iregHist.met_y.at(i));
                Common::AddOverflow(&iregHist.met_phi.at(i));
                Common::AddOverflow(&iregHist.met_sumet.at(i));
                Common::AddOverflow(&iregHist.average_mu.at(i));
                Common::AddOverflow(&iregHist.actual_mu.at(i));
            }
        }
    }
}

void ControlPlots::Write(const std::string& name, TFile* file_ee, TFile* file_mumu, TFile* file_emu, TFile* file_el, TFile* file_mu) const {
    for (std::size_t i = 0; i < m_sf_syst_names.size(); ++i) {
        for (auto& ihist : m_histoVec) {
            auto leptonType = ihist.first;
            const auto& regVector = (leptonType == Common::LEPTONTYPE::EMU) ? m_regionNamesEMu : m_regionNamesNotEMu;
            std::size_t ireg = 0;
            for (auto& iregHist : ihist.second) {
                const std::string regName = regVector.at(ireg);
                switch (leptonType) {
                    case (Common::LEPTONTYPE::EE):
                        if (m_syst == "syst"){
                            file_ee->cd((name+"/"+regName).c_str());
                        } else {
                            file_ee->cd((m_sf_syst_names.at(i)+"/"+regName).c_str());
                        }
                        break;
                    case (Common::LEPTONTYPE::MUMU):
                        if (m_syst == "syst"){
                            file_mumu->cd((name+"/"+regName).c_str());
                        } else {
                            file_mumu->cd((m_sf_syst_names.at(i) + "/" + regName).c_str());
                        }
                        break;
                    case (Common::LEPTONTYPE::EMU):
                        if (m_syst == "syst"){
                            file_emu->cd((name+"/"+regName).c_str());
                        } else {
                            file_emu->cd((m_sf_syst_names.at(i)+"/"+regName).c_str());
                        }
                        break;
                    case (Common::LEPTONTYPE::EL):
                        if (m_syst == "syst"){
                            file_el->cd((name+"/"+regName).c_str());
                        } else {
                            file_el->cd((m_sf_syst_names.at(i)+"/"+regName).c_str());
                        }
                        break;
                    case (Common::LEPTONTYPE::MU):
                        if (m_syst == "syst"){
                            file_mu->cd((name+"/"+regName).c_str());
                        } else {
                            file_mu->cd((m_sf_syst_names.at(i)+"/"+regName).c_str());
                        }
                        break;
                    default:
                        std::cerr << "ControlPlots::Write: Unknown lepton type!";
                        exit(EXIT_FAILURE);
                }
                iregHist.jet1_pt.at(i).Write("jet1_pt", TObject::kOverwrite);
                iregHist.jet1_eta.at(i).Write("jet1_eta", TObject::kOverwrite);
                iregHist.jet1_phi.at(i).Write("jet1_phi", TObject::kOverwrite);
                iregHist.jet1_e.at(i).Write("jet1_e", TObject::kOverwrite);
                iregHist.jet2_pt.at(i).Write("jet2_pt", TObject::kOverwrite);
                iregHist.jet2_eta.at(i).Write("jet2_eta", TObject::kOverwrite);
                iregHist.jet2_phi.at(i).Write("jet2_phi", TObject::kOverwrite);
                iregHist.jet2_e.at(i).Write("jet2_e", TObject::kOverwrite);
                iregHist.jet3_pt.at(i).Write("jet3_pt", TObject::kOverwrite);
                iregHist.jet3_eta.at(i).Write("jet3_eta", TObject::kOverwrite);
                iregHist.jet3_phi.at(i).Write("jet3_phi", TObject::kOverwrite);
                iregHist.jet3_e.at(i).Write("jet3_e", TObject::kOverwrite);
                iregHist.jet4_pt.at(i).Write("jet4_pt", TObject::kOverwrite);
                iregHist.jet4_eta.at(i).Write("jet4_eta", TObject::kOverwrite);
                iregHist.jet4_phi.at(i).Write("jet4_phi", TObject::kOverwrite);
                iregHist.jet4_e.at(i).Write("jet4_e", TObject::kOverwrite);
                iregHist.jet5_pt.at(i).Write("jet5_pt", TObject::kOverwrite);
                iregHist.jet5_eta.at(i).Write("jet5_eta", TObject::kOverwrite);
                iregHist.jet5_phi.at(i).Write("jet5_phi", TObject::kOverwrite);
                iregHist.jet5_e.at(i).Write("jet5_e", TObject::kOverwrite);
                iregHist.jet6_pt.at(i).Write("jet6_pt", TObject::kOverwrite);
                iregHist.jet6_eta.at(i).Write("jet6_eta", TObject::kOverwrite);
                iregHist.jet6_phi.at(i).Write("jet6_phi", TObject::kOverwrite);
                iregHist.jet6_e.at(i).Write("jet6_e", TObject::kOverwrite);
                iregHist.HT.at(i).Write("HT", TObject::kOverwrite);
                iregHist.jet_n.at(i).Write("jet_n", TObject::kOverwrite);
                iregHist.bjet_n_77.at(i).Write("bjet_n_77", TObject::kOverwrite);
                iregHist.jet_tagbin.at(i).Write("jet_tagbin", TObject::kOverwrite);
                iregHist.jet1_tagbin.at(i).Write("jet1_tagbin", TObject::kOverwrite);
                iregHist.jet2_tagbin.at(i).Write("jet2_tagbin", TObject::kOverwrite);
                iregHist.jet3_tagbin.at(i).Write("jet3_tagbin", TObject::kOverwrite);
                iregHist.jet4_tagbin.at(i).Write("jet4_tagbin", TObject::kOverwrite);
                iregHist.bjet_pt_77.at(i).Write("bjet_pt_77", TObject::kOverwrite);
                iregHist.bjet_eta_77.at(i).Write("bjet_eta_77", TObject::kOverwrite);
                iregHist.bjet_phi_77.at(i).Write("bjet_phi_77", TObject::kOverwrite);
                iregHist.bjet_e_77.at(i).Write("bjet_e_77", TObject::kOverwrite);

                if (m_isDilepton) {
                    iregHist.lepton1_pt.at(i).Write("lepton1_pt", TObject::kOverwrite);
                    iregHist.lepton1_eta.at(i).Write("lepton1_eta", TObject::kOverwrite);
                    iregHist.lepton1_phi.at(i).Write("lepton1_phi", TObject::kOverwrite);
                    iregHist.lepton1_e.at(i).Write("lepton1_e", TObject::kOverwrite);
                    iregHist.lepton2_pt.at(i).Write("lepton2_pt", TObject::kOverwrite);
                    iregHist.lepton2_eta.at(i).Write("lepton2_eta", TObject::kOverwrite);
                    iregHist.lepton2_phi.at(i).Write("lepton2_phi", TObject::kOverwrite);
                    iregHist.lepton2_e.at(i).Write("lepton2_e", TObject::kOverwrite);
                    iregHist.electron_pt.at(i).Write("electron_pt", TObject::kOverwrite);
                    iregHist.electron_eta.at(i).Write("electron_eta", TObject::kOverwrite);
                    iregHist.electron_phi.at(i).Write("electron_phi", TObject::kOverwrite);
                    iregHist.electron_e.at(i).Write("electron_e", TObject::kOverwrite);
                    iregHist.muon_pt.at(i).Write("muon_pt", TObject::kOverwrite);
                    iregHist.muon_eta.at(i).Write("muon_eta", TObject::kOverwrite);
                    iregHist.muon_phi.at(i).Write("muon_phi", TObject::kOverwrite);
                    iregHist.muon_e.at(i).Write("muon_e", TObject::kOverwrite);
                    iregHist.dilepton_m.at(i).Write("dilepton_m", TObject::kOverwrite);
                    iregHist.dilepton_m_peak.at(i).Write("dilepton_m_peak", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_fid.at(i).Write("dilepton_m_peak_fid", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_oof.at(i).Write("dilepton_m_peak_oof", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_5bins.at(i).Write("dilepton_m_peak_5bins", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_5bins_fid.at(i).Write("dilepton_m_peak_5bins_fid", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_5bins_oof.at(i).Write("dilepton_m_peak_5bins_oof", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_10bins.at(i).Write("dilepton_m_peak_10bins", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_10bins_fid.at(i).Write("dilepton_m_peak_10bins_fid", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_10bins_oof.at(i).Write("dilepton_m_peak_10bins_oof", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_15bins.at(i).Write("dilepton_m_peak_15bins", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_15bins_fid.at(i).Write("dilepton_m_peak_15bins_fid", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_15bins_oof.at(i).Write("dilepton_m_peak_15bins_oof", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_20bins.at(i).Write("dilepton_m_peak_20bins", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_20bins_fid.at(i).Write("dilepton_m_peak_20bins_fid", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_20bins_oof.at(i).Write("dilepton_m_peak_20bins_oof", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_25bins.at(i).Write("dilepton_m_peak_25bins", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_25bins_fid.at(i).Write("dilepton_m_peak_25bins_fid", TObject::kOverwrite);
                    iregHist.dilepton_m_peak_25bins_oof.at(i).Write("dilepton_m_peak_25bins_oof", TObject::kOverwrite);
                    iregHist.dilepton_deltaR.at(i).Write("dilepton_deltaR", TObject::kOverwrite);
                    iregHist.dilepton_deltaPhi.at(i).Write("dilepton_deltaPhi", TObject::kOverwrite);
                    iregHist.dilepton_pt.at(i).Write("dilepton_pt", TObject::kOverwrite);
                    iregHist.n_geq0.at(i).Write("n_geq0", TObject::kOverwrite);
                    iregHist.n_geq0_fid.at(i).Write("n_geq0_fid", TObject::kOverwrite);
                    iregHist.n_geq0_oof.at(i).Write("n_geq0_oof", TObject::kOverwrite);
                    iregHist.n_1bjet.at(i).Write("n_1bjet", TObject::kOverwrite);
                    iregHist.n_2bjet.at(i).Write("n_2bjet", TObject::kOverwrite);
                    iregHist.n_geq0_30.at(i).Write("n_geq0_30", TObject::kOverwrite);
                    iregHist.n_1bjet_30.at(i).Write("n_1bjet_30", TObject::kOverwrite);
                    iregHist.n_2bjet_30.at(i).Write("n_2bjet_30", TObject::kOverwrite);
                    iregHist.n_geq0_35.at(i).Write("n_geq0_35", TObject::kOverwrite);
                    iregHist.n_1bjet_35.at(i).Write("n_1bjet_35", TObject::kOverwrite);
                    iregHist.n_2bjet_35.at(i).Write("n_2bjet_35", TObject::kOverwrite);
                    iregHist.n_geq0_leadingElectron.at(i).Write("n_geq0_leadingElectron", TObject::kOverwrite);
                    iregHist.n_geq0_leadingMuon.at(i).Write("n_geq0_leadingMuon", TObject::kOverwrite);
                    iregHist.n_1bjet_leadingElectron.at(i).Write("n_1bjet_leadingElectron", TObject::kOverwrite);
                    iregHist.n_1bjet_leadingMuon.at(i).Write("n_1bjet_leadingMuon", TObject::kOverwrite);
                    iregHist.n_2bjet_leadingElectron.at(i).Write("n_2bjet_leadingElectron", TObject::kOverwrite);
                    iregHist.n_2bjet_leadingMuon.at(i).Write("n_2bjet_leadingMuon", TObject::kOverwrite);
                    iregHist.jet_Z_balance_pt_17_30.at(i).Write("jet_Z_balance_pt_17_30", TObject::kOverwrite);
                    iregHist.jet_Z_balance_pt_30_50.at(i).Write("jet_Z_balance_pt_30_50", TObject::kOverwrite);
                    iregHist.jet_Z_balance_pt_50_100.at(i).Write("jet_Z_balance_pt_50_100", TObject::kOverwrite);
                    iregHist.jet_Z_balance_pt_100_200.at(i).Write("jet_Z_balance_pt_100_200", TObject::kOverwrite);
                    iregHist.jet_Z_balance_pt_200_inf.at(i).Write("jet_Z_balance_pt_200_inf", TObject::kOverwrite);
                    iregHist.jet_Z_balance_jet_pt.at(i).Write("jet_Z_balance_jet_pt", TObject::kOverwrite);
                    iregHist.jet_Z_balance_Z_pt.at(i).Write("jet_Z_balance_Z_pt", TObject::kOverwrite);

                    // get special histos
                    std::unique_ptr<TH1D> ha = Common::GetHa(&iregHist.n_geq0.at(i));
                    std::unique_ptr<TH1D> hb = Common::GetHb(&iregHist.n_geq0.at(i), &iregHist.n_1bjet.at(i), &iregHist.n_2bjet.at(i));
                    std::unique_ptr<TH1D> hc = Common::GetHc(&iregHist.n_geq0.at(i), &iregHist.n_1bjet.at(i), &iregHist.n_2bjet.at(i));
                    ha->Write("ha", TObject::kOverwrite);
                    hb->Write("hb", TObject::kOverwrite);
                    hc->Write("hc", TObject::kOverwrite);
                    std::unique_ptr<TH1D> ha_30 = Common::GetHa(&iregHist.n_geq0_30.at(i));
                    std::unique_ptr<TH1D> hb_30 = Common::GetHb(&iregHist.n_geq0_30.at(i), &iregHist.n_1bjet_30.at(i), &iregHist.n_2bjet_30.at(i));
                    std::unique_ptr<TH1D> hc_30 = Common::GetHc(&iregHist.n_geq0_30.at(i), &iregHist.n_1bjet_30.at(i), &iregHist.n_2bjet_30.at(i));
                    ha_30->Write("ha_30", TObject::kOverwrite);
                    hb_30->Write("hb_30", TObject::kOverwrite);
                    hc_30->Write("hc_30", TObject::kOverwrite);
                    std::unique_ptr<TH1D> ha_35 = Common::GetHa(&iregHist.n_geq0_35.at(i));
                    std::unique_ptr<TH1D> hb_35 = Common::GetHb(&iregHist.n_geq0_35.at(i), &iregHist.n_1bjet_35.at(i), &iregHist.n_2bjet_35.at(i));
                    std::unique_ptr<TH1D> hc_35 = Common::GetHc(&iregHist.n_geq0_35.at(i), &iregHist.n_1bjet_35.at(i), &iregHist.n_2bjet_35.at(i));
                    ha_35->Write("ha_35", TObject::kOverwrite);
                    hb_35->Write("hb_35", TObject::kOverwrite);
                    hc_35->Write("hc_35", TObject::kOverwrite);

                    // get the MC stat uncertainty variations
                    // store only for the nominal
                    if (m_syst == "nominal" && i == 0) {
                        std::unique_ptr<TH1D> geq0_up = Common::GetShiftedHisto(&iregHist.n_geq0.at(i), true);
                        std::unique_ptr<TH1D> geq0_dw = Common::GetShiftedHisto(&iregHist.n_geq0.at(i), false);
                        std::unique_ptr<TH1D> bjet1_up = Common::GetShiftedHisto(&iregHist.n_1bjet.at(i), true);
                        std::unique_ptr<TH1D> bjet1_dw = Common::GetShiftedHisto(&iregHist.n_1bjet.at(i), false);
                        std::unique_ptr<TH1D> bjet2_up = Common::GetShiftedHisto(&iregHist.n_2bjet.at(i), true);
                        std::unique_ptr<TH1D> bjet2_dw = Common::GetShiftedHisto(&iregHist.n_2bjet.at(i), false);

                        // ha variations
                        std::unique_ptr<TH1D> ha_geq0_up = Common::GetHa(geq0_up.get());
                        std::unique_ptr<TH1D> ha_geq0_dw = Common::GetHa(geq0_dw.get());
                        ha_geq0_up->Write("ha_geq0_up", TObject::kOverwrite);
                        ha_geq0_dw->Write("ha_geq0_down", TObject::kOverwrite);

                        // hb variations
                        std::unique_ptr<TH1D> hb_geq0_up = Common::GetHb(geq0_up.get(), &iregHist.n_1bjet.at(i), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hb_geq0_dw = Common::GetHb(geq0_dw.get(), &iregHist.n_1bjet.at(i), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hb_1bjet_up = Common::GetHb(&iregHist.n_geq0.at(i), bjet1_up.get(), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hb_1bjet_dw = Common::GetHb(&iregHist.n_geq0.at(i), bjet1_dw.get(), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hb_2bjet_up = Common::GetHb(&iregHist.n_geq0.at(i), &iregHist.n_1bjet.at(i), bjet2_up.get());
                        std::unique_ptr<TH1D> hb_2bjet_dw = Common::GetHb(&iregHist.n_geq0.at(i), &iregHist.n_1bjet.at(i), bjet2_dw.get());
                        hb_geq0_up->Write("hb_geq0_up", TObject::kOverwrite);
                        hb_geq0_dw->Write("hb_geq0_down", TObject::kOverwrite);
                        hb_1bjet_up->Write("hb_1bjet_up", TObject::kOverwrite);
                        hb_1bjet_dw->Write("hb_1bjet_down", TObject::kOverwrite);
                        hb_2bjet_up->Write("hb_2bjet_up", TObject::kOverwrite);
                        hb_2bjet_dw->Write("hb_2bjet_down", TObject::kOverwrite);

                        // hc variations
                        std::unique_ptr<TH1D> hc_geq0_up = Common::GetHc(geq0_up.get(), &iregHist.n_1bjet.at(i), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hc_geq0_dw = Common::GetHc(geq0_dw.get(), &iregHist.n_1bjet.at(i), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hc_1bjet_up = Common::GetHc(&iregHist.n_geq0.at(i), bjet1_up.get(), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hc_1bjet_dw = Common::GetHc(&iregHist.n_geq0.at(i), bjet1_dw.get(), &iregHist.n_2bjet.at(i));
                        std::unique_ptr<TH1D> hc_2bjet_up = Common::GetHc(&iregHist.n_geq0.at(i), &iregHist.n_1bjet.at(i), bjet2_up.get());
                        std::unique_ptr<TH1D> hc_2bjet_dw = Common::GetHc(&iregHist.n_geq0.at(i), &iregHist.n_1bjet.at(i), bjet2_dw.get());
                        hc_geq0_up->Write("hc_geq0_up", TObject::kOverwrite);
                        hc_geq0_dw->Write("hc_geq0_down", TObject::kOverwrite);
                        hc_1bjet_up->Write("hc_1bjet_up", TObject::kOverwrite);
                        hc_1bjet_dw->Write("hc_1bjet_down", TObject::kOverwrite);
                        hc_2bjet_up->Write("hc_2bjet_up", TObject::kOverwrite);
                        hc_2bjet_dw->Write("hc_2bjet_down", TObject::kOverwrite);
                    }
                } else {
                    iregHist.lepton_pt.at(i).Write("lepton_pt", TObject::kOverwrite);
                    iregHist.lepton_eta.at(i).Write("lepton_eta", TObject::kOverwrite);
                    iregHist.lepton_phi.at(i).Write("lepton_phi", TObject::kOverwrite);
                    iregHist.lepton_e.at(i).Write("lepton_e", TObject::kOverwrite);
                    iregHist.mwt.at(i).Write("mwt", TObject::kOverwrite);
                    iregHist.reco_W_mass.at(i).Write("reco_W_mass", TObject::kOverwrite);
                    iregHist.reco_top_mass.at(i).Write("reco_top_mass", TObject::kOverwrite);
                    iregHist.chi2.at(i).Write("chi2", TObject::kOverwrite);
                }
                iregHist.met.at(i).Write("met", TObject::kOverwrite);
                iregHist.met_x.at(i).Write("met_x", TObject::kOverwrite);
                iregHist.met_y.at(i).Write("met_y", TObject::kOverwrite);
                iregHist.met_phi.at(i).Write("met_phi", TObject::kOverwrite);
                iregHist.met_sumet.at(i).Write("met_sumet", TObject::kOverwrite);
                iregHist.average_mu.at(i).Write("mu", TObject::kOverwrite);
                iregHist.actual_mu.at(i).Write("mu_actual", TObject::kOverwrite);
                ireg++;
            }
        }
    }
}

void ControlPlots::FillJetPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index) {

    /// HT
    double ht = std::accumulate(event.jet_pt->begin(), event.jet_pt->end(), 0);
    ht /= 1e3;

    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        if (event.jet_pt->size() > 0) {
            itr->second.at(index).jet1_pt.at(i).Fill(event.jet_pt->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).jet1_eta.at(i).Fill(event.jet_eta->at(0), m_weights.at(i));
            itr->second.at(index).jet1_phi.at(i).Fill(event.jet_phi->at(0), m_weights.at(i));
            itr->second.at(index).jet1_e.at(i).Fill(event.jet_e->at(0)/1e3, m_weights.at(i));
        }
        if (event.jet_pt->size() > 1) {
            itr->second.at(index).jet2_pt.at(i).Fill(event.jet_pt->at(1)/1e3, m_weights.at(i));
            itr->second.at(index).jet2_eta.at(i).Fill(event.jet_eta->at(1), m_weights.at(i));
            itr->second.at(index).jet2_phi.at(i).Fill(event.jet_phi->at(1), m_weights.at(i));
            itr->second.at(index).jet2_e.at(i).Fill(event.jet_e->at(1)/1e3, m_weights.at(i));
        }
        if (event.jet_pt->size() > 2) {
            itr->second.at(index).jet3_pt.at(i).Fill(event.jet_pt->at(2)/1e3, m_weights.at(i));
            itr->second.at(index).jet3_eta.at(i).Fill(event.jet_eta->at(2), m_weights.at(i));
            itr->second.at(index).jet3_phi.at(i).Fill(event.jet_phi->at(2), m_weights.at(i));
            itr->second.at(index).jet3_e.at(i).Fill(event.jet_e->at(2)/1e3, m_weights.at(i));
        }
        if (event.jet_pt->size() > 3) {
            itr->second.at(index).jet4_pt.at(i).Fill(event.jet_pt->at(3)/1e3, m_weights.at(i));
            itr->second.at(index).jet4_eta.at(i).Fill(event.jet_eta->at(3), m_weights.at(i));
            itr->second.at(index).jet4_phi.at(i).Fill(event.jet_phi->at(3), m_weights.at(i));
            itr->second.at(index).jet4_e.at(i).Fill(event.jet_e->at(3)/1e3, m_weights.at(i));
        }
        if (event.jet_pt->size() > 4) {
            itr->second.at(index).jet5_pt.at(i).Fill(event.jet_pt->at(4)/1e3, m_weights.at(i));
            itr->second.at(index).jet5_eta.at(i).Fill(event.jet_eta->at(4), m_weights.at(i));
            itr->second.at(index).jet5_phi.at(i).Fill(event.jet_phi->at(4), m_weights.at(i));
            itr->second.at(index).jet5_e.at(i).Fill(event.jet_e->at(4)/1e3, m_weights.at(i));
        }
        if (event.jet_pt->size() > 5) {
            itr->second.at(index).jet6_pt.at(i).Fill(event.jet_pt->at(5)/1e3, m_weights.at(i));
            itr->second.at(index).jet6_eta.at(i).Fill(event.jet_eta->at(5), m_weights.at(i));
            itr->second.at(index).jet6_phi.at(i).Fill(event.jet_phi->at(5), m_weights.at(i));
            itr->second.at(index).jet6_e.at(i).Fill(event.jet_e->at(5)/1e3, m_weights.at(i));
        }
        itr->second.at(index).jet_n.at(i).Fill(event.jet_pt->size(), m_weights.at(i));

        itr->second.at(index).HT.at(i).Fill(ht, m_weights.at(i));
    }
}

void ControlPlots::FillTagPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index) {
    /// btagging
    std::size_t n_bjet_77(0);

    for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
        if (event.jet_isbtagged_DL1dv01_77->at(ijet)) ++n_bjet_77;
    }

    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
            itr->second.at(index).jet_tagbin.at(i).Fill(event.jet_tagWeightBin_DL1dv01_Continuous->at(ijet), m_weights.at(i));
            if (ijet == 0) {
                itr->second.at(index).jet1_tagbin.at(i).Fill(event.jet_tagWeightBin_DL1dv01_Continuous->at(ijet), m_weights.at(i));
            } else if (ijet == 1) {
                itr->second.at(index).jet2_tagbin.at(i).Fill(event.jet_tagWeightBin_DL1dv01_Continuous->at(ijet), m_weights.at(i));
            } else if (ijet == 2) {
                itr->second.at(index).jet3_tagbin.at(i).Fill(event.jet_tagWeightBin_DL1dv01_Continuous->at(ijet), m_weights.at(i));
            } else if (ijet == 3) {
                itr->second.at(index).jet4_tagbin.at(i).Fill(event.jet_tagWeightBin_DL1dv01_Continuous->at(ijet), m_weights.at(i));
            }
            if (event.jet_isbtagged_DL1dv01_77->at(ijet)) {
                itr->second.at(index).bjet_pt_77.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                itr->second.at(index).bjet_eta_77.at(i).Fill(event.jet_eta->at(ijet), m_weights.at(i));
                itr->second.at(index).bjet_phi_77.at(i).Fill(event.jet_phi->at(ijet), m_weights.at(i));
                itr->second.at(index).bjet_e_77.at(i).Fill(event.jet_e->at(ijet)/1e3, m_weights.at(i));
            }
        }
        itr->second.at(index).bjet_n_77.at(i).Fill(n_bjet_77, m_weights.at(i));
    }
}

void ControlPlots::FillOtherPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index) {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        itr->second.at(index).met.at(i).Fill(event.met_met/1e3, m_weights.at(i));
        itr->second.at(index).met_x.at(i).Fill(event.met_met/1e3*std::cos(event.met_phi), m_weights.at(i));
        itr->second.at(index).met_y.at(i).Fill(event.met_met/1e3*std::sin(event.met_phi), m_weights.at(i));
        itr->second.at(index).met_phi.at(i).Fill(event.met_phi, m_weights.at(i));
        itr->second.at(index).met_sumet.at(i).Fill(event.met_sumet/1e3, m_weights.at(i));

        if (m_isMC) {
            itr->second.at(index).average_mu.at(i).Fill(event.mu, m_weights.at(i));
            itr->second.at(index).actual_mu.at(i).Fill(event.mu_actual, m_weights.at(i));
        } else {
            itr->second.at(index).average_mu.at(i).Fill(event.mu/1.03, m_weights.at(i));
            itr->second.at(index).actual_mu.at(i).Fill(event.mu_actual/1.03, m_weights.at(i));
        }
    }
}

void ControlPlots::FillDileptonPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index, const bool isFiducial) {

    const std::size_t nTags = Common::GetNbtags_77(event);

    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        itr->second.at(index).n_geq0.at(i).Fill(1., m_weights.at(i));
        if (isFiducial) {
            itr->second.at(index).n_geq0_fid.at(i).Fill(1., m_weights.at(i));
        } else {
            itr->second.at(index).n_geq0_oof.at(i).Fill(1., m_weights.at(i));
        }
        if (nTags == 1) itr->second.at(index).n_1bjet.at(i).Fill(1., m_weights.at(i));
        if (nTags == 2) itr->second.at(index).n_2bjet.at(i).Fill(1., m_weights.at(i));
        if (itr->first == Common::LEPTONTYPE::EMU) {
            if (event.el_pt->at(0) > event.mu_pt->at(0)) {
                itr->second.at(index).n_geq0_leadingElectron.at(i).Fill(1., m_weights.at(i));
                if (nTags == 1) itr->second.at(index).n_1bjet_leadingElectron.at(i).Fill(1., m_weights.at(i));
                if (nTags == 2) itr->second.at(index).n_2bjet_leadingElectron.at(i).Fill(1., m_weights.at(i));
            } else {
                itr->second.at(index).n_geq0_leadingMuon.at(i).Fill(1., m_weights.at(i));
                if (nTags == 1) itr->second.at(index).n_1bjet_leadingMuon.at(i).Fill(1., m_weights.at(i));
                if (nTags == 2) itr->second.at(index).n_2bjet_leadingMuon.at(i).Fill(1., m_weights.at(i));
            }
        }

        if (!Common::HasLeptonsBelowX(event, 30000)) {
            itr->second.at(index).n_geq0_30.at(i).Fill(1., m_weights.at(i));
            if (nTags == 1) itr->second.at(index).n_1bjet_30.at(i).Fill(1., m_weights.at(i));
            if (nTags == 2) itr->second.at(index).n_2bjet_30.at(i).Fill(1., m_weights.at(i));
        }
        if (!Common::HasLeptonsBelowX(event, 35000)) {
            itr->second.at(index).n_geq0_35.at(i).Fill(1., m_weights.at(i));
            if (nTags == 1) itr->second.at(index).n_1bjet_35.at(i).Fill(1., m_weights.at(i));
            if (nTags == 2) itr->second.at(index).n_2bjet_35.at(i).Fill(1., m_weights.at(i));
        }

        if (m_lepton_type == Common::LEPTONTYPE::EE) {
            itr->second.at(index).lepton1_pt.at(i).Fill(event.el_pt->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).lepton1_eta.at(i).Fill(event.el_eta->at(0), m_weights.at(i));
            itr->second.at(index).lepton1_phi.at(i).Fill(event.el_phi->at(0), m_weights.at(i));
            itr->second.at(index).lepton1_e.at(i).Fill(event.el_e->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).lepton2_pt.at(i).Fill(event.el_pt->at(1)/1e3, m_weights.at(i));
            itr->second.at(index).lepton2_eta.at(i).Fill(event.el_eta->at(1), m_weights.at(i));
            itr->second.at(index).lepton2_phi.at(i).Fill(event.el_phi->at(1), m_weights.at(i));
            itr->second.at(index).lepton2_e.at(i).Fill(event.el_e->at(1)/1e3, m_weights.at(i));
        } else if (m_lepton_type == Common::LEPTONTYPE::MUMU) {
            itr->second.at(index).lepton1_pt.at(i).Fill(event.mu_pt->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).lepton1_eta.at(i).Fill(event.mu_eta->at(0), m_weights.at(i));
            itr->second.at(index).lepton1_phi.at(i).Fill(event.mu_phi->at(0), m_weights.at(i));
            itr->second.at(index).lepton1_e.at(i).Fill(event.mu_e->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).lepton2_pt.at(i).Fill(event.mu_pt->at(1)/1e3, m_weights.at(i));
            itr->second.at(index).lepton2_eta.at(i).Fill(event.mu_eta->at(1), m_weights.at(i));
            itr->second.at(index).lepton2_phi.at(i).Fill(event.mu_phi->at(1), m_weights.at(i));
            itr->second.at(index).lepton2_e.at(i).Fill(event.mu_e->at(1)/1e3, m_weights.at(i));
        } else if (m_lepton_type == Common::LEPTONTYPE::EMU) {
            if (event.el_pt->at(0) > event.mu_pt->at(0)) {
                itr->second.at(index).lepton1_pt.at(i).Fill(event.el_pt->at(0)/1e3, m_weights.at(i));
                itr->second.at(index).lepton1_eta.at(i).Fill(event.el_eta->at(0), m_weights.at(i));
                itr->second.at(index).lepton1_phi.at(i).Fill(event.el_phi->at(0), m_weights.at(i));
                itr->second.at(index).lepton1_e.at(i).Fill(event.el_e->at(0)/1e3, m_weights.at(i));
                itr->second.at(index).lepton2_pt.at(i).Fill(event.mu_pt->at(0)/1e3, m_weights.at(i));
                itr->second.at(index).lepton2_eta.at(i).Fill(event.mu_eta->at(0), m_weights.at(i));
                itr->second.at(index).lepton2_phi.at(i).Fill(event.mu_phi->at(0), m_weights.at(i));
                itr->second.at(index).lepton2_e.at(i).Fill(event.mu_e->at(0)/1e3, m_weights.at(i));
            } else {
                itr->second.at(index).lepton1_pt.at(i).Fill(event.mu_pt->at(0)/1e3, m_weights.at(i));
                itr->second.at(index).lepton1_eta.at(i).Fill(event.mu_eta->at(0), m_weights.at(i));
                itr->second.at(index).lepton1_phi.at(i).Fill(event.mu_phi->at(0), m_weights.at(i));
                itr->second.at(index).lepton1_e.at(i).Fill(event.mu_e->at(0)/1e3, m_weights.at(i));
                itr->second.at(index).lepton2_pt.at(i).Fill(event.el_pt->at(0)/1e3, m_weights.at(i));
                itr->second.at(index).lepton2_eta.at(i).Fill(event.el_eta->at(0), m_weights.at(i));
                itr->second.at(index).lepton2_phi.at(i).Fill(event.el_phi->at(0), m_weights.at(i));
                itr->second.at(index).lepton2_e.at(i).Fill(event.el_e->at(0)/1e3, m_weights.at(i));
            }
        }
        itr->second.at(index).dilepton_m.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        itr->second.at(index).dilepton_m_peak.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        itr->second.at(index).dilepton_m_peak_5bins.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        itr->second.at(index).dilepton_m_peak_10bins.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        itr->second.at(index).dilepton_m_peak_15bins.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        itr->second.at(index).dilepton_m_peak_20bins.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        itr->second.at(index).dilepton_m_peak_25bins.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        if (isFiducial) {
            itr->second.at(index).dilepton_m_peak_fid.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_5bins_fid.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_10bins_fid.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_15bins_fid.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_20bins_fid.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_25bins_fid.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        } else {
            itr->second.at(index).dilepton_m_peak_oof.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_5bins_oof.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_10bins_oof.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_15bins_oof.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_20bins_oof.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
            itr->second.at(index).dilepton_m_peak_25bins_oof.at(i).Fill(event.dileptonMass/1e3, m_weights.at(i));
        }
        itr->second.at(index).dilepton_deltaR.at(i).Fill(event.dileptonDeltaR, m_weights.at(i));
        itr->second.at(index).dilepton_deltaPhi.at(i).Fill(event.dileptonDeltaPhi, m_weights.at(i));

    }
}

void ControlPlots::FillSingleLepPlots(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index) {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        if (itr->first == Common::LEPTONTYPE::EL) {
            itr->second.at(index).lepton_pt.at(i).Fill(event.el_pt->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).lepton_eta.at(i).Fill(event.el_eta->at(0), m_weights.at(i));
            itr->second.at(index).lepton_phi.at(i).Fill(event.el_phi->at(0), m_weights.at(i));
            itr->second.at(index).lepton_e.at(i).Fill(event.el_e->at(0)/1e3, m_weights.at(i));
        } else {
            itr->second.at(index).lepton_pt.at(i).Fill(event.mu_pt->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).lepton_eta.at(i).Fill(event.mu_eta->at(0), m_weights.at(i));
            itr->second.at(index).lepton_phi.at(i).Fill(event.mu_phi->at(0), m_weights.at(i));
            itr->second.at(index).lepton_e.at(i).Fill(event.mu_e->at(0)/1e3, m_weights.at(i));
        }
        itr->second.at(index).mwt.at(i).Fill(Common::MWT(event, itr->first), m_weights.at(i));

        if (event.hadBjetIndex > 100) return;
        if (event.lightJetIndex1 > 100) return;
        if (event.lightJetIndex2 > 100) return;

        ROOT::Math::PtEtaPhiEVector b(event.jet_pt->at(event.hadBjetIndex)/1e3,
                                      event.jet_eta->at(event.hadBjetIndex),
                                      event.jet_phi->at(event.hadBjetIndex),
                                      event.jet_e->at(event.hadBjetIndex)/1e3);

        ROOT::Math::PtEtaPhiEVector l1(event.jet_pt->at(event.lightJetIndex1)/1e3,
                                       event.jet_eta->at(event.lightJetIndex1),
                                       event.jet_phi->at(event.lightJetIndex1),
                                       event.jet_e->at(event.lightJetIndex1)/1e3);

        ROOT::Math::PtEtaPhiEVector l2(event.jet_pt->at(event.lightJetIndex2)/1e3,
                                       event.jet_eta->at(event.lightJetIndex2),
                                       event.jet_phi->at(event.lightJetIndex2),
                                       event.jet_e->at(event.lightJetIndex2)/1e3);

        const double mW = (l1 + l2).M();
        const double mT = (l1 + l2 + b).M();
        itr->second.at(index).reco_W_mass.at(i).Fill(mW, m_weights.at(i));
        itr->second.at(index).reco_top_mass.at(i).Fill(mT, m_weights.at(i));
        itr->second.at(index).chi2.at(i).Fill(event.chi2, m_weights.at(i));
    }
}

void ControlPlots::FillDileptonPlotsExtra(const Event& event, std::vector<std::pair<Common::LEPTONTYPE, std::vector<Histos> > >::iterator itr, const std::size_t index) {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        for (std::size_t iele = 0; iele < event.el_pt->size(); iele++) {
            itr->second.at(index).electron_pt.at(i).Fill(event.el_pt->at(iele)/1e3, m_weights.at(i));
            itr->second.at(index).electron_eta.at(i).Fill(event.el_eta->at(iele), m_weights.at(i));
            itr->second.at(index).electron_phi.at(i).Fill(event.el_phi->at(iele), m_weights.at(i));
            itr->second.at(index).electron_e.at(i).Fill(event.el_e->at(iele)/1e3, m_weights.at(i));
        }
        for (std::size_t imu = 0; imu < event.mu_pt->size(); imu++) {
            itr->second.at(index).muon_pt.at(i).Fill(event.mu_pt->at(imu)/1e3, m_weights.at(i));
            itr->second.at(index).muon_eta.at(i).Fill(event.mu_eta->at(imu), m_weights.at(i));
            itr->second.at(index).muon_phi.at(i).Fill(event.mu_phi->at(imu), m_weights.at(i));
            itr->second.at(index).muon_e.at(i).Fill(event.mu_e->at(imu)/1e3, m_weights.at(i));
        }

        double pt1;
        double pt2;
        double eta1;
        double eta2;
        double phi1;
        double phi2;
        double e1;
        double e2;

        if (m_lepton_type == Common::LEPTONTYPE::EMU) {
            pt1 = event.el_pt->at(0);
            pt2 = event.mu_pt->at(0);
            eta1 = event.el_eta->at(0);
            eta2 = event.mu_eta->at(0);
            phi1 = event.el_phi->at(0);
            phi2 = event.mu_phi->at(0);
            e1 = event.el_e->at(0);
            e2 = event.mu_e->at(0);
        } else { 
            pt1 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_pt->at(0) : event.mu_pt->at(0);
            pt2 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_pt->at(1) : event.mu_pt->at(1);
            eta1 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_eta->at(0) : event.mu_eta->at(0);
            eta2 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_eta->at(1) : event.mu_eta->at(1);
            phi1 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_phi->at(0) : event.mu_phi->at(0);
            phi2 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_phi->at(1) : event.mu_phi->at(1);
            e1 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_e->at(0) : event.mu_e->at(0);
            e2 = (m_lepton_type == Common::LEPTONTYPE::EE) ? event.el_e->at(1) : event.mu_e->at(1);
        }
        ROOT::Math::PtEtaPhiEVector l1(pt1/1e3, eta1, phi1, e1/1e3);
        ROOT::Math::PtEtaPhiEVector l2(pt2/1e3, eta2, phi2, e2/1e3);
        const double pt = (l1 + l2).Pt();
        itr->second.at(index).dilepton_pt.at(i).Fill(pt, m_weights.at(i));
        // add jet response vs Z 
        // require exactly one jet
        // require delta Phi > 1
        if (m_lepton_type != Common::LEPTONTYPE::EMU) {
            if (event.jet_pt->size() != 1) continue;
            auto DeltaPhi = [](const double p1, const double p2) {
                double result = p1 - p2;
                if (result > M_PI) {
                    result -= 2.0*M_PI;
                } else if (result <= -M_PI) {
                    result += 2.0*M_PI;
                }
                return result;
            };
            const double dPhi = DeltaPhi(event.jet_phi->at(0), (l1+l2).Phi()); 
            if (std::abs(dPhi) < 2.7) continue;
            if (pt < 17) continue;
            itr->second.at(index).jet_Z_balance_jet_pt.at(i).Fill(event.jet_pt->at(0)/1e3, m_weights.at(i));
            itr->second.at(index).jet_Z_balance_Z_pt.at(i).Fill(pt, m_weights.at(i));
            if (pt < 30) {
                itr->second.at(index).jet_Z_balance_pt_17_30.at(i).Fill(event.jet_pt->at(0)/1e3 / pt, m_weights.at(i));
            } else if (pt < 50) {
                itr->second.at(index).jet_Z_balance_pt_30_50.at(i).Fill(event.jet_pt->at(0)/1e3 / pt, m_weights.at(i));
            } else if (pt < 100) {
                itr->second.at(index).jet_Z_balance_pt_50_100.at(i).Fill(event.jet_pt->at(0)/1e3 / pt, m_weights.at(i));
            } else if (pt < 200) {
                itr->second.at(index).jet_Z_balance_pt_100_200.at(i).Fill(event.jet_pt->at(0)/1e3 / pt, m_weights.at(i));
            } else {
                itr->second.at(index).jet_Z_balance_pt_200_inf.at(i).Fill(event.jet_pt->at(0)/1e3 / pt, m_weights.at(i));
            }
        }
    }
}
