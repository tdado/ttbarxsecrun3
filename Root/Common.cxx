#include "TTbarXsec/Common.h"
#include "TTbarXsec/Event.h"

#include "TF1.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TH2F.h"
#include "TString.h"
#include "Math/Vector4D.h"

#include <algorithm>
#include <iostream>

void Common::AddOverflow(TH1D* hist) {
    const int nbins = hist->GetNbinsX();

    hist->SetBinContent(1, hist->GetBinContent(0) + hist->GetBinContent(1) );
    hist->SetBinContent(nbins, hist->GetBinContent(nbins) + hist->GetBinContent(nbins+1) );
    hist->SetBinContent(0, 0 );
    hist->SetBinContent(nbins+1, 0 );
    
    hist->SetBinError(1, std::hypot(hist->GetBinError(0), hist->GetBinError(1)));
    hist->SetBinError(nbins, std::hypot(hist->GetBinError(nbins), hist->GetBinError(nbins+1)));
    hist->SetBinError(0, 0);
    hist->SetBinError(nbins+1, 0);
}

void Common::AddOverflow(TH2D *hist) {
	int nbinsX = hist->GetNbinsX();
	int nbinsY = hist->GetNbinsY();

	for (int i = 1; i < nbinsX+1; i++){
		hist->SetBinContent(i,1, hist->GetBinContent(i,0) + hist->GetBinContent(i,1) );
		hist->SetBinContent(i,nbinsY, hist->GetBinContent(i,nbinsY) + hist->GetBinContent(i,nbinsY+1) );
		hist->SetBinContent(i,0, 0 );
		hist->SetBinContent(i,nbinsY+1, 0 );
	
		hist->SetBinError(i,1, std::hypot(hist->GetBinError(i,0), hist->GetBinError(i,1)));
		hist->SetBinError(i,nbinsY, std::hypot(hist->GetBinError(i,nbinsY), hist->GetBinError(i,nbinsY+1)));
		hist->SetBinError(i,0, 0);
		hist->SetBinError(i,nbinsY+1, 0);
	}

	for (int i = 1; i < nbinsY+1; i++){
		hist->SetBinContent(1,i, hist->GetBinContent(0,i) + hist->GetBinContent(1,i) );
		hist->SetBinContent(nbinsX,i, hist->GetBinContent(nbinsX,i) + hist->GetBinContent(nbinsX+1,i) );
		hist->SetBinContent(0,i, 0 );
		hist->SetBinContent(nbinsX+1,i, 0 );
	
		hist->SetBinError(1,i, std::hypot(hist->GetBinError(0,i), hist->GetBinError(1,i)));
		hist->SetBinError(nbinsX,i, std::hypot(hist->GetBinError(nbinsX,i), hist->GetBinError(nbinsX+1,i)));
		hist->SetBinError(0,1, 0);
		hist->SetBinError(nbinsX+1,i, 0);
	}
}

std::string Common::RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove) {
    std::string result = str;
    result.erase(std::remove(result.begin(), result.end(), '_'), result.end());

    for (const auto& i : remove) {
        auto itr = result.find(i.c_str());       
        if (itr == std::string::npos) continue;

        const auto size = i.size();
        result.erase(itr, size);
    }

    return result;
}
    
double Common::GetMax(const TH1D* const h1, const TH1D* const h2) {
    const double max1 = h1->GetMaximum();
    const double max2 = h2->GetMaximum();

    return std::max(max1, max2);
}

bool Common::EventHasLeptonFakes(const Event& event) {
    for (std::size_t i = 0; i < event.el_pt->size(); i++){  // Electron loop
        if (event.el_true_type->at(i) != 2) return true;
        if (!event.el_true_isPrompt->at(i)) return true;
    }
    
    for (std::size_t i = 0; i < event.mu_pt->size(); i++){  // Muon loop
        if (event.mu_true_type->at(i) != 6) return true;
        if (!event.mu_true_isPrompt->at(i)) return true;
    }
    return false;
}
    
std::string Common::TypeToString(const Common::LEPTONTYPE& type) {
    if (type == Common::LEPTONTYPE::EL) {
        return "el";
    } else if (type == Common::LEPTONTYPE::MU) {
        return "mu";
    } else if (type == Common::LEPTONTYPE::EE) {
        return "ee";
    } else if (type == Common::LEPTONTYPE::MUMU) {
        return "mumu";
    } else if (type == Common::LEPTONTYPE::EMU) {
        return "emu";
    }

    return "UNKNOWN";
}

std::pair<TLorentzVector, TLorentzVector> Common::GetLeptonsPtOrdered(const Event& event,
                                                                      const LEPTONTYPE& type) {

    TLorentzVector lep1{};
    TLorentzVector lep2{};

    if (type == Common::LEPTONTYPE::EE) {
        lep1.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                          event.el_eta->at(0),
                          event.el_phi->at(0),
                          event.el_e->at(0)/1e3);
        lep2.SetPtEtaPhiE(event.el_pt->at(1)/1e3,
                          event.el_eta->at(1),
                          event.el_phi->at(1),
                          event.el_e->at(1)/1e3);
    } else if (type == Common::LEPTONTYPE::MUMU) {
        lep1.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                          event.mu_eta->at(0),
                          event.mu_phi->at(0),
                          event.mu_e->at(0)/1e3);
        lep2.SetPtEtaPhiE(event.mu_pt->at(1)/1e3,
                          event.mu_eta->at(1),
                          event.mu_phi->at(1),
                          event.mu_e->at(1)/1e3);
    } else if (type == Common::LEPTONTYPE::EMU) {
        if (event.el_pt->at(0) > event.mu_pt->at(0)) {
            lep1.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                              event.el_eta->at(0),
                              event.el_phi->at(0),
                              event.el_e->at(0)/1e3);
            lep2.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                              event.mu_eta->at(0),
                              event.mu_phi->at(0),
                              event.mu_e->at(0)/1e3);
        } else {
            lep1.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                              event.mu_eta->at(0),
                              event.mu_phi->at(0),
                              event.mu_e->at(0)/1e3);
            lep2.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                              event.el_eta->at(0),
                              event.el_phi->at(0),
                              event.el_e->at(0)/1e3);
        }
    }
    return std::make_pair(std::move(lep1),std::move(lep2));
}

std::pair<TLorentzVector, TLorentzVector> Common::GetLeptonsChargeOrdered(const Event& event,
                                                                          const Common::LEPTONTYPE& type) {
    TLorentzVector lep1, lep2;

    if (type == Common::LEPTONTYPE::EE) {
        if (event.el_charge->at(0) > 0) {
            lep1.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                              event.el_eta->at(0),
                              event.el_phi->at(0),
                              event.el_e->at(0)/1e3);
            
            lep2.SetPtEtaPhiE(event.el_pt->at(1)/1e3,
                              event.el_eta->at(1),
                              event.el_phi->at(1),
                              event.el_e->at(1)/1e3);
        } else {
            lep2.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                              event.el_eta->at(0),
                              event.el_phi->at(0),
                              event.el_e->at(0)/1e3);
            
            lep1.SetPtEtaPhiE(event.el_pt->at(1)/1e3,
                              event.el_eta->at(1),
                              event.el_phi->at(1),
                              event.el_e->at(1)/1e3);
        }
    } else if (type == Common::LEPTONTYPE::MUMU) {
        if (event.mu_charge->at(0) > 0) {
            lep1.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                              event.mu_eta->at(0),
                              event.mu_phi->at(0),
                              event.mu_e->at(0)/1e3);
            
            lep2.SetPtEtaPhiE(event.mu_pt->at(1)/1e3,
                              event.mu_eta->at(1),
                              event.mu_phi->at(1),
                              event.mu_e->at(1)/1e3);
        } else {
            lep2.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                              event.mu_eta->at(0),
                              event.mu_phi->at(0),
                              event.mu_e->at(0)/1e3);
            
            lep1.SetPtEtaPhiE(event.mu_pt->at(1)/1e3,
                              event.mu_eta->at(1),
                              event.mu_phi->at(1),
                              event.mu_e->at(1)/1e3);
        }
    } else if (type == Common::LEPTONTYPE::EMU) {
        if (event.el_charge->at(0) > 0) {
            lep1.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                              event.el_eta->at(0),
                              event.el_phi->at(0),
                              event.el_e->at(0)/1e3);

            lep2.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                              event.mu_eta->at(0),
                              event.mu_phi->at(0),
                              event.mu_e->at(0)/1e3);
        } else {
            lep2.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                              event.el_eta->at(0),
                              event.el_phi->at(0),
                              event.el_e->at(0)/1e3);

            lep1.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                              event.mu_eta->at(0),
                              event.mu_phi->at(0),
                              event.mu_e->at(0)/1e3);
        }
    }

    std::pair<TLorentzVector, TLorentzVector> result = std::make_pair(lep1, lep2);
    return result;
}
    
bool Common::JetsArePaired(const TLorentzVector& v1, const TLorentzVector& v2) {
    if (v1.DeltaR(v2) < 0.3) return true;

    return false;
}

Common::LEPTONTYPE Common::FindLeptonType(const Event& event) {
    if (event.ee_2022) return Common::LEPTONTYPE::EE;
    else if (event.mumu_2022) return Common::LEPTONTYPE::MUMU;
    else if (event.emu_2022) return Common::LEPTONTYPE::EMU;
    else if (event.ejets_2022) return Common::LEPTONTYPE::EL;
    else if (event.mujets_2022) return Common::LEPTONTYPE::MU;

    std::cerr << "Common::FindLeptonType: No lepton type found!\n";
    exit(EXIT_FAILURE);
}

Common::REGION Common::FindRegion(const Event& event) {
    auto lepType = Common::FindLeptonType(event);

    bool ss{true};

    if (lepType == Common::LEPTONTYPE::EE) {
        if (event.el_charge->at(0) * event.el_charge->at(1) < 0) ss = false; 
    } else if (lepType == Common::LEPTONTYPE::MUMU) {
        if (event.mu_charge->at(0) * event.mu_charge->at(1) < 0) ss = false; 
    } else if (lepType == Common::LEPTONTYPE::EMU) {
        if (event.el_charge->at(0) * event.mu_charge->at(0) < 0) ss = false; 
    }

    if (lepType == Common::LEPTONTYPE::EMU) {
        if (ss) return Common::REGION::SAMESIGN;
        else    return Common::REGION::OPPOSITESIGN;
    }

    const double mass = event.dileptonMass/1e3;
    static const double Zmass = 91.;
    static const double massWindow = 25.;
    const bool isZlike = (mass > (Zmass - massWindow)) && (mass < (Zmass + massWindow));

    if (ss && isZlike) return Common::REGION::SAMESIGNZMASS; 
    if (ss && !isZlike) return Common::REGION::SAMESIGNZVETO; 
    if (!ss && isZlike) return Common::REGION::OPPOSITESIGNZMASS; 
    if (!ss && !isZlike) return Common::REGION::OPPOSITESIGNZVETO; 

    std::cerr << "Common::FindRegion: Wrong region\n";
    exit(EXIT_FAILURE);
}

std::string Common::RegionToString(const Common::REGION reg) {
    if (reg == Common::REGION::SAMESIGNZMASS) return "SS_Zmass";
    else if (reg == Common::REGION::SAMESIGNZVETO) return "SS_Zveto";
    else if (reg == Common::REGION::OPPOSITESIGNZMASS) return "OS_Zmass";
    else if (reg == Common::REGION::OPPOSITESIGNZVETO) return "OS_Zveto";
    else if (reg == Common::REGION::OPPOSITESIGN) return "OS";
    else if (reg == Common::REGION::OPPOSITESIGN1B) return "OS_1b";
    else if (reg == Common::REGION::SAMESIGN) return "SS";
    else if (reg == Common::REGION::INCLUSIVE) return "inclusive";

    std::cerr << "Common::RegionToString: Unknown region\n";
    exit(EXIT_FAILURE);
}

    
std::vector<std::string> Common::GetRegionString(const Common::CHANNEL channel, const Common::LEPTONTYPE type) {
    static const std::vector<Common::REGION> regions = {Common::REGION::SAMESIGNZMASS, Common::REGION::SAMESIGNZVETO,
                                                        Common::REGION::OPPOSITESIGNZMASS, Common::REGION::OPPOSITESIGNZVETO};

    std::vector<std::string> result;
    if (channel == Common::CHANNEL::SINGLELEPTON) {
        result.emplace_back(Common::RegionToString(Common::REGION::INCLUSIVE));
        return result;
    }

    if (type == Common::LEPTONTYPE::EMU) {
        result.emplace_back(Common::RegionToString(Common::REGION::SAMESIGN));
        result.emplace_back(Common::RegionToString(Common::REGION::OPPOSITESIGN));
        result.emplace_back(Common::RegionToString(Common::REGION::OPPOSITESIGN1B));
        return result;
    }
    for (const auto& ireg : regions) {
        result.emplace_back(Common::RegionToString(ireg));
    }

    return result;
}

double Common::MWT(const Event& event, const Common::LEPTONTYPE chan) {
    if (chan != Common::LEPTONTYPE::EL && chan != Common::LEPTONTYPE::MU) {
        std::cerr << "Common::MWT: Calculation valid only for single lepton channels!\n";
        return -1;
    }

    const double lepPt = (chan == Common::LEPTONTYPE::EL) ? event.el_pt->at(0)/1e3 : event.mu_pt->at(0)/1e3;
    const double lepPhi = (chan == Common::LEPTONTYPE::EL) ? event.el_phi->at(0) : event.mu_phi->at(0);

    return std::sqrt(2. * lepPt * event.met_met/1e3 * (1 - std::cos(lepPhi - event.met_phi)) );
}

std::unique_ptr<TH1D> Common::GetHa(const TH1D* geq0) {
    std::unique_ptr<TH1D> result(static_cast<TH1D*>(geq0->Clone()));
    result->SetDirectory(nullptr);

    result->Scale(2.);

    for (int ibin = 1; ibin <= result->GetNbinsX(); ++ibin) {
        result->SetBinError(ibin, 0.);
    }

    return result;
}
    
std::unique_ptr<TH1D> Common::GetHb(const TH1D* geq0, const TH1D* bjet1, const TH1D* bjet2) {
    std::unique_ptr<TH1D> result(static_cast<TH1D*>(geq0->Clone()));
    result->SetDirectory(nullptr);
    std::unique_ptr<TH1D> b1(static_cast<TH1D*>(bjet1->Clone()));
    b1->SetDirectory(nullptr);
    std::unique_ptr<TH1D> b2(static_cast<TH1D*>(bjet2->Clone()));
    b2->SetDirectory(nullptr);

    // N_{geq >= 0} ^2
    result->Multiply(result.get());

    // N_{geq >= 0} ^2 * N_2
    result->Multiply(b2.get());

    // 2* N_2
    b2->Scale(2.);

    // N_1 + 2* N_2
    b1->Add(b2.get());

    // (N_1 + 2* N_2)^2
    b1->Multiply(b1.get());

    // N_{geq >= 0} ^2 * N_2 / (N_1 + 2* N_2)^2
    result->Divide(b1.get());

    // 8* N_{geq >= 0} ^2 * N_2 / (N_1 + 2* N_2)^2
    result->Scale(8.);

    for (int ibin = 1; ibin <= result->GetNbinsX(); ++ibin) {
        result->SetBinError(ibin, 0.);
    }

    return result;
}

std::unique_ptr<TH1D> Common::GetHc(const TH1D* geq0, const TH1D* bjet1, const TH1D* bjet2) {
    std::unique_ptr<TH1D> result(static_cast<TH1D*>(geq0->Clone()));
    result->SetDirectory(nullptr);
    std::unique_ptr<TH1D> b1(static_cast<TH1D*>(bjet1->Clone()));
    b1->SetDirectory(nullptr);
    std::unique_ptr<TH1D> b2(static_cast<TH1D*>(bjet2->Clone()));
    b2->SetDirectory(nullptr);

    // N_{geq >= 0} ^2
    result->Multiply(result.get());

    // N_{geq >= 0} ^2 * N_2
    result->Multiply(b2.get());

    // 2* N_2
    b2->Scale(2.);

    // N_1 + 2* N_2
    b1->Add(b2.get());

    // (N_1 + 2* N_2)^2
    b1->Multiply(b1.get());

    // N_{geq >= 0} ^2 * N_2 / (N_1 + 2* N_2)^2
    result->Divide(b1.get());

    // 4G* N_{geq >= 0} ^2 * N_2 / (N_1 + 2* N_2)^2
    result->Scale(4.);

    for (int ibin = 1; ibin <= result->GetNbinsX(); ++ibin) {
        result->SetBinError(ibin, 0.);
    }

    return result;
}

std::unique_ptr<TH1D> Common::GetShiftedHisto(const TH1D* hist, bool isUp) {
    std::unique_ptr<TH1D> result(static_cast<TH1D*>(hist->Clone()));
    result->SetDirectory(nullptr);

    for (int ibin = 1; ibin <= hist->GetNbinsX(); ++ibin) {
        double content = isUp ? (hist->GetBinContent(ibin) + hist->GetBinError(ibin)) : (hist->GetBinContent(ibin) - hist->GetBinError(ibin));
        if (content < 0.) content = 1e-6;

        result->SetBinContent(ibin, content);
        result->SetBinError(ibin, 0.);
    }

    return result;
}

std::size_t Common::GetNbtags_77(const Event& event) {
    std::size_t nTags(0);

    for (std::size_t i = 0; i < event.jet_pt->size(); ++i) {
        if (event.jet_isbtagged_DL1dv01_77->at(i)) ++nTags;
    }

    return nTags;
}

std::string Common::RegionToStringPlots(const std::string& reg) {
    if (reg == Common::RegionToString(Common::REGION::SAMESIGNZMASS)) return "SS, 66 < m_{ll} < 116 GeV";
    else if (reg == Common::RegionToString(Common::REGION::SAMESIGNZVETO)) return " SS, Z veto";
    else if (reg == Common::RegionToString(Common::REGION::OPPOSITESIGNZMASS)) return "66 < m_{ll} < 116 GeV";
    else if (reg == Common::RegionToString(Common::REGION::OPPOSITESIGNZVETO)) return "Z veto";
    else if (reg == Common::RegionToString(Common::REGION::OPPOSITESIGN)) return "";
    else if (reg == Common::RegionToString(Common::REGION::OPPOSITESIGN1B)) return "#geq 1b";    
    else if (reg == Common::RegionToString(Common::REGION::SAMESIGN)) return "SS";
    else if (reg == Common::RegionToString(Common::REGION::INCLUSIVE)) return "";

    return "";
}

bool Common::HasLeptonsBelowX(const Event& event, const double cut) {
    for (const auto ept : *event.el_pt) {
        if (ept < cut) return true;
    }
    for (const auto mupt : *event.mu_pt) {
        if (mupt < cut) return true;
    }

    return false;
}

double Common::GetNNLOWeight(const TruthEvent* event, const TTbarNNLORecursiveRew* rew) {
    if (!event) return 1.;
    if (!rew) return 1.;
    TLorentzVector t;
    TLorentzVector tbar;
    t.SetPtEtaPhiM(event->MC_t_afterFSR_pt/1e3,
                   event->MC_t_afterFSR_eta,
                   event->MC_t_afterFSR_phi,
                   event->MC_t_afterFSR_m/1e3);
    
    tbar.SetPtEtaPhiM(event->MC_tbar_afterFSR_pt/1e3,
                      event->MC_tbar_afterFSR_eta,
                      event->MC_tbar_afterFSR_phi,
                      event->MC_tbar_afterFSR_m/1e3);
    const double top_pt = t.Pt();
    const double tbar_pt = tbar.Pt();
    const double ttbar_mass = (t + tbar).M();
    const double ttbar_pt = (t + tbar).Pt();
    return rew->GetWeight(top_pt, tbar_pt, ttbar_mass, ttbar_pt);
}

bool Common::IsFiducialZ(const ParticleEvent& pl) {
    for (const auto leptonId : *pl.leptonBornPdgId) {
        // tau lepton
        if (std::abs(leptonId) == 15) return false;
    }

    for (const auto leptonPt : *pl.leptonBornPt) {
        if (leptonPt/1e3 < 27) return false;
    }
    for (const auto leptonEta : *pl.leptonBornEta) {
        if (std::abs(leptonEta) > 2.5) return false;
    }

    if (pl.leptonBornPt->size() != 2) return false;

    ROOT::Math::PtEtaPhiEVector l1(pl.leptonBornPt->at(0)/1e3, pl.leptonBornEta->at(0), pl.leptonBornPhi->at(0), pl.leptonBornE->at(0)/1e3);
    ROOT::Math::PtEtaPhiEVector l2(pl.leptonBornPt->at(1)/1e3, pl.leptonBornEta->at(1), pl.leptonBornPhi->at(1), pl.leptonBornE->at(1)/1e3);

    const double mll = (l1+l2).M();

    if (mll < 66) return false;
    if (mll > 116) return false;

    return true;
}
    
double Common::IsoCorrection(const Event& event, const std::unique_ptr<TH1D>& el, const std::unique_ptr<TH1D>& mu) {
    double corr(1.);

    for (const auto iel : *event.el_pt) {
        int bin = el->FindBin(iel/1e3);
        if (bin >= el->GetNbinsX()) bin = el->GetNbinsX() - 1;
        corr *= el->GetBinContent(bin);
    }
    for (const auto imu : *event.mu_pt) {
        int bin = mu->FindBin(imu/1e3);
        if (bin >= mu->GetNbinsX()) bin = mu->GetNbinsX() - 1;
        corr *= mu->GetBinContent(bin);
    }

    return corr;
}

double Common::JvtSyst(const Event& event, const bool isUp) {
    double result(1.0);

    for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
        if ((event.jet_pt->at(ijet) < 60000) && event.jet_isbtagged_DL1dv01_77->at(ijet)) {
            result *= isUp ? 1.1 : 0.9;
        }
    }

    return result;
}
    
void Common::RemoveJetsBelowX(Event* event, const double pt) {
    std::vector<std::size_t> indices;

    for (std::size_t ijet = 0; ijet < event->jet_pt->size(); ++ijet) {
        if (event->jet_pt->at(ijet) > pt) {
            indices.emplace_back(ijet);
        } 
    }

    // now correct all jet vectors
    std::vector<float> jet_pt;
    std::vector<float> jet_eta;
    std::vector<float> jet_phi;
    std::vector<float> jet_e;
    std::vector<float> jet_jvt;
    std::vector<float> jet_DL1dv01;
    std::vector<float> jet_DL1dv01_pc;
    std::vector<float> jet_DL1dv01_pb;
    std::vector<float> jet_DL1dv01_pu;
    std::vector<char> jet_isbtagged_DL1dv01_85;
    std::vector<char> jet_isbtagged_DL1dv01_77;
    std::vector<char> jet_isbtagged_DL1dv01_70;
    std::vector<char> jet_isbtagged_DL1dv01_60;
    std::vector<int> jet_tagWeightBin_DL1dv01_Continuous;

    for (const std::size_t index : indices) {
        jet_pt.emplace_back(event->jet_pt->at(index));
        jet_eta.emplace_back(event->jet_eta->at(index));
        jet_phi.emplace_back(event->jet_phi->at(index));
        jet_e.emplace_back(event->jet_e->at(index));
        jet_jvt.emplace_back(event->jet_jvt->at(index));
        jet_DL1dv01.emplace_back(event->jet_DL1dv01->at(index));
        jet_DL1dv01_pc.emplace_back(event->jet_DL1dv01_pc->at(index));
        jet_DL1dv01_pb.emplace_back(event->jet_DL1dv01_pb->at(index));
        jet_DL1dv01_pu.emplace_back(event->jet_DL1dv01_pu->at(index));
        jet_isbtagged_DL1dv01_85.emplace_back(event->jet_isbtagged_DL1dv01_85->at(index));
        jet_isbtagged_DL1dv01_77.emplace_back(event->jet_isbtagged_DL1dv01_77->at(index));
        jet_isbtagged_DL1dv01_70.emplace_back(event->jet_isbtagged_DL1dv01_70->at(index));
        jet_isbtagged_DL1dv01_60.emplace_back(event->jet_isbtagged_DL1dv01_60->at(index));
        jet_tagWeightBin_DL1dv01_Continuous.emplace_back(event->jet_tagWeightBin_DL1dv01_Continuous->at(index));
    }

    *(event->jet_pt) = jet_pt;
    *(event->jet_eta) = jet_eta;
    *(event->jet_phi) = jet_phi;
    *(event->jet_e) = jet_e;
    *(event->jet_jvt) = jet_jvt;
    *(event->jet_DL1dv01) = jet_DL1dv01;
    *(event->jet_DL1dv01_pb) = jet_DL1dv01_pb;
    *(event->jet_DL1dv01_pc) = jet_DL1dv01_pc;
    *(event->jet_DL1dv01_pu) = jet_DL1dv01_pu;
    *(event->jet_isbtagged_DL1dv01_85) = jet_isbtagged_DL1dv01_85;
    *(event->jet_isbtagged_DL1dv01_77) = jet_isbtagged_DL1dv01_77;
    *(event->jet_isbtagged_DL1dv01_70) = jet_isbtagged_DL1dv01_70;
    *(event->jet_isbtagged_DL1dv01_60) = jet_isbtagged_DL1dv01_60;
    *(event->jet_tagWeightBin_DL1dv01_Continuous) = jet_tagWeightBin_DL1dv01_Continuous;
}

double Common::GetDileptonPtWeight(const Event& event, const TH1D* hist) {
    double pt1;
    double pt2;
    double eta1;
    double eta2;
    double phi1;
    double phi2;
    double e1;
    double e2;
    if (event.el_pt->size() == 0) {
        pt1 = event.mu_pt->at(0);
        pt2 = event.mu_pt->at(1);
        eta1 = event.mu_eta->at(0);
        eta2 = event.mu_eta->at(1);
        phi1 = event.mu_phi->at(0);
        phi2 = event.mu_phi->at(1);
        e1 = event.mu_e->at(0);
        e2 = event.mu_e->at(1);
    } else if ((event.el_pt->size() == 1) && (event.mu_pt->size() == 1)) {
        pt1 = event.el_pt->at(0);
        pt2 = event.mu_pt->at(0);
        eta1 = event.el_eta->at(0);
        eta2 = event.mu_eta->at(0);
        phi1 = event.el_phi->at(0);
        phi2 = event.mu_phi->at(0);
        e1 = event.el_e->at(0);
        e2 = event.mu_e->at(0);
    } else if (event.el_pt->size() == 2) {
        pt1 = event.el_pt->at(0);
        pt2 = event.el_pt->at(1);
        eta1 = event.el_eta->at(0);
        eta2 = event.el_eta->at(1);
        phi1 = event.el_phi->at(0);
        phi2 = event.el_phi->at(1);
        e1 = event.el_e->at(0);
        e2 = event.el_e->at(1);
    } else {
        return 1.;
    }

    ROOT::Math::PtEtaPhiEVector l1(pt1/1e3, eta1, phi1, e1/1e3);
    ROOT::Math::PtEtaPhiEVector l2(pt2/1e3, eta2, phi2, e2/1e3);
    const double pt = (l1 + l2).Pt();

    int bin = hist->GetXaxis()->FindBin(pt);
    if (bin >= hist->GetNbinsX()) bin = hist->GetNbinsX() - 1;

    return hist->GetBinContent(bin);
}
