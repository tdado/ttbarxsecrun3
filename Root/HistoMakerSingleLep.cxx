#include "TTbarXsec/HistoMakerSingleLep.h"
#include "TTbarXsec/Common.h"
#include "TTbarXsec/Event.h"
#include "TTbarXsec/WeightManager.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TH1D.h"
#include "TTree.h"

#include <iostream>

HistoMakerSingleLep::HistoMakerSingleLep(const std::string& type,
                                         const std::string& systematics,
                                         const std::vector<std::string>& sfsyst_list) :
    m_type(type),
    m_systematics(systematics),
    m_current_syst("nominal"),
    m_isMC(true),
    m_dsid(-1),
    m_tree_name("nominal"),
    m_sfsyst_list(sfsyst_list),
    m_control_plots(nullptr) {

    /// Allocating on heap as these are huge objects
    m_control_plots = std::make_unique<ControlPlots> ();
    m_control_plots->Init(m_sfsyst_list, false);
    m_control_plots->SetSyst(m_systematics);
}

void HistoMakerSingleLep::FillHistos(const WeightManager& wei_manager,
                                     const std::string& tree_name,
                                     const std::string& path) {

    
    /// Set the tree name
    m_tree_name = tree_name;

    std::unique_ptr<TFile> file(TFile::Open(path.c_str(), "READ"));
    if (!file) {
        std::cerr << "HistoMakerSingleLep::FillHistos: Cannot open input file: " << path << ", skipping" << std::endl;
        return;
    }
    TTree *tree = (static_cast<TTree*>(file->Get(tree_name.c_str())));
    if (!tree) {
        std::cerr << "HistoMakerSingleLep::FillHistos: Cannot read tree: " << tree_name << " from file: " << path <<", skipping" << std::endl;
        return;
    }

    const int nentries = tree->GetEntries();

    bool use_loose(false);
    bool is_syst(false);
    if (m_type == "Multijet") use_loose = true;
    if (m_systematics == "syst") is_syst = true;

    Event event(tree, m_isMC, use_loose, is_syst, Common::CHANNEL::SINGLELEPTON);

    /// Run event loop
    for (int ievent = 0; ievent < nentries; ++ievent) {
        event.GetEntry(ievent);
        if (ievent % 100000 == 0) {
            std::cout << "\t\tProcessing event " << ievent << ", out of " << nentries << " events\n";
        }
    
        m_lepton_type = Common::FindLeptonType(event);
        
        /// Prepare vector with weights
        std::vector<double> wei_vec;
        if (m_isMC) {
            if (m_systematics == "sfsyst") {
                wei_vec = wei_manager.GetSfSystWeights(event, false);
            } else {
                wei_vec.emplace_back(wei_manager.GetNominalWeight(event, false));
            }
        } else {
            // weight is one for data
            wei_vec.emplace_back(1.);
        }
        
        if (wei_vec.size() != m_sfsyst_list.size()) {
            std::cerr << "HistoMakerSingleLep::FillHistos: Size of sf names and sf weights do not match" << std::endl;
            std::cerr << "HistoMakerSingleLep::FillHistos: sf names size: " << m_sfsyst_list.size() << ", weights size: " << wei_vec.size() << "\n";
            exit(EXIT_FAILURE);
        }

        m_control_plots->FillAllHistos(event,
                                       {0,0}, // dummy values neede only for dilep
                                       m_isMC,
                                       m_lepton_type,
                                       wei_vec,
                                       true,
                                       Common::REGION::INCLUSIVE);
    }

    delete tree;
    file->Close();
}

void HistoMakerSingleLep::Finalise() {
    m_control_plots->Finalise();
}

void HistoMakerSingleLep::WriteHistosToFile(const std::string& path, const std::string& name) const {
    const std::string suffix = m_systematics == "nominal" ? ".temp" : ""; 
    std::string final_path = path+"/"+name+"/"+m_type+"_"+name;

    std::unique_ptr<TFile> file_el(TFile::Open((final_path+"_el.root"+suffix).c_str(), "UPDATE"));
    std::unique_ptr<TFile> file_mu(TFile::Open((final_path+"_mu.root"+suffix).c_str(), "UPDATE"));
    if (!file_el || !file_mu) {
        std::cerr << "HistoMakerSingleLep::WriteHistosToFile: Cannot open file at: " << final_path << std::endl;
        exit(EXIT_FAILURE);
    }
    
    const std::vector<std::string> regNames = Common::GetRegionString(Common::CHANNEL::SINGLELEPTON);
    
    auto makeRegString = [&regNames](TFile *file, const std::string& folder) {
        for (const auto& i : regNames) {
            file->cd(folder.c_str());
            gDirectory->mkdir(i.c_str());
        }
    };

    /// Create the directory structure
    if (m_systematics == "nominal") {
        file_el->cd();
        gDirectory->mkdir("nominal");
        file_mu->cd();
        gDirectory->mkdir("nominal");
        makeRegString(file_el.get(), "nominal");
        makeRegString(file_mu.get(), "nominal");
    } else if (m_systematics == "syst") {
        if (!file_el->Get(m_current_syst.c_str())) {
            file_el->cd();
            gDirectory->mkdir(m_current_syst.c_str());
            makeRegString(file_el.get(), m_current_syst);
        }
        if (!file_mu->Get(m_current_syst.c_str())) {
            file_mu->cd();
            gDirectory->mkdir(m_current_syst.c_str());
            makeRegString(file_mu.get(), m_current_syst);
        }
    } else if (m_systematics == "sfsyst") {
        for (const auto& isf : m_sfsyst_list) {
            if (!file_el->Get(isf.c_str())) {
                file_el->cd();
                gDirectory->mkdir(isf.c_str());
                makeRegString(file_el.get(), isf);
            }
            if (!file_mu->Get(isf.c_str())) {
                file_mu->cd();
                gDirectory->mkdir(isf.c_str());
                makeRegString(file_mu.get(), isf);
            }
        }
    }

    m_control_plots->Write(m_current_syst, nullptr, nullptr, nullptr, file_el.get(), file_mu.get());

    file_el->Close();
    file_mu->Close();
}
